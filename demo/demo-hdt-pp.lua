-- The following hack is not needed in your own code: it allows to
-- `require 'dsp402'` without having to install `libmachinery`
do
    local arg0 = select(1, ...)
    local dspfile = arg0:gsub('demo/demo%-hdt%-pp.lua$', 'src/dsp402.lua')
    package.preload['dsp402'] = loadfile(dspfile)
end


-- Load the DSP402 script
local dsp402 = require 'dsp402'


-- Instantiate the axis: the demo APIs are provided by the C code
local axis = dsp402.new {
    status_word = demo.status_word,
    control_word = demo.control_word,
    target_point = demo.target_point,
    sdo_download = function (...)
        local done = demo.sdo_download(...)
        if done ~= nil then
            print(string.format('SDO 0x%X <- %d', ...),
                  done and 'done' or 'fail')
        end
        return done
    end,
    step = function (n)
        print(string.format('Step %d', n))
    end,
}


-- Define a simple automata, just to do something:
--
-- 1. configure some parameters
--    (state `start`)
-- 2. reset the position on the home switch (see homing method 19)
--    (automatically performed before the first positioning)
-- 3. move to -500000 at 200 RpM
--    (state `slow_move`)
-- 4. move to +500000 at 2000 RpM
--    (state `quick_move`)
-- 5. jump to step 3
local start, slow_move, quick_move

function start()
    axis:configure {
        homing_method        = 19,
        homing_speed_search  = 1000,
        homing_speed_zero    = 100,
        homing_acceleration  = 4000,
        profile_acceleration = 4000,
        profile_deceleration = 4000,
    }
    return slow_move
end

function slow_move()
    axis:configure { profile_velocity = 500 }
    axis:set_target(-500000)
    return axis.runtime.done and quick_move or slow_move
end

function quick_move()
    axis:configure { profile_velocity = 2000 }
    axis:set_target(500000)
    return axis.runtime.done and slow_move or quick_move
end


local loop_function = axis:profile_position()
local state = start

-- Return the function called on each iteration
return function ()
    -- The loop function must be called on every iteration
    loop_function()
    -- Run through the automata
    state = state()
end
