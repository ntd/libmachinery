/* Handling a DSP402 compliant axis with CoE (CANopen over EtherCAT)
 *
 * A demo program that moves an axis in profile position mode. To be
 * able to use it, you must fulfill the following conditions:
 *
 * - the igh-EtherCAT master stack must be already running
 * - the user must have read/write access to the EtherCAT device
 * - an HDT drive must be connected as first node
 * - the motor must be already properly configured
 *
 * This code showcases some libmachinery features:
 *
 * - interactions with the EtherCAT master stack
 * - real-time loop handling
 * - Lua scripting support
 * - DSP402 profile support
 *
 * To keep the implementation as simple as possible, global variables
 * and packed bit-fields are used and threads are avoided. Keep in mind
 * this is not always possible!
 */

#include "machinery.h"
#include "ethercat-slaves.h"
#include <lualib.h>
#include <lauxlib.h>


typedef struct __attribute__((packed)) {
    unsigned    control_word : 16;
    int         target_velocity : 16;
    int         home_offset : 32;
    int         target_position : 32;
    unsigned    profile_velocity : 32;
} RxPdo_1_2;

typedef struct __attribute__((packed)) {
    unsigned    status_word: 16;
    int         mode_of_operation : 8;
    unsigned    error_register : 8;
    int         position_demand_value : 32;
    int         position_actual_value : 32;
} TxPdo_1_2;


static App app;
static RxPdo_1_2 *rxpdo = NULL;
static TxPdo_1_2 *txpdo = NULL;


static int
api_status_word(lua_State *L)
{
    lua_pushnumber(L, txpdo->status_word);
    return 1;
}

static int
api_control_word(lua_State *L)
{
    if (lua_gettop(L) == 0) {
        /* Read action (no argument provided) */
        lua_pushnumber(L, rxpdo->control_word);
        return 1;
    } else {
        /* Write action (one argument provided) */
        lua_Number value = luaL_checknumber(L, 1);
        rxpdo->control_word = value;
        return 0;
    }
}

static int
api_target_point(lua_State *L)
{
    if (lua_gettop(L) == 0) {
        /* Read action (no argument provided) */
        lua_pushnumber(L, rxpdo->target_position);
        return 1;
    } else {
        /* Write action (one argument provided) */
        lua_Number value = luaL_checknumber(L, 1);
        rxpdo->target_position = value;
        return 0;
    }
}

static int
api_sdo_download(lua_State *L)
{
    uint32_t map = luaL_checknumber(L, 1);
    lua_Number value = luaL_checknumber(L, 2);

    switch (app_ethercat_sdo_download(&app, 0, map, value)) {
    case ACTION_COMPLETE:
        lua_pushboolean(L, 1);
        return 1;
    case ACTION_ERROR:
        lua_pushboolean(L, 0);
        return 1;
    default:
        return 0;
    }
}

int
main(int argc, char *argv[])
{
    static const luaL_Reg library[] = {
        { "status_word",  api_status_word },
        { "control_word", api_control_word },
        { "target_point", api_target_point },
        { "sdo_download", api_sdo_download },
        { NULL,           NULL }
    };
    int n;

    /* Initialize the app */
    app_init(&app, "demo");

    /* Configure and initialize the EtherCAT stack */
    if (app_ethercat_init(&app) != ACTION_DONE) {
        die("EtherCAT cannot be initialized");
    }

    /* The first node connected to the bus must be an HDT driver */
    app_ethercat_add_slave(&app, 0, 0, machinery_ethercat_slave_hdt_pp());

    /* Start the EtherCAT communications */
    app_ethercat_start(&app);

    /* Get the process images */
    rxpdo = app_ethercat_image(&app, 0, 2);
    txpdo = app_ethercat_image(&app, 0, 3);

    /* Initialize the Lua script */
    app_lua_init(&app, SRCDIR "/demo-hdt-pp.lua", library);

    /* Initialize the timer: 5 milliseconds per iteration */
    app_timer_init(&app, 5000);

    /* Real-time loop: run it for about 20 seconds (4000 iterations) */
    for (n = 0; n < 4000; ++n) {
        /* Read inputs */
        app_ethercat_receive(&app);

        /* Iteration */
        app_lua_run(&app);

        /* Write outputs */
        app_ethercat_send(&app);

        /* Wait for next iteration */
        app_timer_wait(&app);
    }

    /* Free the resources allocated for the timer */
    app_timer_free(&app);

    /* Free the resources allocated for Lua scripting */
    app_lua_free(&app);

    /* Stop the EtherCAT communications */
    app_ethercat_stop(&app);

    /* Free the resources allocated for app */
    app_free(&app);

    return 0;
}
