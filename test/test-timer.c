#include <stdarg.h>
#include <stddef.h>
#include <semaphore.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <stdlib.h>
#include <sys/timerfd.h>

void
__wrap_die_with_errno(const char *caller)
{
    check_expected(caller);
}

void
__wrap_info(const char *format, ...)
{
    check_expected(format);
}

ssize_t
__wrap_read(int fildes, void *buf, size_t nbyte)
{
    check_expected(fildes);
    assert_non_null(buf);
    check_expected(nbyte);

    /* Simulate one missed timeslot */
    * (uint64_t *) buf = 1;

    return mock_type(int);
}

int
__wrap_close(int fildes)
{
    check_expected(fildes);
    return 0;
}

int
__wrap_timerfd_create(int clockid, int flags)
{
    check_expected(clockid);
    check_expected(flags);
    return mock_type(int);
}

int
__wrap_timerfd_settime(int fildes, int flags,
                       const struct itimerspec *new_value,
                       struct itimerspec *old_value)
{
    check_expected(fildes);
    check_expected(flags);
    assert_non_null(new_value);
    assert_null(old_value);
    return mock_type(int);
}

static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_timer_init_free(void **state)
{
    App *app = *state;

    assert_null(app->timer);

    /* Checking various failure types */
    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, -1);
    expect_string(__wrap_die_with_errno, caller, "timerfd_create");
    app_timer_init(app, 1234);
    assert_null(app->timer);

    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, 666);
    expect_value(__wrap_timerfd_settime, fildes, 666);
    expect_value(__wrap_timerfd_settime, flags, 0);
    will_return(__wrap_timerfd_settime, -1);
    expect_string(__wrap_die_with_errno, caller, "timerfd_settime");
    app_timer_init(app, 1234);
    assert_null(app->timer);

    /* Checking successful construction */
    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, 666);
    expect_value(__wrap_timerfd_settime, fildes, 666);
    expect_value(__wrap_timerfd_settime, flags, 0);
    will_return(__wrap_timerfd_settime, 0);
    app_timer_init(app, 1234);
    assert_non_null(app->timer);

    /* Checking successful destruction */
    expect_string(__wrap_info, format, "timer: destruction");
    expect_value(__wrap_close, fildes, 666);
    app_timer_free(app);
    assert_null(app->timer);

    /* Checking idempotence: closing a closed timer should be a no-op */
    app_timer_free(app);
}

static void
test_timer_wait(void **state)
{
    App *app = *state;

    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, 123);
    expect_value(__wrap_timerfd_settime, fildes, 123);
    expect_value(__wrap_timerfd_settime, flags, 0);
    will_return(__wrap_timerfd_settime, 0);
    app_timer_init(app, 10000);
    assert_non_null(app->timer);

    /* Checking failure */
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, -1);
    expect_string(__wrap_die_with_errno, caller, "read");
    app_timer_wait(app);

    /* Checking success */
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);

    expect_string(__wrap_info, format, "timer: destruction");
    expect_value(__wrap_close, fildes, 123);
    app_timer_free(app);
}

static void
test_timer_missed(void **state)
{
    App *app = *state;

    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, 123);
    expect_value(__wrap_timerfd_settime, fildes, 123);
    expect_value(__wrap_timerfd_settime, flags, 0);
    will_return(__wrap_timerfd_settime, 0);
    app_timer_init(app, 1000);
    assert_non_null(app->timer);

    /* Checking proper initial value */
    assert_int_equal(0, app_timer_get_missed(app));

    /* Checking feedback after a couple of missed timeslots */
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_int_equal(1, app_timer_get_missed(app));

    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_int_equal(2, app_timer_get_missed(app));

    expect_string(__wrap_info, format, "timer: destruction");
    expect_value(__wrap_close, fildes, 123);
    app_timer_free(app);
}

static void
test_timer_busy(void **state)
{
    App *app = *state;

    expect_string(__wrap_info, format, "timer: construction");
    expect_value(__wrap_timerfd_create, clockid, CLOCK_MONOTONIC);
    expect_value(__wrap_timerfd_create, flags, 0);
    will_return(__wrap_timerfd_create, 123);
    expect_value(__wrap_timerfd_settime, fildes, 123);
    expect_value(__wrap_timerfd_settime, flags, 0);
    will_return(__wrap_timerfd_settime, 0);
    app_timer_init(app, 1000);
    assert_non_null(app->timer);

    /* Checking proper initial value */
    assert_float_equal(0.0, app_timer_get_busy(app), 0.0005);

    /* Checking immediate feedback, i.e. ~0 busy time */
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_float_equal(0.0, app_timer_get_busy(app), 0.0005);

    /* Checking feedback after 1 millisecond delay */
    usleep(1000);
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_float_equal(0.001, app_timer_get_busy(app), 0.0005);

    /* Checking feedback after 0.5 millisecond delay */
    usleep(500);
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_float_equal(0.001, app_timer_get_busy(app), 0.0005);

    /* Checking feedback after 2 millisecond delay */
    usleep(2000);
    expect_value(__wrap_read, fildes, 123);
    expect_value(__wrap_read, nbyte, 8);
    will_return(__wrap_read, 0);
    app_timer_wait(app);
    assert_float_equal(0.002, app_timer_get_busy(app), 0.0005);

    expect_string(__wrap_info, format, "timer: destruction");
    expect_value(__wrap_close, fildes, 123);
    app_timer_free(app);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_timer_init_free),
        cmocka_unit_test(test_timer_wait),
        cmocka_unit_test(test_timer_missed),
        cmocka_unit_test(test_timer_busy),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
