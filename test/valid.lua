-- This is a valid Lua file and a valid libmachinery script because
-- it returns a function that can be called iteratively
local n = 0
return function ()
    demo.set_value(n)
    n = n + 1
end
