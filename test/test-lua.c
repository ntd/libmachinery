#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <stdlib.h>
#include <lua.h>
#include <lauxlib.h>


void
__wrap_die(const char *format, ...)
{
    check_expected(format);
}


static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "demo");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_lua_init_free(void **state)
{
    App *app = *state;

    assert_null(app->lua);

    expect_string(__wrap_die, format, "Script '%s' failed to load");
    app_lua_init(app, "inexistent.lua", NULL);

    expect_string(__wrap_die, format, "The script '%s' does not return a function");
    app_lua_init(app, SRCDIR "/invalid.lua", NULL);

    app_lua_init(app, SRCDIR "/valid.lua", NULL);
    assert_non_null(app->lua);

    app_lua_free(app);
    assert_null(app->lua);

    /* Check further app_lua_free() are a NO-OP */
    app_lua_free(app);
    app_lua_free(app);
}

static int value;

static int
api_set_value(lua_State *L)
{
    value = lua_tonumber(L, 1);
    return 0;
}

static void
test_lua_run(void **state)
{
    static const luaL_Reg library[] = {
        { "set_value", api_set_value },
        { NULL,        NULL }
    };
    App *app = *state;

    app_lua_init(app, SRCDIR "/valid.lua", library);
    assert_non_null(app->lua);

    value = 0;
    app_lua_run(app);
    assert_int_equal(0, value);

    app_lua_run(app);
    assert_int_equal(1, value);

    app_lua_run(app);
    assert_int_equal(2, value);

    app_lua_free(app);
    assert_null(app->lua);
}

static void
test_lua_set_name(void **state)
{
    static const luaL_Reg library[] = {
        { "set_value", api_set_value },
        { NULL,        NULL }
    };
    App *app = *state;

    app_lua_set_name(app, "customname");
    app_lua_init(app, SRCDIR "/valid-customname.lua", library);
    assert_non_null(app->lua);

    value = 0;
    app_lua_run(app);
    assert_int_equal(0, value);

    app_lua_run(app);
    assert_int_equal(1, value);

    app_lua_run(app);
    assert_int_equal(2, value);

    app_lua_free(app);
    assert_null(app->lua);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_lua_init_free),
        cmocka_unit_test(test_lua_run),
        cmocka_unit_test(test_lua_set_name),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
