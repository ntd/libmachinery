#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <stdlib.h>
#include <canfestival.h>

/* The following references are here to avoid linking with CANfestival */

void
_RxPDO_EventTimers_Handler(CO_Data *data, UNS32 pdo)
{
}

void
_initialisation(CO_Data *data)
{
}

void
_preOperational(CO_Data *data)
{
}

void
_operational(CO_Data *data)
{
}

void
_stopped(CO_Data *data)
{
}

void
_post_sync(CO_Data *data)
{
}

void
_post_TPDO(CO_Data *data)
{
}

void
_post_emcy(CO_Data *data, UNS8 id, UNS16 errcode, UNS8 reg, const UNS8 spec[5])
{
}

void
_heartbeatError(CO_Data *data, UNS8 id)
{
}

void
_post_SlaveBootup(CO_Data *data, UNS8 id)
{
}

void
_post_SlaveStateChange(CO_Data *data, UNS8 id, e_nodeState state)
{
}

void
_nodeguardError(CO_Data *data, UNS8 id)
{
}

UNS32
_storeODSubIndex(CO_Data *data, UNS16 idx, UNS8 subidx)
{
    return 0;
}

/* Wrappers follow */

void
__wrap_die(const char *format, ...)
{
    check_expected(format);
}

void
__wrap_die_with_errno(const char *caller)
{
    check_expected(caller);
}

void
__wrap_info(const char *format, ...)
{
    check_expected(format);
}

void
__wrap_warning(const char *format, ...)
{
    check_expected(format);
}

LIB_HANDLE
__wrap_LoadCanDriver(const char *driver_name)
{
    assert_non_null(driver_name);
    return mock_type(LIB_HANDLE);
}

CAN_PORT
__wrap_canOpen(s_BOARD *board, CO_Data *data)
{
    assert_non_null(board);
    check_expected(data);
    return mock_type(CAN_PORT);
}

int
__wrap_canClose(CO_Data *data)
{
    check_expected(data);
    /* Result is ignored anyway */
    return 1;
}

UNS8
__wrap_GetSDOClientFromNodeId(CO_Data *data, UNS8 id)
{
    check_expected(data);
    check_expected(id);
    return mock_type(UNS8);
}

UNS8
__wrap_closeSDOtransfer(CO_Data *data, UNS8 id, UNS8 whoami)
{
    return 0;
}

void
__wrap_setNodeId(CO_Data *data, UNS8 id)
{
    check_expected(data);
    check_expected(id);
}

UNS8
__wrap_setState(CO_Data *data, e_nodeState state)
{
    check_expected(data);
    check_expected(state);
    return mock_type(UNS8);
}

UNS8
__wrap_masterSendNMTstateChange(CO_Data *data, UNS8 id, UNS8 state)
{
    check_expected(data);
    check_expected(id);
    check_expected(state);
    return mock_type(UNS8);
}

TIMER_HANDLE
__wrap_SetAlarm(CO_Data *data, UNS32 id, TimerCallback_t callback,
                TIMEVAL value, TIMEVAL period)
{
    check_expected(data);
    check_expected(id);
    check_expected(callback);
    check_expected(value);
    check_expected(period);
    return mock_type(TIMER_HANDLE);
}

TIMER_HANDLE
__wrap_DelAlarm(TIMER_HANDLE handle)
{
    check_expected(handle);
    return handle;
}

UNS8
__wrap_sendPDOevent(CO_Data *data)
{
    check_expected(data);
    /* Return value is ignored by the machinery code */
    return 0;
}

void
__wrap_TimerInit(void)
{
    function_called();
}

void
__wrap_StartTimerLoop(TimerCallback_t callback)
{
    assert_non_null(callback);
    function_called();
}

void
__wrap_StopTimerLoop(TimerCallback_t callback)
{
    assert_non_null(callback);
    function_called();
}

UNS8
__wrap_readNetworkDict(CO_Data *data, UNS8 id, UNS16 idx, UNS8 subidx,
                       UNS8 type, UNS8 blocking)
{
    check_expected(data);
    check_expected(id);
    check_expected(idx);
    check_expected(subidx);
    assert_int_equal(type, 0);
    assert_int_equal(blocking, 0);
    return mock_type(UNS8);
}

UNS8
__wrap_getReadResultNetworkDict(CO_Data *data, UNS8 id,
                                void *value, UNS32 *size, UNS32 *code)
{
    check_expected(data);
    check_expected(id);
    check_expected(value);
    assert_non_null(size);
    assert_non_null(code);
    return mock_type(UNS8);
}

UNS8
__wrap_writeNetworkDict(CO_Data *data, UNS8 id,
                        UNS16 idx, UNS8 subidx,
                        UNS32 count, UNS8 type, void *value,
                        UNS8 blocking)
{
    check_expected(data);
    check_expected(id);
    check_expected(idx);
    check_expected(subidx);
    check_expected(count);
    assert_int_equal(type, 0);
    assert_non_null(value);
    assert_int_equal(blocking, 0);
    return mock_type(UNS8);
}

UNS8
__wrap_getWriteResultNetworkDict(CO_Data *data, UNS8 id, UNS32 *code)
{
    check_expected(data);
    check_expected(id);
    assert_non_null(code);
    return mock_type(UNS8);
}

UNS32
__wrap_RegisterSetODentryCallBack(CO_Data *data,
                                  UNS16 idx, UNS8 subidx,
                                  ODCallback_t callback)
{
    check_expected(data);
    check_expected(idx);
    check_expected(subidx);
    assert_non_null(callback);
    return mock_type(UNS8);
}

UNS32
__wrap__getODentry(CO_Data *data, UNS16 idx, UNS8 subidx,
                   void *value, UNS32 *size, UNS8 *type,
                   UNS8 check_access, UNS8 endianize)
{
    check_expected(data);
    check_expected(idx);
    check_expected(subidx);
    assert_non_null(value);
    assert_non_null(size);
    assert_non_null(type);
    return mock_type(UNS32);
}

UNS8
__wrap_writeNetworkDictCallBack(CO_Data *data, UNS8 id, UNS16 idx,
                                UNS8 subidx, UNS32 count, UNS8 type,
                                void *value, SDOCallback_t callback,
                                UNS8 blocking)
{
    check_expected(data);
    check_expected(id);
    check_expected(idx);
    check_expected(subidx);
    check_expected(count);
    assert_int_equal(type, 0);
    assert_non_null(value);
    assert_non_null(callback);
    assert_int_equal(blocking, 0);
    return mock_type(UNS8);
}

void
__wrap_resetClientSDOLineFromNodeId(CO_Data* d, UNS8 nodeId)
{
}


static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_canopen_init_free(void **state)
{
    App *app = *state;

    assert_null(app->canopen);

    will_return(__wrap_LoadCanDriver, 0);
    expect_string(__wrap_die_with_errno, caller, "LoadCanDriver");
    app_canopen_init(app, "1", 2);
    assert_null(app->canopen);

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);
    assert_non_null(app->canopen);

    app_canopen_free(app);

    /* Check further calls are a no-op */
    app_canopen_free(app);
}

static void
test_canopen_set_id(void **state)
{
    App *app = *state;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Just check that arbitrary calls do not crash the process */
    app_canopen_set_id(app, 0);
    app_canopen_set_id(app, 1);
    app_canopen_set_id(app, 0xFF);

    app_canopen_free(app);
}

static void
test_canopen_set_data(void **state)
{
    App *app = *state;
    CO_Data data;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Ensure a NULL call does not crash the process */
    app_canopen_set_data(app, NULL);

    app_canopen_set_data(app, &data);

    app_canopen_free(app);
}

static void
test_canopen_set_receiver(void **state)
{
    App *app = *state;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Just check that arbitrary calls do not crash the process */
    app_canopen_set_receiver(app, (void *) 1);
    app_canopen_set_receiver(app, NULL);

    app_canopen_free(app);
}

static void
test_canopen_add_slave(void **state)
{
    int i;
    App *app = *state;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Just check that arbitrary calls do not crash the process */
    app_canopen_add_slave(app, 123, NULL);
    app_canopen_add_slave(app, 0xFF, (void *) 1);

    /* 0 is not a valid id (is reserved for broadcast) so the following calls
     * should be equivalent to a no-op */
    app_canopen_add_slave(app, 0, NULL);
    app_canopen_add_slave(app, 0, (void *) 1);

    /* Check that adding too many slaves triggers an error. Actually 20 nodes
     * are allowed: 2 has been already added, insert the other 18 */
    for (i = 1; i <= 18; ++i) {
        app_canopen_add_slave(app, i, (void *) 1);
    }

    expect_string(__wrap_die, format, "canopen: maximum number of slaves reached (%d)");
    app_canopen_add_slave(app, 1, (void *) 1);

    /* Although 0 is invalid, it should trigger the error anyway */
    expect_string(__wrap_die, format, "canopen: maximum number of slaves reached (%d)");
    app_canopen_add_slave(app, 0, (void *) 1);

    app_canopen_free(app);
}

static void
test_canopen_start_stop(void **state)
{
    App *app = *state;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Check error condition */
    expect_value(__wrap_canOpen, data, NULL);
    will_return(__wrap_canOpen, 0);
    expect_string(__wrap_die_with_errno, caller, "canOpen");
    app_canopen_start(app);

    /* Check successful condition */
    expect_value(__wrap_canOpen, data, NULL);
    will_return(__wrap_canOpen, 1);
    expect_function_call(__wrap_TimerInit);
    expect_function_call(__wrap_StartTimerLoop);
    app_canopen_start(app);

    /* Check that further calls are a no-op */
    app_canopen_start(app);

    expect_function_call(__wrap_StopTimerLoop);
    expect_value(__wrap_canClose, data, NULL);
    app_canopen_stop(app);

    /* Check that further calls are a no-op */
    app_canopen_stop(app);

    app_canopen_free(app);
}

static void
test_canopen_map_tpdo(void **state)
{
    App *app = *state;
    int done;
    uint8_t id = 123;

    will_return(__wrap_LoadCanDriver, 456);
    app_canopen_init(app, "1", 2);

    /* 1. Destroy PDO by setting bit valid to 1 b of sub-index 01 h
     *    of the according PDO communication parameter. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1801);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 2. Disable mapping by setting sub-index 00 h to 00 h. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1A01);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 0);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 2a. Always use asynchronous transmission mode */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1801);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 2);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 2b. Set the inhibit time for TxPDO */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1801);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 3);
    expect_value(__wrap_writeNetworkDictCallBack, count, 2);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 2c. Set the event timer for TxPDO */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1801);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 5);
    expect_value(__wrap_writeNetworkDictCallBack, count, 2);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 3.  Modify mapping by changing the values of the corresponding
     *     sub-indices. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1A01);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 3.  Modify mapping by changing the values of the corresponding
     *     sub-indices (second object). */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1A01);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 2);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 4.  Enable mapping by setting sub-index 00 h to the number
     *     mapped objects (second object). */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1A01);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 0);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* 5.  Create PDO by setting bit valid to 0 b of sub-index 01 h
     *     of the according PDO communication parameter. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1801);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_WAITING);

    /* Mapping succesfully done */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    /* It is not easy to mock a whole compliant object dictionary,
     * so failing is just the quickest route to get things done */
    expect_value(__wrap__getODentry, data, NULL);
    expect_value(__wrap__getODentry, idx, 0x1400);
    expect_value(__wrap__getODentry, subidx, 1);
    will_return(__wrap__getODentry, OD_NOT_MAPPABLE);

    expect_string(__wrap_warning, format, "canopen: %cxPDO%d of remote node %d is not locally mapped");

    done = app_canopen_map_tpdos(app, id, PDO2, 2, 0x5678, 0x6789);
    assert_int_equal(done, ACTION_DONE);

    app_canopen_free(app);
}

static void
test_canopen_map_rpdo(void **state)
{
    App *app = *state;
    int done;
    uint8_t id = 210;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* 1. Destroy PDO by setting bit valid to 1 b of sub-index 01 h
     *    of the according PDO communication parameter. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1401);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 2.  Disable mapping by setting sub-index 00 h to 00 h. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1601);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 0);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 2a. Always use asynchronous transmission mode */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1401);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 2);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 2b. Set the inhibit time for TxPDO, if requested:
     *     by default the inhibit time is disabled */

    /* 2c. Set the event timer for TxPDO, if requested:
     *     this is not a TxPDO mapping */

    /* 3.  Modify mapping by changing the values of the corresponding
     *     sub-indices. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1601);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 3.  Modify mapping by changing the values of the corresponding
     *     sub-indices (second object). */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1601);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 2);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 4.  Enable mapping by setting sub-index 00 h to the number
     *     mapped objects (second object). */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1601);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 0);
    expect_value(__wrap_writeNetworkDictCallBack, count, 1);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* 5.  Create PDO by setting bit valid to 0 b of sub-index 01 h
     *     of the according PDO communication parameter. */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    expect_value(__wrap_writeNetworkDictCallBack, data, NULL);
    expect_value(__wrap_writeNetworkDictCallBack, id, id);
    expect_value(__wrap_writeNetworkDictCallBack, idx, 0x1401);
    expect_value(__wrap_writeNetworkDictCallBack, subidx, 1);
    expect_value(__wrap_writeNetworkDictCallBack, count, 4);
    will_return(__wrap_writeNetworkDictCallBack, 0);

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_WAITING);

    /* Mapping succesfully done */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    /* It is not easy to mock a whole compliant object dictionary,
     * so failing is just the quickest route to get things done */
    expect_value(__wrap__getODentry, data, NULL);
    expect_value(__wrap__getODentry, idx, 0x1800);
    expect_value(__wrap__getODentry, subidx, 1);
    will_return(__wrap__getODentry, OD_NOT_MAPPABLE);

    expect_string(__wrap_warning, format, "canopen: %cxPDO%d of remote node %d is not locally mapped");

    done = app_canopen_map_rpdos(app, id, PDO2, 2, 0x1234, 0x4321);
    assert_int_equal(done, ACTION_DONE);

    app_canopen_free(app);
}

static void
test_canopen_read_sdo(void **state)
{
    App *app = *state;
    int done;
    uint8_t id = 123;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Set max scans to 1 and max errors to 0 to avoid repeated attempts */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    app_canopen_settings(app, id, -2, -2, 1, 0, -2);

    /* Check successful SDO read */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_readNetworkDict, data, NULL);
    expect_value(__wrap_readNetworkDict, id, id);
    expect_value(__wrap_readNetworkDict, idx, 0x1234);
    expect_value(__wrap_readNetworkDict, subidx, 0x56);
    will_return(__wrap_readNetworkDict, 0);

    done = app_canopen_read_sdo(app, id, 0x12345678, NULL, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getReadResultNetworkDict, data, NULL);
    expect_value(__wrap_getReadResultNetworkDict, id, id);
    expect_value(__wrap_getReadResultNetworkDict, value, NULL);
    will_return(__wrap_getReadResultNetworkDict, SDO_UPLOAD_IN_PROGRESS);

    done = app_canopen_read_sdo(app, id, 0x12345678, NULL, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getReadResultNetworkDict, data, NULL);
    expect_value(__wrap_getReadResultNetworkDict, id, id);
    expect_value(__wrap_getReadResultNetworkDict, value, NULL);
    will_return(__wrap_getReadResultNetworkDict, SDO_FINISHED);

    done = app_canopen_read_sdo(app, id, 0x12345678, NULL, 0);
    assert_int_equal(done, ACTION_DONE);

    /* Check for errors on SDO request */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_readNetworkDict, data, NULL);
    expect_value(__wrap_readNetworkDict, id, id);
    expect_value(__wrap_readNetworkDict, idx, 0x8765);
    expect_value(__wrap_readNetworkDict, subidx, 0x43);
    will_return(__wrap_readNetworkDict, 1);

    expect_string(__wrap_warning, format, "SDO read error (id %u, entry %04X:%02X, result %X)");

    done = app_canopen_read_sdo(app, id, 0x87654321, NULL, 0);
    assert_int_equal(done, ACTION_ERROR);

    /* Check for errors on SDO response */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_readNetworkDict, data, NULL);
    expect_value(__wrap_readNetworkDict, id, id);
    expect_value(__wrap_readNetworkDict, idx, 0x1234);
    expect_value(__wrap_readNetworkDict, subidx, 0x56);
    will_return(__wrap_readNetworkDict, 0);

    done = app_canopen_read_sdo(app, id, 0x12345678, NULL, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getReadResultNetworkDict, data, NULL);
    expect_value(__wrap_getReadResultNetworkDict, id, id);
    expect_value(__wrap_getReadResultNetworkDict, value, NULL);
    will_return(__wrap_getReadResultNetworkDict, SDO_RESET);

    expect_string(__wrap_warning, format, "SDO read failed (id %u, entry %04X:%02X, result %X, abort code %X)");

    done = app_canopen_read_sdo(app, id, 0x12345678, NULL, 0);
    assert_int_equal(done, ACTION_ERROR);

    /* Check for timeout error */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_readNetworkDict, data, NULL);
    expect_value(__wrap_readNetworkDict, id, id);
    expect_value(__wrap_readNetworkDict, idx, 0x8765);
    expect_value(__wrap_readNetworkDict, subidx, 0x43);
    will_return(__wrap_readNetworkDict, 0);

    done = app_canopen_read_sdo(app, id, 0x87654321, NULL, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getReadResultNetworkDict, data, NULL);
    expect_value(__wrap_getReadResultNetworkDict, id, id);
    expect_value(__wrap_getReadResultNetworkDict, value, NULL);
    will_return(__wrap_getReadResultNetworkDict, SDO_UPLOAD_IN_PROGRESS);

    done = app_canopen_read_sdo(app, id, 0x87654321, NULL, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getReadResultNetworkDict, data, NULL);
    expect_value(__wrap_getReadResultNetworkDict, id, id);
    expect_value(__wrap_getReadResultNetworkDict, value, NULL);
    will_return(__wrap_getReadResultNetworkDict, SDO_UPLOAD_IN_PROGRESS);

    expect_string(__wrap_warning, format, "SDO read timeout (id %u, entry %04X:%02X)");

    done = app_canopen_read_sdo(app, id, 0x87654321, NULL, 0);
    assert_int_equal(done, ACTION_ERROR);

    app_canopen_free(app);
}

static void
test_canopen_write_sdo(void **state)
{
    App *app = *state;
    int done;
    uint8_t id = 123;

    will_return(__wrap_LoadCanDriver, 123);
    app_canopen_init(app, "1", 2);

    /* Set max scans to 1 and max errors to 0 to avoid repeated attempts */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    app_canopen_settings(app, id, -2, -2, 1, 0, -2);

    /* Check successful SDO write */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDict, data, NULL);
    expect_value(__wrap_writeNetworkDict, id, id);
    expect_value(__wrap_writeNetworkDict, idx, 0x1234);
    expect_value(__wrap_writeNetworkDict, subidx, 0x56);
    expect_value(__wrap_writeNetworkDict, count, 0x78 / 8);
    will_return(__wrap_writeNetworkDict, 0);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_DOWNLOAD_IN_PROGRESS);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_FINISHED);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_DONE);

    /* Check for errors on SDO request */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDict, data, NULL);
    expect_value(__wrap_writeNetworkDict, id, id);
    expect_value(__wrap_writeNetworkDict, idx, 0x8765);
    expect_value(__wrap_writeNetworkDict, subidx, 0x43);
    expect_value(__wrap_writeNetworkDict, count, 0x21 / 8);
    will_return(__wrap_writeNetworkDict, 1);

    expect_string(__wrap_warning, format, "SDO write error (id %u, entry %04X:%02X, value %0*X, result %X)");

    done = app_canopen_write_sdo(app, id, 0x87654321, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_ERROR);

    /* Check for errors on SDO response */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDict, data, NULL);
    expect_value(__wrap_writeNetworkDict, id, id);
    expect_value(__wrap_writeNetworkDict, idx, 0x1234);
    expect_value(__wrap_writeNetworkDict, subidx, 0x56);
    expect_value(__wrap_writeNetworkDict, count, 0x78 / 8);
    will_return(__wrap_writeNetworkDict, 0);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_RESET);

    expect_string(__wrap_warning, format, "SDO write failed (id %u, entry %04X:%02X, value %0*X, result %X, abort code %X)");

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_ERROR);

    /* Check for timeout error */
    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_writeNetworkDict, data, NULL);
    expect_value(__wrap_writeNetworkDict, id, id);
    expect_value(__wrap_writeNetworkDict, idx, 0x1234);
    expect_value(__wrap_writeNetworkDict, subidx, 0x56);
    expect_value(__wrap_writeNetworkDict, count, 0x78 / 8);
    will_return(__wrap_writeNetworkDict, 0);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_DOWNLOAD_IN_PROGRESS);

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_WAITING);

    expect_value(__wrap_GetSDOClientFromNodeId, data, NULL);
    expect_value(__wrap_GetSDOClientFromNodeId, id, id);
    will_return(__wrap_GetSDOClientFromNodeId, 0);

    expect_value(__wrap_getWriteResultNetworkDict, data, NULL);
    expect_value(__wrap_getWriteResultNetworkDict, id, id);
    will_return(__wrap_getWriteResultNetworkDict, SDO_DOWNLOAD_IN_PROGRESS);

    expect_string(__wrap_warning, format, "SDO write timeout (id %u, entry %04X:%02X)");

    done = app_canopen_write_sdo(app, id, 0x12345678, 0xDEADBEEF, 0);
    assert_int_equal(done, ACTION_ERROR);

    app_canopen_free(app);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_canopen_init_free),
        cmocka_unit_test(test_canopen_set_id),
        cmocka_unit_test(test_canopen_set_data),
        cmocka_unit_test(test_canopen_set_receiver),
        cmocka_unit_test(test_canopen_add_slave),
        cmocka_unit_test(test_canopen_start_stop),
        cmocka_unit_test(test_canopen_map_tpdo),
        cmocka_unit_test(test_canopen_map_rpdo),
        cmocka_unit_test(test_canopen_read_sdo),
        cmocka_unit_test(test_canopen_write_sdo),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
