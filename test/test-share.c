#include <stdarg.h>
#include <stddef.h>
#include <semaphore.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include "../src/share-private.h"

void
__wrap_die_with_errno(const char *caller)
{
    check_expected(caller);
}

void
__wrap_info(const char *format, ...)
{
    check_expected(format);
}

int
__wrap_ftruncate(int fildes, off_t length)
{
    check_expected(fildes);
    assert_true(length > 0);
    return mock_type(int);
}

int
__wrap_ftruncate64(int fildes, off_t length)
{
    return __wrap_ftruncate(fildes, length);
}

int
__wrap_fstat(int fildes, struct stat *buf)
{
    check_expected(fildes);
    assert_non_null(buf);
    buf->st_size = mock_type(int);
    return mock_type(int);
}

int
__wrap_fstat64(int fildes, struct stat *buf)
{
    return __wrap_fstat(fildes, buf);
}

int
__wrap___fxstat(int fildes, struct stat *buf)
{
    return __wrap_fstat(fildes, buf);
}

int
__wrap___fxstat64(int fildes, struct stat *buf)
{
    return __wrap_fstat(fildes, buf);
}

int
__wrap_close(int fildes)
{
    check_expected(fildes);
    return 0;
}

int
__wrap_kill(pid_t pid, int sig)
{
    check_expected(pid);
    check_expected(sig);
    return mock_type(int);
}

int
__wrap_shm_open(const char *name, int oflag, mode_t mode)
{
    check_expected(name);
    check_expected(oflag);
    check_expected(mode);
    return mock_type(int);
}

void *
__wrap_mmap(void *addr, size_t len, int prot, int flags, int fildes, off_t off)
{
    check_expected(addr);
    assert_true(len > 0);
    check_expected(prot);
    check_expected(flags);
    check_expected(fildes);
    check_expected(off);
    return mock_type(void *);
}

void *
__wrap_mmap64(void *addr, size_t len, int prot, int flags, int fildes, off_t off)
{
    return __wrap_mmap(addr, len, prot, flags, fildes, off);
}

int
__wrap_munmap(void *addr, size_t len)
{
    assert_non_null(addr);
    function_called();
    return 0;
}

int
__wrap_shm_unlink(const char *name)
{
    check_expected(name);
    return 0;
}

int
__wrap_sem_init(sem_t *sem, int pshared, unsigned int value)
{
    assert_non_null(sem);
    /* All semaphores must be shared across processes */
    assert_int_not_equal(pshared, 0);
    assert_int_equal(value, 1);
    function_called();
    return mock_type(int);
}

int
__wrap_sem_destroy(sem_t *sem)
{
    assert_non_null(sem);
    function_called();
    return 0;
}

int
__wrap_sem_post(sem_t *sem)
{
    assert_non_null(sem);
    function_called();
    return mock_type(int);
}

int
__wrap_sem_wait(sem_t *sem)
{
    assert_non_null(sem);
    function_called();
    return mock_type(int);
}

int
__wrap_sem_timedwait(sem_t *sem, const struct timespec *abs_timeout)
{
    assert_non_null(sem);
    assert_non_null(abs_timeout);
    function_called();
    return mock_type(int);
}

static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_share_master(void **state)
{
    App *app = *state;
    uint8_t map[1000] = { 0 };

    assert_null(app->share);

    /* Checking various failure types */
    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, -1);
    expect_string(__wrap_die_with_errno, caller, "shm_open");
    app_share_init(app, 2, 22, 33);
    assert_non_null(app->share);
    app_share_free(app);

    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_ftruncate, fildes, 123);
    will_return(__wrap_ftruncate, -1);
    expect_string(__wrap_die_with_errno, caller, "ftruncate");
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 2, 22, 33);
    assert_non_null(app->share);
    app_share_free(app);

    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_ftruncate, fildes, 123);
    will_return(__wrap_ftruncate, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, MAP_FAILED);
    expect_string(__wrap_die_with_errno, caller, "mmap");
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 2, 22, 33);
    assert_non_null(app->share);
    app_share_free(app);

    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_ftruncate, fildes, 123);
    will_return(__wrap_ftruncate, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, map);
    expect_function_call(__wrap_sem_init);
    will_return(__wrap_sem_init, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_init");
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 2, 22, 33);
    assert_non_null(app->share);
    expect_function_call(__wrap_munmap);
    expect_string(__wrap_info, format, "share: destroying SHM object");
    expect_string(__wrap_shm_unlink, name, "/test");
    app_share_free(app);

    /* Checking successful construction */
    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_ftruncate, fildes, 123);
    will_return(__wrap_ftruncate, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, map);
    expect_function_call(__wrap_sem_init);
    will_return(__wrap_sem_init, 0);
    expect_function_call(__wrap_sem_init);
    will_return(__wrap_sem_init, 0);
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 2, 22, 33);
    assert_non_null(app->share);

    /* Checking successful destruction */
    expect_function_call(__wrap_sem_destroy);
    expect_function_call(__wrap_sem_destroy);
    expect_function_call(__wrap_munmap);
    expect_string(__wrap_info, format, "share: destroying SHM object");
    expect_string(__wrap_shm_unlink, name, "/test");
    app_share_free(app);

    /* Checking idempotence: closing a closed share should be a no-op */
    app_share_free(app);
}

static void
test_share_slave(void **state)
{
    App *app = *state;
    uint8_t map[1000] = { 0 };

    assert_null(app->share);

    /* Checking various failure types */
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR);
    expect_value(__wrap_shm_open, mode, 0);
    will_return(__wrap_shm_open, -1);
    expect_string(__wrap_die_with_errno, caller, "shm_open");
    app_share_init(app, 0);
    assert_non_null(app->share);
    app_share_free(app);

    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR);
    expect_value(__wrap_shm_open, mode, 0);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_fstat, fildes, 123);
    will_return(__wrap_fstat, 50);
    will_return(__wrap_fstat, -1);
    expect_string(__wrap_die_with_errno, caller, "fstat");
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 0);
    assert_non_null(app->share);
    app_share_free(app);

    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR);
    expect_value(__wrap_shm_open, mode, 0);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_fstat, fildes, 123);
    will_return(__wrap_fstat, 50);
    will_return(__wrap_fstat, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, NULL);
    expect_string(__wrap_die_with_errno, caller, "mmap");
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 0);
    assert_non_null(app->share);
    app_share_free(app);

    /* Checking successful construction */
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR);
    expect_value(__wrap_shm_open, mode, 0);
    will_return(__wrap_shm_open, 123);
    expect_value(__wrap_fstat, fildes, 123);
    will_return(__wrap_fstat, 50);
    will_return(__wrap_fstat, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, map);
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 0);
    assert_non_null(app->share);

    /* Checking successful destruction */
    expect_function_call(__wrap_munmap);
    app_share_free(app);
    assert_null(app->share);

    /* Checking idempotence: closing a closed share should be a no-op */
    app_share_free(app);
}

static void
test_share_read_write(void **state)
{
    App *app = *state;
    char dst[10] = "dummytext";
    const char *src     = "012345678";
    const char *overlay = "AAAAAAAAA";
    uint8_t map[1000];
    const Zone zone = { 0, 10, { 0 }, };

    /* The tests need a working mmapped area */
    * (int *) map = 1;
    memcpy(map + sizeof(int), &zone, sizeof(zone));

    /* Checking successful construction */
    expect_string(__wrap_info, format, "share: creating SHM object");
    expect_string(__wrap_shm_open, name, "/test");
    expect_value(__wrap_shm_open, oflag, O_RDWR|O_CREAT|O_EXCL);
    expect_value(__wrap_shm_open, mode, 0660);
    will_return(__wrap_shm_open, 123);
    expect_function_call(__wrap_sem_init);
    will_return(__wrap_sem_init, 0);
    expect_value(__wrap_ftruncate, fildes, 123);
    will_return(__wrap_ftruncate, 0);
    expect_value(__wrap_mmap, addr, 0);
    expect_value(__wrap_mmap, prot, PROT_READ|PROT_WRITE);
    expect_value(__wrap_mmap, flags, MAP_SHARED);
    expect_value(__wrap_mmap, fildes, 123);
    expect_value(__wrap_mmap, off, 0);
    will_return(__wrap_mmap, &map);
    expect_value(__wrap_close, fildes, 123);
    app_share_init(app, 1, sizeof(dst));
    assert_non_null(app->share);

    /* Checking different kind of failures */
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_wait");
    assert_false(app_share_write(app, 0, src, NULL, 0));

    expect_function_call(__wrap_sem_timedwait);
    will_return(__wrap_sem_timedwait, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_timedwait");
    assert_false(app_share_write(app, 0, src, NULL, 43210));

    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_post");
    /* Errors in sem_post are *not* considered failures because the data has
     * supposedly been already written */
    assert_true(app_share_write(app, 0, src, NULL, 0));

    /* Checking successful writes */
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_write(app, 0, src, NULL, 0));

    expect_function_call(__wrap_sem_timedwait);
    will_return(__wrap_sem_timedwait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_write(app, 0, src, NULL, 12345));

    /* Check successful reading */
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_read(app, 0, dst, NULL, 0));
    assert_string_equal(dst, src);

    /* Check errors in reading */
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_wait");
    assert_false(app_share_read(app, 0, dst, NULL, 0));

    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, -1);
    expect_string(__wrap_die_with_errno, caller, "sem_post");
    assert_false(app_share_read(app, 0, dst, NULL, 0));

    /* Check masked reading and writing */
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_write(app, 0, overlay,
                                (const uint8_t *) "\xFF\x00\x00\x00\x00\x00\x00\x00\x00\x00",
                                0));
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_read(app, 0, dst, NULL, 0));
    assert_string_equal(dst, "A12345678");

    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_write(app, 0, overlay,
                                (const uint8_t *) "\x00\x00\x0F\xF2\x69\x00\x00\x00\x00\x00",
                                0));
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_read(app, 0, dst, NULL, 0));
    assert_string_equal(dst, "A11AU5678");

    strcpy((char *) dst, "         ");
    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_read(app, 0, dst,
                               (const uint8_t *) "\xFF\x00\x00\x00\x00\x00\x00\xFF\x00\x00",
                               0));
    assert_string_equal(dst, "A      7 ");

    expect_function_call(__wrap_sem_wait);
    will_return(__wrap_sem_wait, 0);
    expect_function_call(__wrap_sem_post);
    will_return(__wrap_sem_post, 0);
    assert_true(app_share_read(app, 0, dst,
                               (const uint8_t *) "\x00\x03\x42\x0F\x60\x00\x00\x00\x00\x00",
                               0));
    assert_string_equal(dst, "A! !@  7 ");

    expect_function_call(__wrap_sem_destroy);
    expect_function_call(__wrap_munmap);
    expect_string(__wrap_info, format, "share: destroying SHM object");
    expect_string(__wrap_shm_unlink, name, "/test");
    app_share_free(app);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(test_share_master, setup, teardown),
        cmocka_unit_test_setup_teardown(test_share_slave, setup, teardown),
        cmocka_unit_test_setup_teardown(test_share_read_write, setup, teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
