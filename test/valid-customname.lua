-- Similar to `valid.lua` but using the "customname" table instead.
local n = 0
return function ()
    customname.set_value(n)
    n = n + 1
end
