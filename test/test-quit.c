#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <syslog.h>

static void
quit_handler(AppStatus status)
{
    check_expected(status);
}

static void
test_quit(void **state)
{
    quit_set_handler(quit_handler);

    expect_value(quit_handler, status, APP_INVALID);
    quit(APP_INVALID);

    expect_value(quit_handler, status, APP_FAILURE);
    quit(APP_FAILURE);

    expect_value(quit_handler, status, APP_SUCCESS);
    quit(APP_SUCCESS);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_quit),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
