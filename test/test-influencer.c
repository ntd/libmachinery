#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>

static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_influencer_init_free(void **state)
{
    App *app = *state;

    assert_null(app->influencer);
    app_influencer_init(app);
    assert_non_null(app->influencer);
    app_influencer_free(app);
    assert_null(app->influencer);
    app_influencer_free(app);
    assert_null(app->influencer);
}

static void
test_influencer_push_pop(void **state)
{
    App *app = *state;
    const char *action;
    const char *channel;
    double data;

    app_influencer_init(app);

    action = "non-null string";
    app_influencer_pop(app, &action, &channel, &data);
    assert_null(action);
    action = "non-null string";
    app_influencer_pop(app, &action, NULL, NULL);
    assert_null(action);

    app_influencer_push(app, "first", NULL, 4);
    app_influencer_push(app, "second", "action", 3);
    app_influencer_push(app, "third", "action", 2);
    app_influencer_push(app, "forth", NULL, 1);

    action = NULL;
    channel = "non-null string";
    app_influencer_pop(app, &action, &channel, &data);
    assert_non_null(action);
    assert_null(channel);
    assert_float_equal(4, data, 0.1);

    action = NULL;
    channel = NULL;
    app_influencer_pop(app, &action, &channel, NULL);
    assert_non_null(action);
    assert_non_null(channel);

    action = NULL;
    app_influencer_pop(app, &action, NULL, &data);
    assert_non_null(action);
    assert_float_equal(2, data, 0.1);

    action = NULL;
    channel = "non-null string";
    app_influencer_pop(app, &action, &channel, &data);
    assert_non_null(action);
    assert_null(channel);
    assert_float_equal(1, data, 0.1);

    app_influencer_pop(app, &action, &channel, &data);
    assert_null(action);

    app_influencer_free(app);
}

static void
test_influencer_queued(void **state)
{
    App *app = *state;
    const char *action;

    app_influencer_init(app);

    assert_int_equal(0, app_influencer_queued(app));
    app_influencer_push(app, "1", NULL, 1);
    assert_int_equal(1, app_influencer_queued(app));
    app_influencer_push(app, "2", NULL, 0);
    assert_int_equal(2, app_influencer_queued(app));

    app_influencer_pop(app, &action, NULL, NULL);
    assert_int_equal(1, app_influencer_queued(app));
    app_influencer_pop(app, &action, NULL, NULL);
    assert_int_equal(0, app_influencer_queued(app));

    app_influencer_free(app);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_influencer_init_free),
        cmocka_unit_test(test_influencer_push_pop),
        cmocka_unit_test(test_influencer_queued),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
