#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <syslog.h>

void
__wrap_openlog(const char *ident, int option, int facility)
{
    check_expected(ident);
    check_expected(option);
    check_expected(facility);
}

static void
test_init_free(void **state)
{
    App app;

    expect_string(__wrap_openlog, ident, "test");
    expect_value(__wrap_openlog, option, LOG_CONS);
    expect_value(__wrap_openlog, facility, LOG_USER);
    app_init(&app, "test");

    assert_string_equal(app.name, "test");
    assert_null(app.share);
    assert_null(app.lua);
    assert_null(app.canopen);

    app_free(&app);

    assert_null(app.name);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_init_free),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
