#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <syslog.h>

/*
 * If stack checking is enabled, __vsyslog_chk() is called instead of
 * vsyslog(). Not sure this is portable though: it surely works on Ubuntu
 * 16.04 and ArchLinux.
 */
#if __USE_FORTIFY_LEVEL > 0 && defined __fortify_function
#define SYSLOG __wrap___vsyslog_chk

void
__wrap___vsyslog_chk(int priority, int flag, const char *format, va_list ap)
{
    check_expected(priority);
    check_expected(format);
}

#else
#define SYSLOG __wrap_vsyslog

void
__wrap_vsyslog(int priority, const char *format, va_list ap)
{
    check_expected(priority);
    check_expected(format);
}

#endif

void
__wrap_quit(int status)
{
    check_expected(status);
}

static void
test_debug(void **state)
{
    expect_value(SYSLOG, priority, LOG_DEBUG);
    expect_string(SYSLOG, format, "Some debug text");
    debug("Some debug text");

    expect_value(SYSLOG, priority, LOG_DEBUG);
    expect_string(SYSLOG, format, "%s");
    debug("%s", "Test");
}

static void
test_info(void **state)
{
    expect_value(SYSLOG, priority, LOG_INFO);
    expect_string(SYSLOG, format, "Test");
    info("Test");

    expect_value(SYSLOG, priority, LOG_INFO);
    expect_string(SYSLOG, format, "%s");
    info("%s", "Test");
}

static void
test_warning(void **state)
{
    expect_value(SYSLOG, priority, LOG_WARNING);
    expect_string(SYSLOG, format, "Warning message");
    warning("Warning message");

    expect_value(SYSLOG, priority, LOG_WARNING);
    expect_string(SYSLOG, format, "%s");
    warning("%s", "Test");
}

static void
test_die(void **state)
{
    expect_value(__wrap_quit, status, APP_FAILURE);
    die(NULL);

    expect_value(SYSLOG, priority, LOG_ERR);
    expect_string(SYSLOG, format, "Test");
    expect_value(__wrap_quit, status, APP_FAILURE);
    die("Test");

    expect_value(SYSLOG, priority, LOG_ERR);
    expect_string(SYSLOG, format, "%s");
    expect_value(__wrap_quit, status, APP_FAILURE);
    die("%s", "Test");
}

static void
test_die_with_errno(void **state)
{
    expect_value(SYSLOG, priority, LOG_ERR);
    expect_string(SYSLOG, format, "%s");
    expect_value(__wrap_quit, status, APP_FAILURE);
    die_with_errno(NULL);

    expect_value(SYSLOG, priority, LOG_ERR);
    expect_string(SYSLOG, format, "%s error: %s");
    expect_value(__wrap_quit, status, APP_FAILURE);
    die_with_errno("prefix");
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_debug),
        cmocka_unit_test(test_info),
        cmocka_unit_test(test_warning),
        cmocka_unit_test(test_die),
        cmocka_unit_test(test_die_with_errno),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
