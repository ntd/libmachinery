#include <machinery.h>
#include <ramp.h>

#include <stdarg.h>
#include <stddef.h>
#include <semaphore.h>
#include <setjmp.h>
#include <cmocka.h>


static int
setup(void **state)
{
    *state = machinery_ramp_new();
    return 0;
}

static int
teardown(void **state)
{
    MachineryRamp *ramp = *state;
    g_object_unref(G_OBJECT(ramp));
    return 0;
}

static void
base(void **state)
{
    /* Check basic construction/desctruction operations */
    MachineryRamp *ramp = machinery_ramp_new();
    assert_non_null(ramp);
    assert_true(MACHINERY_IS_RAMP(ramp));
    g_object_unref(ramp);
}

static void
min_speed(void **state)
{
    MachineryRamp *ramp = *state;
    guint speed;

    machinery_ramp_get_min_speed(NULL);
    machinery_ramp_set_min_speed(NULL, 0);

    /* Check the default minimum speed is 0 */
    speed = machinery_ramp_get_min_speed(ramp);
    assert_int_equal(speed, 0);

    /* Check common setter behavior */
    machinery_ramp_set_min_speed(NULL, 1234);
    speed = machinery_ramp_get_min_speed(ramp);
    assert_int_equal(speed, 0);
    machinery_ramp_set_min_speed(ramp, 1234);
    speed = machinery_ramp_get_min_speed(ramp);
    assert_int_equal(speed, 1234);
    machinery_ramp_set_min_speed(ramp, 0);
    speed = machinery_ramp_get_min_speed(ramp);
    assert_int_equal(speed, 0);
}

static void
max_speed(void **state)
{
    MachineryRamp *ramp = *state;
    guint speed;

    machinery_ramp_get_max_speed(NULL);
    machinery_ramp_set_max_speed(NULL, 0);

    /* Check the default maximum speed is 1000 */
    speed = machinery_ramp_get_max_speed(ramp);
    assert_int_equal(speed, 1000);

    /* Check common setter behavior */
    machinery_ramp_set_max_speed(NULL, 1234);
    speed = machinery_ramp_get_max_speed(ramp);
    assert_int_equal(speed, 1000);
    machinery_ramp_set_max_speed(ramp, 1234);
    speed = machinery_ramp_get_max_speed(ramp);
    assert_int_equal(speed, 1234);
    machinery_ramp_set_max_speed(ramp, 0);
    speed = machinery_ramp_get_max_speed(ramp);
    assert_int_equal(speed, 0);
}

static void
speed(void **state)
{
    MachineryRamp *ramp = *state;
    guint speed;

    machinery_ramp_get_speed(NULL);

    /* Check the default speed is 0 */
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 0);
}

static void
acceleration(void **state)
{
    MachineryRamp *ramp = *state;
    guint speed;

    machinery_ramp_get_acceleration(NULL);
    machinery_ramp_set_acceleration(NULL, 0);

    /* Check the default acceleration is 500 */
    speed = machinery_ramp_get_acceleration(ramp);
    assert_int_equal(speed, 500);

    /* Check common setter behavior */
    machinery_ramp_set_acceleration(NULL, 1234);
    speed = machinery_ramp_get_acceleration(ramp);
    assert_int_equal(speed, 500);
    machinery_ramp_set_acceleration(ramp, 1234);
    speed = machinery_ramp_get_acceleration(ramp);
    assert_int_equal(speed, 1234);
    machinery_ramp_set_acceleration(ramp, 0);
    speed = machinery_ramp_get_acceleration(ramp);
    assert_int_equal(speed, 0);
}

static void
deceleration(void **state)
{
    MachineryRamp *ramp = *state;
    guint speed;

    machinery_ramp_get_deceleration(NULL);
    machinery_ramp_set_deceleration(NULL, 0);

    /* Check the default deceleration is 500 */
    speed = machinery_ramp_get_deceleration(ramp);
    assert_int_equal(speed, 500);

    /* Check common setter behavior */
    machinery_ramp_set_deceleration(NULL, 1234);
    speed = machinery_ramp_get_deceleration(ramp);
    assert_int_equal(speed, 500);
    machinery_ramp_set_deceleration(ramp, 1234);
    speed = machinery_ramp_get_deceleration(ramp);
    assert_int_equal(speed, 1234);
    machinery_ramp_set_deceleration(ramp, 0);
    speed = machinery_ramp_get_deceleration(ramp);
    assert_int_equal(speed, 0);
}

static void
move(void **state)
{
    MachineryRamp *ramp = *state;

    machinery_ramp_move(NULL, 0);
    machinery_ramp_move(NULL, 1234);

    machinery_ramp_move(ramp, 0);
    machinery_ramp_move(ramp, 1234);
}

static void
update_position(void **state)
{
    MachineryRamp *ramp = *state;

    assert_false(machinery_ramp_update_position(NULL, 0));

    /* Check the ramp is at 0 position by default */
    assert_true(machinery_ramp_update_position(ramp, 0));

    /* Check the ramp is not at 1234 position */
    assert_false(machinery_ramp_update_position(ramp, 1234));
}

static void
speed_loop(void **state)
{
    MachineryRamp *ramp = *state;
    gint speed, last;

    /* Construct an asymmetric ramp for better introspection */
    machinery_ramp_set_min_speed(ramp, 200);
    machinery_ramp_set_max_speed(ramp, 1000);
    machinery_ramp_set_acceleration(ramp, 8000);
    machinery_ramp_set_deceleration(ramp, 10000);

    /* Check a full movement */
    machinery_ramp_move(ramp, 1000);

    /* Check for the minimum speed limit */
    assert_false(machinery_ramp_update_position(ramp, 0));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 200);

    /* Check the speed in the rising ramp */
    g_usleep(25000);
    assert_false(machinery_ramp_update_position(ramp, 250));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, last + 10, 900);

    g_usleep(25000);
    assert_false(machinery_ramp_update_position(ramp, 500));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, last + 100, 900);

    /* Now it should be at the maximum speed */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 750));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 1000);

    /* Check the falling slope: with a deceleration of 10000, it needs
     * at least a space of 50 to decelerate from 1000 */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 950));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 1000);

    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 995));
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, 200, 900);

    /* Check for the minimum speed limit */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 999));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 200);

    /* Check for destination reached */
    assert_true(machinery_ramp_update_position(ramp, 1000));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 0);

    /* Check reverse direction */
    machinery_ramp_move(ramp, 0);

    /* Check for the minimum speed limit */
    assert_false(machinery_ramp_update_position(ramp, 1000));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, -200);

    /* Check the speed in the rising ramp */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 750));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, -1000, last - 10);

    /* Change the destination dynamically: the speed must be reverted
     * with ramp immediately */
    machinery_ramp_move(ramp, 1000);
    assert_false(machinery_ramp_update_position(ramp, 750));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, last - 5, last + 5);

    g_usleep(25000);
    assert_false(machinery_ramp_update_position(ramp, 700));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, last, -200);

    /* Check that the speed is reverted */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 800));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, 200, 500);

    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 980));
    last = speed;
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, last + 10, 1000);

    /* Check falling slope */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 995));
    speed = machinery_ramp_get_speed(ramp);
    assert_in_range(speed, 200, 500);

    /* Check for the minimum speed limit */
    g_usleep(50000);
    assert_false(machinery_ramp_update_position(ramp, 999));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 200);

    /* Check for destination reached */
    assert_true(machinery_ramp_update_position(ramp, 1000));
    speed = machinery_ramp_get_speed(ramp);
    assert_int_equal(speed, 0);
}

static void
nop(void)
{
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(base),
        cmocka_unit_test_setup_teardown(min_speed, setup, teardown),
        cmocka_unit_test_setup_teardown(max_speed, setup, teardown),
        cmocka_unit_test_setup_teardown(speed, setup, teardown),
        cmocka_unit_test_setup_teardown(acceleration, setup, teardown),
        cmocka_unit_test_setup_teardown(deceleration, setup, teardown),
        cmocka_unit_test_setup_teardown(move, setup, teardown),
        cmocka_unit_test_setup_teardown(update_position, setup, teardown),
        cmocka_unit_test_setup_teardown(speed_loop, setup, teardown),
    };

    /* Shut up internal assertions: I need to check them too
     * without cluttering stderr */
    g_log_set_default_handler((GLogFunc) nop, NULL);

    return cmocka_run_group_tests(tests, NULL, NULL);
}
