#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <signal.h>
#include <stdlib.h>

typedef const char*(* daemon_pid_file_proc_t)(void);

const char *daemon_log_ident;
const char *daemon_pid_file_ident;
daemon_pid_file_proc_t daemon_pid_file_proc;

void
__wrap_die_with_errno(const char *caller)
{
    check_expected(caller);
}

int
__wrap_daemon_signal_init(int s, ...)
{
    return mock_type(int);
}

void
__wrap_daemon_signal_done(void)
{
    function_called();
}

pid_t
__wrap_daemon_fork(void)
{
    return 1;
}

int
__wrap_daemon_retval_init(void)
{
    return mock_type(int);
}

int
__wrap_daemon_retval_wait(int timeout)
{
    return 0;
}

int
__wrap_daemon_retval_send(int s)
{
    check_expected(s);
    return 0;
}

void
__wrap_daemon_retval_done(void)
{
}

int
__wrap_daemon_reset_sigs(int except, ...)
{
    check_expected(except);
    return mock_type(int);
}

int
__wrap_daemon_unblock_sigs(int except, ...)
{
    check_expected(except);
    return mock_type(int);
}

int
__wrap_daemon_pid_file_create(void)
{
    return 0;
}

pid_t
__wrap_daemon_pid_file_is_running(void)
{
    return mock_type(int);
}

int
__wrap_daemon_pid_file_remove(void)
{
    function_called();
    return 0;
}

int
__wrap_daemon_pid_file_kill_wait(int s, int m)
{
    check_expected(s);
    check_expected(m);
    return 0;
}

int
__wrap_daemon_close_all(int except, ...)
{
    check_expected(except);
    return mock_type(int);
}

static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_daemon_init_free(void **state)
{
    App *app = *state;
    char *path;
    long uid;

    /* Avoid to depend on custom environment */
    unsetenv("XDG_RUNTIME_DIR");

    /* Check for initialization errors */
    expect_value(__wrap_daemon_reset_sigs, except, -1);
    will_return(__wrap_daemon_reset_sigs, -1);
    expect_string(__wrap_die_with_errno, caller, "daemon_reset_sigs");
    app_daemon_init(app, NULL);

    expect_value(__wrap_daemon_reset_sigs, except, -1);
    will_return(__wrap_daemon_reset_sigs, 0);
    expect_value(__wrap_daemon_unblock_sigs, except, -1);
    will_return(__wrap_daemon_unblock_sigs, -1);
    expect_string(__wrap_die_with_errno, caller, "daemon_unblock_sigs");
    app_daemon_init(app, NULL);

    /* Check successful initialization */
    uid = (long) getuid();
    expect_value(__wrap_daemon_reset_sigs, except, -1);
    will_return(__wrap_daemon_reset_sigs, 0);
    expect_value(__wrap_daemon_unblock_sigs, except, -1);
    will_return(__wrap_daemon_unblock_sigs, 0);
    app_daemon_init(app, NULL);
    assert_string_equal(daemon_log_ident, "test");
    if (uid == 0) {
        path = malloc_printf("/var/run/test.pid", (int) getuid());
    } else {
        path = malloc_printf("/var/run/%ld/test.pid", uid);
    }
    assert_string_equal(daemon_pid_file_ident, path);
    free(path);

    expect_value(__wrap_daemon_retval_send, s, APP_INVALID);
    expect_function_call(__wrap_daemon_signal_done);
    expect_function_call(__wrap_daemon_pid_file_remove);
    app_daemon_free(app);

    /* Check module support and custom environment */
    putenv("XDG_RUNTIME_DIR=/custom/location");

    expect_value(__wrap_daemon_reset_sigs, except, -1);
    will_return(__wrap_daemon_reset_sigs, 0);
    expect_value(__wrap_daemon_unblock_sigs, except, -1);
    will_return(__wrap_daemon_unblock_sigs, 0);
    app_daemon_init(app, "module");
    assert_string_equal(daemon_log_ident, "test-module");
    assert_string_equal(daemon_pid_file_ident, "/custom/location/test-module.pid");

    expect_value(__wrap_daemon_retval_send, s, APP_INVALID);
    expect_function_call(__wrap_daemon_signal_done);
    expect_function_call(__wrap_daemon_pid_file_remove);
    app_daemon_free(app);
    assert_null(app->module);
    assert_null(app->pid);

    /* Check app_daemon_free idempotency */
    app_daemon_free(app);
}

static void
test_daemon_kill(void **state)
{
    App *app = *state;

    expect_value(__wrap_daemon_pid_file_kill_wait, s, SIGTERM);
    expect_value(__wrap_daemon_pid_file_kill_wait, m, 5);
    app_daemon_kill(app);
}

static void
test_daemon_start(void **state)
{
    App *app = *state;
    int result;

    /* Check daemon already running */
    will_return(__wrap_daemon_pid_file_is_running, 1);
    result = app_daemon_start(app);
    assert_int_equal(result, 0);

    /* Check successful workflow */
    will_return(__wrap_daemon_pid_file_is_running, -1);
    will_return(__wrap_daemon_retval_init, 0);
    will_return(__wrap_daemon_fork, 0);
    expect_value(__wrap_daemon_close_all, except, -1);
    will_return(__wrap_daemon_close_all, 0);
    will_return(__wrap_daemon_pid_file_create, 0);
    will_return(__wrap_daemon_signal_init, 0);
    expect_value(__wrap_daemon_retval_send, s, APP_SUCCESS);
    result = app_daemon_start(app);
    assert_int_equal(result, 1);

    /* Check error reporting */
    will_return(__wrap_daemon_pid_file_is_running, -1);
    will_return(__wrap_daemon_retval_init, -1);
    expect_string(__wrap_die_with_errno, caller, "daemon_retval_init");
    will_return(__wrap_daemon_fork, -1);
    expect_string(__wrap_die_with_errno, caller, "daemon_fork");
    expect_value(__wrap_daemon_close_all, except, -1);
    will_return(__wrap_daemon_close_all, -1);
    expect_value(__wrap_daemon_retval_send, s, APP_FAILURE);
    expect_string(__wrap_die_with_errno, caller, "daemon_close_all");
    will_return(__wrap_daemon_pid_file_create, -1);
    expect_value(__wrap_daemon_retval_send, s, APP_FAILURE);
    expect_string(__wrap_die_with_errno, caller, "daemon_pid_file_create");
    will_return(__wrap_daemon_signal_init, -1);
    expect_value(__wrap_daemon_retval_send, s, APP_FAILURE);
    expect_string(__wrap_die_with_errno, caller, "daemon_signal_int");
    result = app_daemon_start(app);
    assert_int_equal(result, 1);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_daemon_init_free),
        cmocka_unit_test(test_daemon_kill),
        cmocka_unit_test(test_daemon_start),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
