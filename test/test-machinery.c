#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <stdlib.h>
#include <syslog.h>

static void
test_malloc_printf(void **state)
{
    char *text = malloc_printf("Test: %s, %d", "string", 123);
    assert_string_equal(text, "Test: string, 123");
    free(text);
}

static void
test_hash(void **state)
{
    int n;
    struct {
        uint32_t    hash;
        const char *text;
    } data[] = {
        { 0x023bc04c, "rulliera_frenatura" },
        { 0x102a238d, "prova" },
        { 0x14365c55, "pagina" },
        { 0x1a2fa0d3, "rilascio_elemento" },
        { 0x1fd53484, "tastatore_minimo" },
        { 0x208db24e, "rulliera_discriminante" },
        { 0x25f9efa2, "rulliera_pezzo_corto" },
        { 0x26a003c0, "rulliera_pezzo_lungo" },
        { 0x2b8032b7, "tastatore_riposo" },
        { 0x2f0b5051, "avvio_prova" },
        { 0x30379fa4, "manipolatore_accelerazione" },
        { 0x3098bd16, "rulliera_inerzia" },
        { 0x318dfb46, "centralina" },
        { 0x40131e79, "rulliera_standby" },
        { 0x45562d0f, "manipolatore_zero" },
        { 0x48477153, "etichettatrice2_abilitata" },
        { 0x4abfb059, "indietro_tastatore" },
        { 0x4af7d9e2, "indietro_rulliera" },
        { 0x596c444d, "salita_manipolatore" },
        { 0x62376f4b, "discesa_manipolatore" },
        { 0x7ac56db6, "cappello_morsa" },
        { 0x7c2e580f, "posizione_manipolatore" },
        { 0x803b76e9, "manipolatore_decelerazione" },
        { 0x823c74c6, "rulliera_lento" },
        { 0x84a86bc8, "tastatore_m" },
        { 0x84a86bcc, "tastatore_q" },
        { 0x87b13bfe, "avanti_tastatore" },
        { 0x8eeccf76, "chiusura_guide" },
        { 0x963135c7, "manipolatore_ricerca_zero" },
        { 0x9a6404e7, "avanti_rulliera" },
        { 0x9d58f122, "etichetta_inferiore" },
        { 0x9fe91c50, "controllo_cappello" },
        { 0xa1c88cdd, "etichetta_superiore" },
        { 0xaac49369, "serraggio_morsa" },
        { 0xc193d774, "guide_morsa" },
        { 0xc22bb179, "rulliera_accelerazione" },
        { 0xc2eabc12, "etichettatrice1_abilitata" },
        { 0xc986fa17, "rulliera_lavoro" },
        { 0xd2a752a2, "manipolatore_lavoro" },
        { 0xd69fc57a, "manipolatore_quota10" },
        { 0xd69fc57b, "manipolatore_quota11" },
        { 0xd69fc57c, "manipolatore_quota12" },
        { 0xd69fc57d, "manipolatore_quota13" },
        { 0xd69fc57e, "manipolatore_quota14" },
        { 0xd69fc57f, "manipolatore_quota15" },
        { 0xd69fc580, "manipolatore_quota16" },
        { 0xd9f89056, "manipolatore_offset" },
        { 0xdfb74409, "manipolatore_quota0" },
        { 0xdfb7440a, "manipolatore_quota1" },
        { 0xdfb7440b, "manipolatore_quota2" },
        { 0xdfb7440c, "manipolatore_quota3" },
        { 0xdfb7440d, "manipolatore_quota4" },
        { 0xdfb7440e, "manipolatore_quota5" },
        { 0xdfb7440f, "manipolatore_quota6" },
        { 0xdfb74410, "manipolatore_quota7" },
        { 0xdfb74411, "manipolatore_quota8" },
        { 0xdfb74412, "manipolatore_quota9" },
        { 0xeebaf638, "presa_elemento" },
    };

    for (n = 0; n < sizeof(data) / sizeof(data[0]); ++n) {
        assert_int_equal(get_hash(data[n].text), data[n].hash);
    }
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_malloc_printf),
        cmocka_unit_test(test_hash),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
