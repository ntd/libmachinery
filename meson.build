project('libmachinery', 'c',
        version: '0.5.5',
        license: 'LGPLv2.1+',
        meson_version: '>= 0.47.0')

libmachinery_metadata = {
    'description': 'A library for speeding up development of industrial machineries',
    'url': 'https://codeberg.org/ntd/libmachinery',
    # How to handle LT versions (current:revision:age):
    # - If the library source code has changed at all since the last
    #   update, then increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
    # - If any interfaces have been added, removed, or changed since the
    #   last update, increment current, and set revision to 0.
    # - If any interfaces have been added since the last public release,
    #   then increment age.
    # - If any interfaces have been removed or changed since the last
    #   public release, then set age to 0.
    'soversion': '21:0:5',
}

cc = meson.get_compiler('c')

pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())
top_srcdir = meson.current_source_dir()

# Dependencies
libm            = cc.find_library('m')
librt           = cc.find_library('rt')
thread_dep      = dependency('threads')
daemon_dep      = dependency('libdaemon', required: get_option('daemon'))
serialport_dep  = dependency('libserialport', required: get_option('grbl'))
modbus_dep      = dependency('libmodbus', required: get_option('modbus'))
canfestival_dep = dependency('canfestival', required: get_option('canopen'))
ethercat_dep    = dependency('libethercat', required: get_option('ethercat'))
gobject_dep     = dependency('gobject-2.0')
gtk3_dep        = dependency('gtk+-3.0', required: get_option('gtk3'))
cmocka_dep      = dependency('cmocka', required: get_option('test'))

if not get_option('scripting').disabled()
    # Finding the right Lua name is more complex
    foreach name: ['lua5.4', 'lua-5.4', 'lua54',
                   'lua5.3', 'lua-5.3', 'lua53',
                   'lua5.2', 'lua-5.2', 'lua52', 'lua']
        lua_dep = dependency(name, required: false)
        if lua_dep.found()
            break
        endif
    endforeach
    if get_option('scripting').enabled() and not lua_dep.found()
        error('Lua could not be found!')
    endif
else
    lua_dep = dependency('', required: false)
endif

# External programs
doxygen = find_program('doxygen', required: get_option('doxygen'))

# Subdirectories
subdir('src')
if ethercat_dep.found() and lua_dep.found()
    subdir('demo')
endif
if doxygen.found()
    subdir('doc')
endif
if cmocka_dep.found()
    subdir('test')
endif
