#ifndef MACHINERY_GRBL_H_
#define MACHINERY_GRBL_H_

#include "gobject.h"

#define MACHINERY_TYPE_GRBL             (machinery_grbl_get_type())
#define MACHINERY_GRBL(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), MACHINERY_TYPE_GRBL, MachineryGrbl))
#define MACHINERY_GRBL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), MACHINERY_TYPE_GRBL, MachineryGrblClass))
#define MACHINERY_IS_GRBL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), MACHINERY_TYPE_GRBL))
#define MACHINERY_IS_GRBL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), MACHINERY_TYPE_GRBL))
#define MACHINERY_GRBL_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), MACHINERY_TYPE_GRBL, MachineryGrblClass))


G_BEGIN_DECLS

typedef struct _MachineryGrbl       MachineryGrbl;
typedef struct _MachineryGrblClass  MachineryGrblClass;

struct _MachineryGrbl {
    GObject         parent;
};

struct _MachineryGrblClass {
    GObjectClass    parent_class;
};

typedef enum {
    MACHINERY_GRBL_STATUS_UNKNOWN,
    MACHINERY_GRBL_STATUS_NONE,
    MACHINERY_GRBL_STATUS_IDLE,
    MACHINERY_GRBL_STATUS_RUN,
    MACHINERY_GRBL_STATUS_HOLD,
    MACHINERY_GRBL_STATUS_JOG,
    MACHINERY_GRBL_STATUS_ALARM,
    MACHINERY_GRBL_STATUS_DOOR,
    MACHINERY_GRBL_STATUS_CHECK,
    MACHINERY_GRBL_STATUS_HOME,
    MACHINERY_GRBL_STATUS_SLEEP,
} MachineryGrblStatus;


MachineryGrbl * machinery_grbl_new          (void);
MachineryGrbl * machinery_grbl_new_full     (const gchar *      device,
                                             guint              baudrate);
void            machinery_grbl_set_device   (MachineryGrbl *    grbl,
                                             const gchar *      device);
const gchar *   machinery_grbl_get_device   (MachineryGrbl *    grbl);
void            machinery_grbl_set_baudrate (MachineryGrbl *    grbl,
                                             guint              baudrate);
guint           machinery_grbl_get_baudrate (MachineryGrbl *    grbl);
gboolean        machinery_grbl_start        (MachineryGrbl *    grbl);
void            machinery_grbl_stop         (MachineryGrbl *    grbl);
gboolean        machinery_grbl_command      (MachineryGrbl *    grbl,
                                             guint              timeout,
                                             const gchar *      format,
                                             ...);
gboolean        machinery_grbl_ready        (MachineryGrbl *    grbl);
gchar *         machinery_grbl_get_error    (MachineryGrbl *    grbl);
gint            machinery_grbl_alarm        (MachineryGrbl *    grbl);
MachineryGrblStatus
                machinery_grbl_status       (MachineryGrbl *    grbl);
gdouble         machinery_grbl_x            (MachineryGrbl *    grbl);
gdouble         machinery_grbl_y            (MachineryGrbl *    grbl);
gdouble         machinery_grbl_z            (MachineryGrbl *    grbl);

G_END_DECLS

#endif /* MACHINERY_GRBL_H_ */
