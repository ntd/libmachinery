#ifndef MACHINERY_ETHERCAT_SLAVES_H_
#define MACHINERY_ETHERCAT_SLAVES_H_

#ifdef __cplusplus
extern "C" {
#endif

const void *    machinery_ethercat_slave_ek1100             (void);
const void *    machinery_ethercat_slave_ek1110             (void);
const void *    machinery_ethercat_slave_el1808             (void);
const void *    machinery_ethercat_slave_el1809             (void);
const void *    machinery_ethercat_slave_el2808             (void);
const void *    machinery_ethercat_slave_el2809             (void);
const void *    machinery_ethercat_slave_el3164             (void);
const void *    machinery_ethercat_slave_el4104             (void);
const void *    machinery_ethercat_slave_el7047p            (void);
const void *    machinery_ethercat_slave_el7047pi           (void);
const void *    machinery_ethercat_slave_hdt_pp             (void);
const void *    machinery_ethercat_slave_hdt_full           (void);
const void *    machinery_ethercat_slave_ntt_pp             (void);
const void *    machinery_ethercat_slave_ntt_full           (void);
const void *    machinery_ethercat_slave_atv320_vm          (void);
const void *    machinery_ethercat_slave_dms                (void);
const void *    machinery_ethercat_slave_dms4331            (void);

#ifdef __cplusplus
}
#endif

#endif /* MACHINERY_ETHERCAT_SLAVES_H_ */
