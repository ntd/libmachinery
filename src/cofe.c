#include "machinery.h"
#include "od/Dummy.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEVICE
#    define DEVICE   "can0"
#endif
#ifndef BAUDRATE
#    define BAUDRATE 500000
#endif
#ifndef MASTERID
#    define MASTERID 1
#endif


typedef enum {
    COMMAND_NONE,
    COMMAND_NMT,
    COMMAND_SDO_UPLOAD,
    COMMAND_SDO_DOWNLOAD,
    COMMAND_SYNC,
} Command;


static void
bailout(const char *message)
{
    printf("Error: %s.\n"
           "\n"
           "SYNOPSIS\n"
           "    machinery-cofe [-d DEVICE] [-b BAUDRATE] nmt NODE SERVICE\n"
           "    machinery-cofe [-d DEVICE] [-b BAUDRATE] sdo-upload NODE MAP\n"
           "    machinery-cofe [-d DEVICE] [-b BAUDRATE] sdo-download NODE MAP VALUE\n"
           "    machinery-cofe [-d DEVICE] [-b BAUDRATE] sync\n"
           "\n"
           "DESCRIPTION\n"
           "    Perform a single CANopen command  on the interface DEVICE.  NODE is the\n"
           "    id of the node the command is referring (use 0 for broadcast requests).\n"
           "    SERVICE can be start, stop, preoperational, reset or reset-communication.\n"
           "    MAP identifies an object dictionary entry. Must be a hexadecimal number\n"
           "    in the format AAAABBCC where AAAA is the index, BB is the sub-index and\n"
           "    CC is the number of bits. For example, `12345610` will be broken into:\n"
           "\n"
           "        - 1234 (index)\n"
           "        - 56   (sub-index)\n"
           "        - 10   (number of bits)\n"
           "\n"
           "    Hence it will refer to the first 16 bits of entry $1234:56.\n"
           "\n"
           "EXAMPLES\n"
           "    machinery-cofe -d can0 -b 500000 nmt 0 start\n"
           "        Start all nodes connected to can0 (500 Kbauds) in broadcast.\n"
           "\n"
           "    machinery-cofe sdo-upload 1 60000110\n"
           "        Read 16 bits from the entry $6000:01 of the OD of slave 1.\n"
           "\n"
           "    machinery-cofe sdo-upload 1 60000110 1234\n"
           "        Write 1234 into the same above entry.\n"
           "\n",
           message);
    exit(1);
}

int
main(int argc, char *argv[])
{
    App app;
    CO_Data *data;
    int n;
    char *arg;
    long num;
    struct timespec tenth = { 0, 100000000L };
    char *device    = DEVICE;
    int baudrate    = BAUDRATE;
    Command command = COMMAND_NONE;
    int node        = -1;
    uint32_t map    = 0x00000000;
    char *value     = NULL;
    ActionStatus result;

    for (n = 1; n < argc; ++n) {
        arg = argv[n];
        if (strcmp(arg, "-d") == 0) {
            ++n;
            if (n == argc) {
                bailout("missing device");
            }
            device = argv[n];
        } else if (strcmp(arg, "-b") == 0) {
            ++n;
            if (n == argc) {
                bailout("missing baudrate");
            }
            sscanf(argv[n], "%d", &baudrate);
        } else if (command == COMMAND_NONE) {
            if (strcmp(arg, "nmt") == 0) {
                command = COMMAND_NMT;
            } else if (strcmp(arg, "sdo-upload") == 0) {
                command = COMMAND_SDO_UPLOAD;
            } else if (strcmp(arg, "sdo-download") == 0) {
                command = COMMAND_SDO_DOWNLOAD;
            } else if (strcmp(arg, "sync") == 0) {
                command = COMMAND_SYNC;
            } else {
                bailout("invalid command");
            }
        } else if (node == -1) {
            if (command == COMMAND_SYNC) {
                bailout("SYNC message does not need more arguments");
            }
            sscanf(arg, "%d", &node);
            if (node < 0 || (command != COMMAND_NMT && node == 0)) {
                bailout("invalid NODE");
            }
        } else if (command != COMMAND_NMT && map == 0) {
            sscanf(arg, "%x", &map);
            if (map <= 0) {
                bailout("invalid MAP");
            }
        } else if (command != COMMAND_SDO_UPLOAD && value == NULL) {
            value = arg;
        } else {
            bailout("too many arguments");
        }
    }

    if (command == COMMAND_NONE) {
        bailout("missing command");
    } else if (command == COMMAND_NMT && value == NULL) {
        bailout("missing SERVICE");
    } else if (command == COMMAND_SDO_DOWNLOAD && value == NULL) {
        bailout("missing VALUE");
    } else if (command == COMMAND_SDO_UPLOAD && map == 0) {
        bailout("missing MAP");
    }

    data = &Dummy_Data;

    if (node > 0) {
        /* Dynamically initialize the first SDO client in the OD to make
         * canfestival happy */
        UNS32 value, size;

        size  = 4;
        value = 0x600 + node;
        setODentry(data, 0x1280, 1, &value, &size, 0);

        size  = 4;
        value = 0x580 + node;
        setODentry(data, 0x1280, 2, &value, &size, 0);

        size  = 1;
        value =  node;
        setODentry(data, 0x1280, 3, &value, &size, 0);
    }

    app_init(&app, "machinery-cofe");

    app_canopen_init(&app, device, baudrate);

    app_canopen_set_id(&app, MASTERID);
    app_canopen_set_data(&app, data);
    app_canopen_start(&app);

    switch (command) {

    case COMMAND_NMT:
        if (strcmp(value, "start") == 0) {
            num = NMT_Start_Node;
        } else if (strcmp(value, "stop") == 0) {
            num = NMT_Stop_Node;
        } else if (strcmp(value, "preoperational") == 0) {
            num = NMT_Enter_PreOperational;
        } else if (strcmp(value, "reset") == 0) {
            num = NMT_Reset_Node;
        } else if (strcmp(value, "reset-communication") == 0) {
            num = NMT_Reset_Comunication;
        } else {
            bailout("invalid NMT service");
        }
        result = masterSendNMTstateChange(data, node, (UNS8) num) == 0 ? ACTION_DONE : ACTION_ERROR;
        break;

    case COMMAND_SDO_UPLOAD:
        num = 0;
        do {
            /* Give 5 seconds max to respond (50 x 0,1 s) */
            nanosleep(&tenth, NULL);
            result = app_canopen_read_sdo(&app, node, map, &num, 50);
        } while (result == ACTION_WAITING);
        printf("%0*X\n", (map & 0xFF) / 4, (unsigned) num);
        break;

    case COMMAND_SDO_DOWNLOAD:
        if (value[0] == '0' && value[1] == 'x') {
            /* Hexadecimal constant */
            sscanf(value, "%lX", &num);
        } else {
            /* Likely decimal constant */
            sscanf(value, "%ld", &num);
        }
        do {
            /* Give 5 seconds max to respond (50 x 0,1 s) */
            nanosleep(&tenth, NULL);
            result = app_canopen_write_sdo(&app, node, map, num, 50);
        } while (result == ACTION_WAITING);
        break;

    case COMMAND_SYNC:
        result = sendSYNCMessage(data) == 0 ? ACTION_DONE : ACTION_ERROR;
        break;

    case COMMAND_NONE:
    default:
        result = ACTION_DONE;
        break;
    }

    app_canopen_stop(&app);
    app_canopen_free(&app);
    app_free(&app);

    return result == ACTION_DONE ? APP_SUCCESS : APP_FAILURE;
}
