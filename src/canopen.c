/**
 * @file  canopen.c
 * @brief CANopen master stack
 *
 * The master stack is based on CANfestival 3.0: see its
 * [home page](http://www.canfestival.org) for more details.
 *
 * WARNING: CANfestival implementation is quite dumb and forces the use of a
 * lot of global states. This means everything based on it should be
 * considered *not* thread-safe.
 *
 * In the specific the following functions work properly because they keep a
 * global pointer to the "main" App instance (the so called GAIP, i.e. global
 * app instance pointer). This pointer is set by the app_canopen_init() call.
 */

#include "machinery.h"

#include <canfestival.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_SLAVES  20
#define CANOPEN     ((CANopen *) (gaip)->canopen)
#define IDX(map)    ((map) >> 16)
#define SUB(map)    (((map) & 0xFF00) >> 8)
#define SIZE(map)   ((map) & 0xFF)
#define BYTES(map)  (((map) & 0xFF) / 8)
#define MAP(i,s,z)  ((z) + ((s) << 8) + ((i) << 16))
#define SDO_CONFIGURE(idx,sub,bytes,value) \
                    { \
                        uint32_t tmp = (value); \
                        result = writeNetworkDictCallBack(data, slave_id, \
                                                          (idx), (sub), (bytes), \
                                                          0, &tmp, configuration, 0); \
                        if (result != 0) { \
                            slave->scans  = 0; \
                            slave->errors = 0; \
                            if (slave->is_blind) { \
                                info("canopen: skip PDO setting (id %u, entry %04X:%02X, value %0*X, result %X, step %d)", \
                                     slave_id, idx, sub, bytes*2, tmp, result, slave->step); \
                                slave->step += 2; \
                                goto redo; \
                            } \
                            warning("PDO setting error (id %u, entry %04X:%02X, value %0*X, result %X, step %d)", \
                                    slave_id, idx, sub, bytes*2, tmp, result, slave->step); \
                            slave->step = 0; \
                            return ACTION_ERROR; \
                        } \
                    }


/* Not sure why this is not declared in canfestival.h */
extern UNS8 GetSDOClientFromNodeId(CO_Data *data, UNS8 id);


typedef struct {
    UNS8      id;
    Configure configure;

    /* Settings */
    int32_t   inhibit;
    int32_t   event;
    int       max_scans;
    int       max_errors;
    int       is_blind;

    /* Internal states */
    int       is_booted;
    int       is_configured;
    int       step;
    int       configure_step;
    unsigned  scans;
    unsigned  errors;
} Slave;

typedef enum {
    TPDO = 1,
    RPDO = 2,
} PdoType;

/* Internal struct, pointed by app->canopen */
typedef struct {
    s_BOARD         board;
    UNS8            master_id;
    CO_Data *       data;
    Slave           slaves[MAX_SLAVES];
    int             opened;
    TIMER_HANDLE    bootup_handle;
    ODCallback      transmitter;
    ODCallback      receiver;
} CANopen;

/* Global state needed because CANfestival sucks */
static App *gaip = NULL;


static char *
to_string(int n)
{
    size_t size = snprintf(NULL, 0, "%d", n) + 1;
    char *string = malloc(size);
    snprintf(string, size, "%d", n);
    return string;
}

static CANopen *
create_canopen(const char *device, int baudrate)
{
    CANopen *canopen = malloc(sizeof(CANopen));

    canopen->board.busname  = strdup(device);
    canopen->board.baudrate = to_string(baudrate);
    canopen->master_id      = 0;
    canopen->data           = NULL;
    canopen->opened         = 0;
    canopen->bootup_handle  = 0;
    canopen->transmitter    = NULL;
    canopen->receiver       = NULL;
    memset(&canopen->slaves, 0, sizeof(Slave) * MAX_SLAVES);

    return canopen;
}

static int
process_configure(Slave *slave)
{
    /* Check for no configuration required */
    if (slave->configure == NULL) {
        return 1;
    }

process:
    switch (slave->configure(gaip, slave->id, slave->configure_step)) {

    case ACTION_WAITING:
        return 0;

    case ACTION_DONE:
        ++ slave->configure_step;
        goto process;

    case ACTION_COMPLETE:
    case ACTION_ERROR:
        slave->configure_step = 0;
        return 1;

    default:
        die("canopen: unexpected condition met during configuration");
        slave->configure_step = 0;
        return 1;
    }
}

static void
configuration(CO_Data *data, UNS8 slave_id)
{
    Slave *slave;
    int i;

    /* Configure all unconfigured slaves */
    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (slave->is_booted && ! slave->is_configured) {
            slave->is_configured = process_configure(slave);
        }
    }

    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (slave->is_booted && ! slave->is_configured) {
            return;
        }
    }

    /* When all booted nodes are configured, start in broadcast... */
    masterSendNMTstateChange(data, 0, NMT_Start_Node);
    /* ... and switch to Operational */
    setState(data, Operational);
}

static Slave *
slave_from_id(UNS8 id)
{
    Slave *slave;
    int i;

    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (slave->id == id) {
            return slave;
        }
    }

    return NULL;
}

static void
bootup_callback(CO_Data *data, UNS8 id)
{
    Slave *slave = slave_from_id(id);
    int i;

    /* If an unknown device booted up, just ignore it */
    if (slave == NULL) {
        return;
    }

    slave->is_booted = 1;

    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (! slave->is_booted) {
            return;
        }
    }

    /* When all nodes booted up, remove the timeout callback... */
    DelAlarm(CANOPEN->bootup_handle);
    /* ...and start the configuration */
    configuration(data, id);
}

static void
bootup_failed(CO_Data *data)
{
    Slave *slave;
    int i;

    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (! slave->is_booted) {
            warning("canopen: node #%d did not boot", slave->id);
        }
    }

    /* Start the configuration anyway, just to allow to have a half baked
     * machine, useful e.g. during construction */
    configuration(data, 0);
}

static void
start_callback(void)
{
    setNodeId(CANOPEN->data, CANOPEN->master_id);
    setState(CANOPEN->data, Initialisation);
}

static void
stop_callback(void)
{
    /* CANfestival sends a broadcast message when the state machine is
     * stopped, so do it only when there are slaves */
    if (CANOPEN->slaves->id > 0) {
        setState(CANOPEN->data, Stopped);
    }
}

static void
initialisation_callback(CO_Data *data)
{
    if (CANOPEN->slaves->id > 0) {
        info("canopen: NMT state Initialisation");
    }
}

static void
preoperational_callback(CO_Data *data)
{
    /* Go operational only if there is at least one slave configured */
    if (CANOPEN->slaves->id > 0) {
        info("canopen: NMT state Pre-operational");

        /* Put all CANopen devices in pre-operational mode */
        masterSendNMTstateChange(data, 0, NMT_Reset_Node);

        /* Give some time to the nodes to perform the bootup */
        CANOPEN->bootup_handle = SetAlarm(data, 0,
                                          (TimerCallback_t) bootup_failed,
                                          MS_TO_TIMEVAL(10000), 0);
    }
}

static void
operational_callback(CO_Data *data)
{
    info("canopen: NMT state Operational");
}

static void
stopped_callback(CO_Data *data)
{
    /* Broadcasting a NMT reset if there is at least one slave*/
    if (CANOPEN->slaves->id > 0) {
        info("canopen: NMT state Stopped");
        masterSendNMTstateChange(data, 0, NMT_Reset_Node);
    }
}

static UNS32
rpdo_callback(CO_Data *data, const indextable *table, UNS8 subidx)
{
    if (CANOPEN->transmitter != NULL) {
        subindex *p  = table->pSubindex + subidx;
        uint32_t map = MAP(table->index, subidx, p->size * 8);
        (*CANOPEN->transmitter)(gaip, map, p->pObject);
    }

    /* Trigger the sending of RPDOs */
    sendPDOevent(data);

    return OD_SUCCESSFUL;
}

static UNS32
tpdo_callback(CO_Data *data, const indextable *table, UNS8 subidx)
{
    if (CANOPEN->receiver != NULL) {
        subindex *p  = table->pSubindex + subidx;
        uint32_t map = MAP(table->index, subidx, p->size * 8);
        (*CANOPEN->receiver)(gaip, map, p->pObject);
    }
    return OD_SUCCESSFUL;
}

static int
read_od(CO_Data *data, uint32_t map, void *dst)
{
    UNS8  type;
    UNS32 size = SIZE(map);
    return getODentry(data, IDX(map), SUB(map), dst, &size, &type, 0) == OD_SUCCESSFUL;
}

static int
bind_callback(CO_Data *data, uint16_t can_id, PdoType pdo_type)
{
    uint16_t mapping, settings;
    ODCallback_t callback;
    uint32_t map;
    uint8_t i, n;

    /* Locally the type is reversed (a remote TxPDO is a local RxPDO) so the
     * following lines should be the reverse of what found in map_pdo() */
    mapping  = (pdo_type == RPDO ? 0x1A00 : 0x1600);
    settings = (pdo_type == RPDO ? 0x1800 : 0x1400);
    callback = (pdo_type == RPDO ? rpdo_callback : tpdo_callback);

    /* n should be read by read_od() but, while testing, that function
     * is mocked */
    n = 1;

    while (read_od(data, MAP(settings, 1, 4), &map)) {
        if (map == can_id) {
            /* When the CAN id stored in the settings matches the provided
             * one, bind the callback to all the mapped entries */
            if (read_od(data, MAP(mapping, 0, 1), &n)) {
                for (i = 1; i <= n; ++i) {
                    if (read_od(data, MAP(mapping, i, 4), &map)) {
                        RegisterSetODentryCallBack(data, IDX(map), SUB(map), callback);
                    }
                }
            }
            return 1;
        }
        ++mapping;
        ++settings;
    }

    return 0;
}

static void
reset_slave(Slave *slave)
{
    slave->step           = 0;
    slave->configure_step = 0;
    slave->scans          = 0;
    slave->errors         = 0;
}

/**
 * PDO mapping.
 *
 * The algorithm strictly follows what described by sections 7.5.2.36 and
 * 7.5.2.38 of CiA 301 4.2.0.
 */
static ActionStatus
map_pdo(App *app, uint8_t slave_id, uint8_t pdo, PdoType type,
        uint8_t n_maps, uint32_t maps[])
{
    CANopen *canopen;
    CO_Data *data;
    UNS16 mapping;
    UNS16 settings;
    Slave *slave;
    uint8_t n_map;
    uint32_t map;
    uint16_t can_id;
    UNS8 result;
    UNS32 code;

    if (n_maps > 4) {
        die("A maximum of 4 entries can be mapped to a single PDO");
    }

    canopen  = app->canopen;
    data     = canopen->data;
    slave    = canopen->slaves + GetSDOClientFromNodeId(data, slave_id);
    mapping  = (type == RPDO ? 0x1600 : 0x1A00) + pdo;
    settings = (type == RPDO ? 0x1400 : 0x1800) + pdo;
    /* `pdf & 3` is a hack to be able to use the Schneider ATV312: that
     * crap uses PDO6 mapping and setting registers with PDO2 CAN ID */
    can_id   = (type == RPDO ? 0x200 : 0x180) + 0x100 * (pdo & 3) + slave_id;

redo:
    n_map    = slave->step / 100 + 1;
    map      = maps[n_map - 1];

    /* Define only even numbers because all response are handled by the same
     * code in the fallback case (default:) when the step is odd */
    switch (slave->step) {

    /* 1. Destroy PDO by setting bit valid to 1 b of sub-index 01 h of the
     *    according PDO communication parameter. */
    case 0:
        SDO_CONFIGURE(settings, 1, 4, (uint32_t) 0x80000000UL + can_id);
        break;

    /* 2. Disable mapping by setting sub-index 00 h to 00 h. */
    case 2:
        SDO_CONFIGURE(mapping, 0, 1, 0);
        break;

    /* 2a. Always use asynchronous transmission mode */
    case 4:
        SDO_CONFIGURE(settings, 2, 1, 0xFF);
        break;

    /* 2b. Set the inhibit time for TxPDO, if requested */
    case 6:
        if (type == TPDO && slave->inhibit >= 0) {
            SDO_CONFIGURE(settings, 3, 2, (uint16_t) slave->inhibit);
            break;
        }

        /* Nothing to do: fallthrough the next case */
        slave->step += 2;

    /* 2c. Set the event timer for TxPDO, if requested */
    case 8:
        if (type == TPDO && slave->event >= 0) {
            SDO_CONFIGURE(settings, 5, 2, (uint16_t) slave->event);
            break;
        }

        /* Nothing to do: fallthrough the next case */
        slave->step += 2;

    /* 3. Modify mapping by changing the values of the corresponding
     *    sub-indices. */
    case 10:
    case 100:
    case 200:
    case 300:
        SDO_CONFIGURE(mapping, n_map, 4, map);
        break;

    /* 4. Enable mapping by setting sub-index 00 h to the number mapped
     *    objects. */
    case 12:
    case 102:
    case 202:
    case 302:
        if (n_map >= n_maps) {
            SDO_CONFIGURE(mapping, 0, 1, n_maps);
            break;
        }

        /* Nothing to do: fallthrough the next case */
        slave->step += 2;

    /* 5. Create PDO by setting bit valid to 0 b of sub-index 01 h of the
     *    according PDO communication parameter. */
    case 14:
    case 104:
    case 204:
    case 304:
        if (n_map >= n_maps) {
            SDO_CONFIGURE(settings, 1, 4, can_id);
            break;
        }

        /* Nothing to do: fallthrough the next case */
        slave->step += 2;

    /* Mapping procedure terminated. */
    case 16:
    case 106:
    case 206:
    case 306:
        if (n_map >= n_maps) {
            /* Last map: bind the machinery callback to the entries mapped by
             * this PDO and exit */
            if (! bind_callback(data, can_id, type)) {
                warning("canopen: %cxPDO%d of remote node %d is not locally mapped",
                        type == RPDO ? 'R' : 'T', pdo + 1, slave_id);
            }
            slave->step   = 0;
            slave->scans  = 0;
            slave->errors = 0;
            return ACTION_DONE;
        }

        /* Not the last map: process the next one */
        slave->step = n_map * 100;
        goto redo;

    default:
        result = getWriteResultNetworkDict(data, slave_id, &code);

        switch (result) {

        case SDO_DOWNLOAD_IN_PROGRESS:
        case SDO_UPLOAD_IN_PROGRESS:
            if (slave->max_scans > 0 && ++slave->scans > slave->max_scans) {
                resetClientSDOLineFromNodeId(data, slave_id);
                slave->scans  = 0;
                slave->errors = 0;
                warning("PDO setting timeout (id %u, entry %04X:%02X, result %X, abort code %X)",
                        slave_id, IDX(map), SUB(map), result, code);
                slave->step = 0;
                return ACTION_ERROR;
            }
            return ACTION_WAITING;

        case SDO_FINISHED:
            closeSDOtransfer(data, slave_id, SDO_CLIENT);
            ++slave->step;
            slave->scans  = 0;
            slave->errors = 0;
            goto redo;

        default:
            resetClientSDOLineFromNodeId(data, slave_id);
            if (slave->is_blind) {
                ++slave->step;
                slave->scans  = 0;
                slave->errors = 0;
                goto redo;
            }

            ++slave->errors;
            if (slave->max_errors > 0 && slave->errors >= slave->max_errors) {
                slave->scans  = 0;
                slave->errors = 0;
                warning("PDO setting failed (id %u, entry %04X:%02X, result %X, abort code %X, step %d)",
                        slave_id, IDX(map), SUB(map), result, code, slave->step);
                slave->step = 0;
                return ACTION_ERROR;
            } else {
                /* Try again Tony */
                --slave->step;
                goto redo;
            }
        }
    }

    ++slave->step;
    return ACTION_WAITING;
}

/**
 * Initialize an application instance for CANopen interaction.
 *
 * This should be called before any other CANopen related call. Any blocking
 * error will generate a relevant message and will quit the current process.
 *
 * Secondarily, this function sets the GAIP too (global App instance pointer):
 * see the documentation of canopen.c for further details.
 *
 * @param app       An App instance to initialize
 * @param device    The CANbus device name, e.g. "can0"
 * @param baudrate  The CANbus speed in bits per seconds, e.g. 500000
 */
void
app_canopen_init(App *app, const char *device, int baudrate)
{
    if (! LoadCanDriver("/usr/local/lib/libcanfestival_can_socket.so")) {
        die_with_errno("LoadCanDriver");
        return;
    }

    app->canopen = create_canopen(device, baudrate);

    /* Set the global state to the current app instance */
    gaip = app;
}

/**
 * Finalize the CANopen communication of an application.
 *
 * This is the counterpart of app_canopen_init().
 *
 * @param app The instance to free
 */
void
app_canopen_free(App *app)
{
    CANopen *canopen = app->canopen;

    if (canopen != NULL) {
        if (canopen->board.busname != NULL) {
            free(canopen->board.busname);
        }
        if (canopen->board.baudrate != NULL) {
            free(canopen->board.baudrate);
        }
        free(canopen);
        app->canopen = NULL;
    }
}

/**
 * Set the master ID.
 *
 * The CANbus id of the master will be effectively set during the start
 * procedure, i.e. in the app_canopen_start() function.
 *
 * @param app An App instance to initialize
 * @param id  The CANbus id to associate to this device, e.g. 1
 */
void
app_canopen_set_id(App *app, uint8_t id)
{
    CANopen *canopen = app->canopen;
    canopen->master_id = id;
}

/**
 * Set the data needed by the underlying CANopen stack.
 *
 * Actually, the machinery CANopen stack is based on CANfestival. You need to
 * pass a CO_Data pointer as data parameter. This call effectively bound the
 * app instance to that internal CANfestival data.
 *
 * @param app  An App instance to initialize
 * @param data The CANfestival data
 */
void
app_canopen_set_data(App *app, void *data)
{
    CANopen *canopen = app->canopen;
    canopen->data = data;

    if (data != NULL) {
        canopen->data->initialisation   = initialisation_callback;
        canopen->data->preOperational   = preoperational_callback;
        canopen->data->operational      = operational_callback;
        canopen->data->stopped          = stopped_callback;
        canopen->data->post_SlaveBootup = bootup_callback;
    }
}

/**
 * Set the transmitter callback.
 *
 * The transmitter callback is called whenever an object dictionary entry
 * bound to a RPDO has changed. This is usually triggered by the application
 * when some output signal must be set.
 *
 * @param app         The application instance
 * @param transmitter The transmitter callback
 */
void
app_canopen_set_transmitter(App *app, ODCallback transmitter)
{
    CANopen *canopen     = app->canopen;
    canopen->transmitter = transmitter;
}

/**
 * Set the receiver callback.
 *
 * The receiver callback is called whenever an object dictionary entry bound
 * to a TPDO object has changed. This is usually triggered by the reception of
 * a TPDO frame sent usually because some input changed its state.
 *
 * @param app      The application instance
 * @param receiver The receiver callback
 */
void
app_canopen_set_receiver(App *app, ODCallback receiver)
{
    CANopen *canopen  = app->canopen;
    canopen->receiver = receiver;
}

/**
 * Add a new CANopen slave.
 *
 * An application instance can have up to MAX_SLAVES CANopen slave nodes. This
 * method add a new slave. It is not planned to remove and/or change slaves:
 * once a slave is added there is nothing you can do to remove or change it.
 *
 * During the configuration phase (i.e. after every node has booted up), the
 * configure callback is called repeatedly until it returns `ACTION_COMPLETE`
 * (if terminated successfully) or `ACTION_ERROR` (if an error occurred).
 * It must **never** block, e.g. it should return `ACTION_WAITING` instead
 * when waiting for I/O. This is purposedly compatible with many functions
 * found in this module, so you can freely call them in your code. See
 * `slaves.c` for implementation examples.
 *
 * The `step` index is set to 0 the first call and incremented every time
 * `configure` returns `ACTION_DONE`. It can be used internally for perform
 * subsequent configuration steps.
 *
 * @param app       The application instance
 * @param slave_id  The CANbus id of the slave to add
 * @param configure The callback to be used for configuring the slave
 */
void
app_canopen_add_slave(App *app, uint8_t slave_id, Configure configure)
{
    Slave *slave;
    int i;

    for (i = 0; slave = CANOPEN->slaves + i, slave->id > 0; ++i) {
        if (i >= MAX_SLAVES - 1) {
            die("canopen: maximum number of slaves reached (%d)", MAX_SLAVES);
            return;
        }
    }

    slave->id             = slave_id;
    slave->configure      = configure;
    slave->inhibit        = -1;
    slave->event          = -1;
    slave->max_scans      = 0;
    slave->max_errors     = 3;
    slave->is_blind       = 0;
    slave->is_booted      = 0;
    slave->is_configured  = 0;
    slave->step           = 0;
    slave->configure_step = 0;
    slave->scans          = 0;
    slave->errors         = 0;
}

/**
 * Start the CANopen thread.
 *
 * If there are slave devices (added by app_canopen_add_slave() calls), all
 * the nodes (master node included) are put in operational node with a
 * broadcast message. The data acquisition is done by a working thread. This
 * method spawns that thread and triggers the CANopen configuration and bootup
 * procedures.
 *
 * If there are no slaves, *only* the master node is put in pre-operational
 * state. This allows the @p app instance to perform simply CANopen
 * operations, e.g. perform SDO uploads or downloads.
 *
 * WARNING: this function must be called only after having set the master id
 * and the CANopen implementation data with app_canopen_set_id() and
 * app_canopen_set_data().
 *
 * @param app The application instance
 */
void
app_canopen_start(App *app)
{
    CANopen *canopen = app->canopen;

    if (! canopen->opened) {
        if (! canOpen(&canopen->board, canopen->data)) {
            die_with_errno("canOpen");
            return;
        }

        TimerInit();
        StartTimerLoop((TimerCallback_t) start_callback);

        canopen->opened = 1;
    }
}

/**
 * Stop the CANopen thread.
 *
 * Counterpart of app_canopen_start(): it stops the working thread, eventually
 * triggering the callbacks bound to the stop event.
 *
 * @param app The application instance
 */
void
app_canopen_stop(App *app)
{
    CANopen *canopen = app->canopen;

    if (canopen->opened) {
        StopTimerLoop((TimerCallback_t) stop_callback);
        canClose(canopen->data);

        canopen->opened = 0;
    }
}

/**
 * Reset internal SDO states.
 *
 * Multistep actions (i.e. all APIs that return an `ActionStatus`) expect to
 * be called multiple times until they return `ACTION_DONE` or `ACTION_ERROR`.
 * If for some reason you want to stop this loop, you must call this function
 * to reset the internal states.
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node to reset
 */
void
app_canopen_reset(App *app, uint8_t slave_id)
{
    CANopen *canopen = app->canopen;
    CO_Data *data    = canopen->data;
    UNS8     idx     = GetSDOClientFromNodeId(data, slave_id);
    Slave   *slave   = canopen->slaves + idx;

    reset_slave(slave);
}

/**
 * Change settings on a specific node.
 *
 * Settings are stored on a per node basis.
 *
 * `inhibit` is the value of the inhibit time, as specified by CiA DSP 301. A
 * value of -1 do not set the inhibit time while a value of -2 leaves the old
 * setting untouched.
 * 
 * `event` is the event timer. Similarly to the inhibit time, a value of -1 do
 * not set the event timer while a value of -2 leaves this setting untouched.
 *
 * `max_scans` specifies the number of times a function can return in a
 * waiting state (e.g. while waiting a response from a remote node) before
 * giving up. Use 0 to disable this feature (wait forever), or a value less
 * than 0 to leave the old setting untouched. When set to 0 it is a good
 * strategy to perform a similar check at higher level, eventually using
 * app_canopen_reset(), to avoid waiting indefinitely for a response.
 *
 * `max_errors` specifies the number of retries allowed afer an error before
 * giving up. Use a value less than 0 to not change this setting.
 *
 * `is_blind` is a flag that, when set, changes the program behavior to be
 * more resilient to non-conforming devices. Most notably, errors triggered
 * during the PDO mapping procedure will be ignored.
 *
 * @param app        The application instance
 * @param slave_id   The CANbus id of the slave node to map
 * @param inhibit    Minimum time between two TxPDO (1 = 100 µs, -1 = skip)
 * @param event      Maximum time between two TxPDO (1 = 1 ms, -1 = skip)
 * @param max_scans  Max number of times to call actions before giving up.
 * @param max_errors Number of retries after an error
 * @param blind
 */
void
app_canopen_settings(App *app, uint8_t slave_id, int32_t inhibit, int32_t event,
                     int max_scans, int max_errors, int is_blind)
{
    CANopen *canopen;
    CO_Data *data;
    Slave   *slave;

    canopen = app->canopen;
    data    = canopen->data;
    slave   = canopen->slaves + GetSDOClientFromNodeId(data, slave_id);

    if (inhibit >= -1) {
        slave->inhibit = inhibit;
    }
    if (event >= -1) {
        slave->event = event;
    }
    if (max_scans >= 0) {
        slave->max_scans = max_scans;
    }
    if (max_errors >= 0) {
        slave->max_errors = max_errors;
    }
    if (is_blind >= 0) {
        slave->is_blind = is_blind;
    }
}

/**
 * Map a TxPDO to multiple entries of a remote dictionary.
 *
 * Maps the entries specified with @p n_maps and @p maps of the object
 * dictionary bound to @p slave_id node to the @p pdo TxPDO object.
 *
 * For example, if you want the PDO1 to be 24 bits, the first 8 ones being
 * bound to $6000:01 and the last 16 ones being bound to $6100:01 of the node
 * with id 123, you should write something along these lines;
 *
 * @code
 *     uint16_t maps[] = {
 *         0x60000108,
 *         0x61000110,
 *     }
 *     while (app_canopen_map_tpdoa(app, 123, PDO1, 2, maps) == 0)
 *         ;
 * @endcode
 *
 * @param app       The application instance
 * @param slave_id  The CANbus id of the slave node to map
 * @param pdo       Number of the PDO to map, e.g. PDO1
 * @param n_maps    Number of dictionary entries in maps
 * @param maps      The object dictionary entries to map on the slave node, e.g.
 *                  0x61000110 maps 16 bits of data from the entry $6100:01.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_tpdoa(App *app, uint8_t slave_id, uint8_t pdo,
                      uint8_t n_maps, uint32_t maps[])
{
    return map_pdo(app, slave_id, pdo, TPDO, n_maps, maps);
}

/**
 * Map a TxPDO to multiple entries of a remote dictionary.
 *
 * Similar to app_canopen_map_tpdoa() but provided for C convenience. It
 * accepts a list of uint32_t maps instead of requiring to pass an array.
 *
 * For example, if you want the PDO1 to be 24 bits, the first 8 ones being
 * bound to $6000:01 and the last 16 ones being bound to $6100:01 of the node
 * with id 123, you should write something along these lines;
 *
 * @code
 *     while (app_canopen_map_tpdos(app, 123, PDO1, 2, 0x60000108, 0x61000110) == 0)
 *         ;
 * @endcode
 *
 * @param app       The application instance
 * @param slave_id  The CANbus id of the slave node to map
 * @param pdo       Number of the PDO to map, e.g. PDO1
 * @param n         Number of maps passed in varargs
 * @param ...       A serie of n uint32_t maps
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_tpdos(App *app, uint8_t slave_id, uint8_t pdo, int n, ...)
{
    uint32_t maps[n];
    va_list ap;
    uint8_t n_maps;

    va_start(ap, n);
    for (n_maps = 0; n_maps < n; ++n_maps) {
        maps[n_maps] = va_arg(ap, uint32_t);
    }
    va_end(ap);

    return app_canopen_map_tpdoa(app, slave_id, pdo, n_maps, maps);
}

/**
 * Map a TxPDO to a single entry of a remote dictionary.
 *
 * This is just a convenience function built around app_canopen_map_tpdoa().
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node to map
 * @param pdo      Number of the PDO to map, e.g. PDO1
 * @param map      The object dictionary entry to map on the slave node, e.g.
 *                 0x61000110 maps 16 bits of data from the entry $6100:01.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_tpdo(App *app, uint8_t slave_id, uint8_t pdo, uint32_t map)
{
    uint32_t maps[1];
    maps[0] = map;
    return app_canopen_map_tpdoa(app, slave_id, pdo, 1, maps);
}

/**
 * Map a RxPDO to multiple entries of a remote dictionary.
 *
 * Maps the entries specified with @p n_maps and @p maps of the object
 * dictionary bound to @p slave_id node to the @p pdo RxPDO object. This is
 * similar to app_canopen_map_rpdoa() but works for RxPDO objects.
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node to map
 * @param pdo      Number of the PDO to map, e.g. PDO1
 * @param n_maps   Number of dictionary entries in maps
 * @param maps     The object dictionary entries to map on the slave node, e.g.
 *                 0x63000110 maps 16 bits of data from the entry $6300:01.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_rpdoa(App *app, uint8_t slave_id, uint8_t pdo,
                      uint8_t n_maps, uint32_t maps[])
{
    return map_pdo(app, slave_id, pdo, RPDO, n_maps, maps);
}

/**
 * Map a RxPDO to multiple entries of a remote dictionary.
 *
 * Similar to app_canopen_map_rpdoa() but provided for C convenience. It
 * accepts a list of uint32_t maps instead of requiring to pass an array.
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node to map
 * @param pdo      Number of the PDO to map, e.g. PDO1
 * @param n        Number of maps passed in varargs
 * @param ...      A serie of n uint32_t maps
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_rpdos(App *app, uint8_t slave_id, uint8_t pdo, int n, ...)
{
    uint32_t maps[n];
    va_list ap;
    uint8_t n_maps;

    va_start(ap, n);
    for (n_maps = 0; n_maps < n; ++n_maps) {
        maps[n_maps] = va_arg(ap, uint32_t);
    }
    va_end(ap);

    return app_canopen_map_rpdoa(app, slave_id, pdo, n_maps, maps);
}

/**
 * Map a remote RxPDO to a single entry of the remote dictionary.
 *
 * This is just a convenience function built around app_canopen_map_rpdoa().
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node to map
 * @param pdo      Number of the PDO to map, e.g. PDO1
 * @param map      The object dictionary entry to map on the slave node, e.g.
 *                 0x63000110 maps 16 bits of data from the entry $6300:01.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_map_rpdo(App *app, uint8_t slave_id, uint8_t pdo, uint32_t map)
{
    uint32_t maps[1];
    maps[0] = map;
    return app_canopen_map_rpdoa(app, slave_id, pdo, 1, maps);
}

/**
 * Perform a slave SDO read.
 *
 * This function must be called in a loop until it returns 1. For example,
 * this is a blocking SDO read using a typical busy loop:
 *
 * @code
 *     uint8_t byte;
 *
 *     // Don't do this: leverage your event loop instead!
 *     while (app_canopen_read_sdo(app, 50, 0x60000108, &byte) == ACTION_WAITING)
 *         ;
 * @endcode
 *
 * The slave must be previously added with app_canopen_add_slave(). Also, keep
 * in mind that the various app_canopen_map_*() functions and this one share
 * the same context, so they cannot be mixed together.
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node
 * @param map      The object dictionary entry to read on the slave node,
 *                 e.g. 0x63000110 reads 2 bytes at address $6300:01.
 * @param value    Where to store the result.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_read_sdo(App *app, uint8_t slave_id, uint32_t map, void *value, int timeout)
{
    CANopen *canopen;
    CO_Data *data;
    Slave *slave;
    UNS8 result;
    UNS32 code, size;

    canopen = app->canopen;
    data    = canopen->data;
    slave   = canopen->slaves + GetSDOClientFromNodeId(data, slave_id);

redo:
    switch (slave->step) {

    case 0:
        result = readNetworkDict(data, slave_id, IDX(map), SUB(map), 0, 0);
        if (result != 0) {
            warning("SDO read error (id %u, entry %04X:%02X, result %X)",
                    slave_id, IDX(map), SUB(map), result);
            return ACTION_ERROR;
        } else {
            ++slave->step;
            return ACTION_WAITING;
        }

    case 1:
        size   = BYTES(map);
        result = getReadResultNetworkDict(data, slave_id, value, &size, &code);

        switch (result) {

        case SDO_DOWNLOAD_IN_PROGRESS:
        case SDO_UPLOAD_IN_PROGRESS:
            if (slave->max_scans > 0 && ++slave->scans > slave->max_scans) {
                resetClientSDOLineFromNodeId(data, slave_id);
                reset_slave(slave);
                warning("SDO read timeout (id %u, entry %04X:%02X)",
                        slave_id, IDX(map), SUB(map));
                return ACTION_ERROR;
            }
            return ACTION_WAITING;

        case SDO_FINISHED:
            closeSDOtransfer(data, slave_id, SDO_CLIENT);
            reset_slave(slave);
            return ACTION_DONE;

        default:
            ++slave->errors;
            resetClientSDOLineFromNodeId(data, slave_id);

            if (slave->errors >= slave->max_errors) {
                reset_slave(slave);
                warning("SDO read failed (id %u, entry %04X:%02X, result %X, abort code %X)",
                        slave_id, IDX(map), SUB(map), result, code);
                return ACTION_ERROR;
            } else {
                --slave->step;
                goto redo;
            }
        }

    default:
        die("canopen: unexpected condition met during SDO read");
        slave->step = 0;
        return ACTION_ERROR;
    }
}

/**
 * Perform a slave SDO write.
 *
 * This function must be called in a loop until it returns 1. For example,
 * this is a blocking SDO write using a typical busy loop:
 *
 * @code
 *     // Don't do this: leverage your event loop instead!
 *     while (app_canopen_write_sdo(app, 50, 0x60000108, 10, 0) == ACTION_WAITING)
 *         ;
 * @endcode
 *
 * The slave must be previously added with app_canopen_add_slave(). Also, keep
 * in mind that the various app_canopen_map_*() functions and this one share
 * the same context, so they cannot be mixed together.
 *
 * In `timeout` specifies the number of times the function will return waiting
 * for a response before giving up. To avoid timing out just specify `0`, in
 * which case a good strategy will be to perform a similar check at higher
 * level, eventually using app_canopen_reset(), to avoid waiting indefinitely
 * for a response.
 *
 * @param app      The application instance
 * @param slave_id The CANbus id of the slave node
 * @param map      The object dictionary entry to write on the slave node,
 *                 e.g. 0x63000110 writes 2 bytes into address $6300:01.
 * @param value    The value to write in the dictionary. The real number of
 *                 bytes to transfer is defined by map. This is an uint32_t
 *                 just for convenience, so it is easy to use constants.
 * @param timeout  Max number of times to call this function before giving up.
 * @return         `ACTION_DONE` on success, `ACTION_ERROR` on errors or
 *                 `ACTION_WAITING` on operation still in progress.
 */
ActionStatus
app_canopen_write_sdo(App *app, uint8_t slave_id, uint32_t map, uint32_t value, int timeout)
{
    CANopen *canopen;
    CO_Data *data;
    Slave *slave;
    UNS8 result;
    UNS32 code;

    canopen = app->canopen;
    data    = canopen->data;
    slave   = canopen->slaves + GetSDOClientFromNodeId(data, slave_id);

redo:
    switch (slave->step) {

    case 0:
        result = writeNetworkDict(data, slave_id, IDX(map), SUB(map), BYTES(map), 0, &value, 0);
        if (result == 0) {
            ++slave->step;
            return ACTION_WAITING;
        } else if (slave->is_blind) {
            return ACTION_DONE;
        } else {
            warning("SDO write error (id %u, entry %04X:%02X, value %0*X, result %X)",
                    slave_id, IDX(map), SUB(map), BYTES(map)*2, value, result);
            return ACTION_ERROR;
        }

    case 1:
        result = getWriteResultNetworkDict(data, slave_id, &code);

        switch (result) {

        case SDO_DOWNLOAD_IN_PROGRESS:
        case SDO_UPLOAD_IN_PROGRESS:
            if (slave->max_scans > 0 && ++slave->scans > slave->max_scans) {
                resetClientSDOLineFromNodeId(data, slave_id);
                reset_slave(slave);
                if (slave->is_blind) {
                    return ACTION_DONE;
                } else {
                    warning("SDO write timeout (id %u, entry %04X:%02X)",
                            slave_id, IDX(map), SUB(map));
                    return ACTION_ERROR;
                }
            }
            return ACTION_WAITING;

        case SDO_FINISHED:
            closeSDOtransfer(data, slave_id, SDO_CLIENT);
            reset_slave(slave);
            return ACTION_DONE;

        default:
            ++slave->errors;
            resetClientSDOLineFromNodeId(data, slave_id);

            if (slave->errors >= slave->max_errors) {
                reset_slave(slave);
                if (slave->is_blind) {
                    return ACTION_DONE;
                } else {
                    warning("SDO write failed (id %u, entry %04X:%02X, value %0*X, result %X, abort code %X)",
                            slave_id, IDX(map), SUB(map), SIZE(map)/4, value, result, code);
                    return ACTION_ERROR;
                }
            } else {
                --slave->step;
                goto redo;
            }
        }

    default:
        die("canopen: unexpected condition met during SDO write");
        slave->step = 0;
        return ACTION_ERROR;
    }
}

static ActionStatus
autoconf(App *app, uint8_t slave_id, int active)
{
    CANopen *canopen;
    CO_Data *data;
    int      n, step;
    uint32_t can_id;

    canopen = app->canopen;
    data    = canopen->data;
    step    = 0;

    /* Look up the active step in the TxPDO entries */
    for (n = 0; read_od(data, 0x14000120 + (n << 16), &can_id); ++n) {
        if ((can_id & 0x7F) != slave_id) {
            continue;
        }
        if (step == active) {
            /* How to get the entry to map (e.g. 0x61000110 for 16 DI)? */
            /* TODO */
        }
        ++step;
    }

    /* Look up the active step in the RxPDO entries */
    for (n = 0; read_od(data, 0x18000120 + (n << 16), &can_id); ++n) {
        if ((can_id & 0x7F) != slave_id) {
            continue;
        }
        if (step == active) {
            /* How to get the entry to map (e.g.0x62000108 for 8 DO)? */
            /* TODO */
        }
        ++step;
    }

    return ACTION_COMPLETE;
}

/**
 * app_canopen_autoconf_slaves:
 *
 * Autoconfigure the slaves based on some external knowledge, e.g. on a
 * CANfestival based stack the internal object dictionary is leveraged to get
 * the settings and mappings of every PDO message.
 *
 * Remember the real configuration procedure is triggered after the CANopen
 * stack is started, e.g. after calling app_canopen_start().
 *
 * @param app  The App instance
 * @return     The number of slaves to configure.
 **/
int
app_canopen_autoconf_slaves(App *app)
{
    CANopen *canopen;
    CO_Data *data;
    int n;
    uint8_t slave_id, slaves[128] = { 0 };
    uint32_t can_id;

    canopen = app->canopen;
    data    = canopen->data;

    /* Enable all slaves that have at least one TxPDO mapped */
    for (n = 0; read_od(data, 0x14000120 + (n << 16), &can_id); ++n) {
        slaves[can_id & 0x7F] = 1;
    }

    /* Also enable slaves that have one or more RxPDO mapped */
    for (n = 0; read_od(data, 0x18000120 + (n << 16), &can_id); ++n) {
        slaves[can_id & 0x7F] = 1;
    }

    n = 0;
    for (slave_id = 0; slave_id < 128; ++slave_id) {
        if (slaves[slave_id]) {
            app_canopen_add_slave(app, slave_id, autoconf);
            ++n;
        }
    }

    return n;
}
