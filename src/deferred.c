/**
 * @file  deferred.c
 * @brief A way to postpone execution of GSignal callbacks.
 *
 * Deferring the execution of a signal callback when the main cycle is
 * idle provides two main benefits:
 * 1. you are sure your handler will be executed with fresh data, particularly
 *    useful when used in conjunction with GSettings `changed` signal;
 * 2. multiple signal emissions will be squashed into a single call.
 *
 * The implementation avoids C closure complications, so it is based on
 * the assumtpion that the callback has 0, 1 or 2 arguments. The full
 * signature of the callback is:
 *     void callback(gpointer user_data, gpointer instance)
 * You can omit one or both arguments but you cannot change the order or
 * use more than 2 arguments.
 *     // These are all valid callbacks:
 *     void no_args_callback(void);
 *     void one_arg_callback(MyCustomData *data);
 *     void two_arg_callback(gpointer other_data, GtkWidget *signal_source);
 *     // PROBABLY WRONG: you are expecting the instance as first argument
 *     void wrong_order(GtkWidget *widget_that_generated_the_signal);
 *     // WRONG: incompatible data type, only two pointers are handled
 *     void invalid_type(gboolean flag);
 *     // WRONG: too many arguments, two maximum
 *     void too_many_args(gpointer a, gpointer b, gpointer c);
 */

#include "machinery.h"
#include "gtk.h"


typedef void (*Callback)(gpointer user_data, gpointer instance);

struct _Deferred {
    gpointer    instance;
    Callback    callback;
    gpointer    user_data;
    guint       source_id;
    gulong      handler_id;
};


static gboolean
deferred_launcher(Deferred *deferred)
{
    deferred->callback(deferred->user_data, deferred->instance);
    deferred->source_id = 0;
    return G_SOURCE_REMOVE;
}

static void
deferred_destroy(Deferred *deferred)
{
    if (deferred->source_id != 0) {
        g_source_remove(deferred->source_id);
    }
    g_free(deferred);
}

static void
deferred_call(Deferred *deferred)
{
    /* If a previous queue already exists, remove it and requeue it
     * to ensure proper ordering is retained */
    if (deferred->source_id != 0) {
        g_source_remove(deferred->source_id);
    }
    deferred->source_id = g_idle_add((GSourceFunc) deferred_launcher, deferred);
}


/**
 * Connect a deferred callback to a signal.
 *
 * Similar to g_signal_connect_swapped(), but deferring the execution of
 * @p callback when the main loop is in idle state.
 *
 * The returned pointer is owned by @p instance and should not be modified
 * or freed. It can be used to connect the same deferred callback to
 * different signals with machinery_deferred_connect_again(), effectively
 * allowing to squash different signal emissions into one single callback
 * call.
 *
 * @param instance   The instance to connect to
 * @param signal     The string identifying the signal
 * @param callback   The callback to connect
 * @param user_data  The first argument to pass when calling callback
 * @return           The deferred data.
 */
Deferred *
machinery_deferred_connect(gpointer instance, const gchar *signal,
                           GCallback callback, gpointer user_data)
{
    Deferred *deferred;

    g_return_val_if_fail(instance != NULL, NULL);
    g_return_val_if_fail(signal != NULL, NULL);
    g_return_val_if_fail(callback != NULL, NULL);

    deferred             = g_new(Deferred, 1);
    deferred->instance   = instance;
    deferred->callback   = (Callback) callback;
    deferred->user_data  = user_data;
    deferred->source_id  = 0;
    deferred->handler_id = g_signal_connect_data(instance, signal,
                                                 G_CALLBACK(deferred_call),
                                                 deferred,
                                                 (GClosureNotify) deferred_destroy,
                                                 G_CONNECT_SWAPPED);

    return deferred;
}

/**
 * Return the main handler id of the primary signal of a deferred connection.
 *
 * @param deferred  Deferred data to inspect
 * @return          The handler ID, or 0 on errors.
 */
gulong
machinery_deferred_get_handler_id(Deferred *deferred)
{
    g_return_val_if_fail(deferred != NULL, 0);
    return deferred->handler_id;
}

/**
 * Connect the same deferred callback to a secondary signal.
 *
 * This allows the same callback to be called from different signal. This
 * will allow to squash different signals into a single callback call:
 *     deferred = machinery_deferred_connect(settings, "changed::n1",
 *                                           G_CALLBACK(data_changed), NULL);
 *     machinery_deferred_connect_again(deferred, settings, "changed::n2");
 *     machinery_deferred_connect_again(deferred, settings, "changed::n3");
 *     // data_changed will be called only once when the cycle becomes idle
 *     g_settings_set_int(settings, "n1", 123);
 *     g_settings_set_int(settings, "n2", 456);
 *     g_settings_set_int(settings, "n3", 789);
 *
 * If used, keep in mind that the instance pointer passed to the callback will
 * always be the widget of the primary signal (the one passed to
 * machinery_deferred_connect()).
 *
 * @param deferred  Deferred data to use
 * @param instance  The instance to connect to
 * @param signal    The string identifying the signal
 * @return          The handler ID, always greater than 0 on success.
 */
gulong
machinery_deferred_connect_again(Deferred *deferred, gpointer instance, const gchar *signal)
{
    g_return_val_if_fail(deferred != NULL, 0);
    g_return_val_if_fail(instance != NULL, 0);
    g_return_val_if_fail(signal != NULL, 0);

    return g_signal_connect_swapped(instance, signal,
                                    G_CALLBACK(deferred_call), deferred);
}

/**
 * Convenient shortcut to connect to multiple signals on the same instance.
 *
 * Similar to machinery_deferred_connect_again(), but intended to be used
 * to connect to different signals on the same instance. Useful for
 * GSettings signal handlers, where the instance is always the same but
 * the detailed signals are different.
 *
 * ```
 * d = machinery_deferred_connect(object, "signal", callback, data);
 * // Defer a call to callback when some fields on settings change
 * machinery_deferred_connect_again(d, settings, "changed::field1");
 * machinery_deferred_connect_again(d, settings, "changed::field2");
 * machinery_deferred_connect_again(d, settings, "changed::field3");
 * // The following call is equivalent to the above three lines
 * machinery_deferred_connect_signals(d, settings,
 *                                    "changed::field1",
 *                                    "changed::field2",
 *                                    "changed::field3", NULL);
 * ```
 *
 * @param deferred  Deferred data to use
 * @param instance  The instance to connect to
 * @param ...       NULL terminated list of signal strings
 * @return          The handler ID, always greater than 0 on success.
 */
void
machinery_deferred_connect_signals(Deferred *deferred, gpointer instance, ...)
{
    va_list ap;
    const gchar *signal;

    va_start(ap, instance);
    while ((signal = va_arg(ap, const gchar *)) != NULL) {
        machinery_deferred_connect_again(deferred, instance, signal);
    }
    va_end(ap);
}

/**
 * Programmatically enqueue a deferred call.
 *
 * Queues the callback call in the next idle cycle. Usually this
 * function is implicitely executed by other APIs but there is some
 * situations when it is desirable to manually defer a call.
 *
 * @param deferred  Deferred data to inspect
 */
void
machinery_deferred_call(Deferred *deferred)
{
    g_return_if_fail(deferred != NULL);
    deferred_call(deferred);
}
