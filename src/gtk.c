/**
 * @file  gtk.c
 * @brief A collection of GTK based helper functions.
 *
 * Some general purpose helper functions that are not officially part
 * of the GTK framework.
 */

#include "machinery.h"
#include "gtk.h"
#include "push-button.h"

#include <string.h>
#include <stdlib.h>


/**
 * Get the index of a widget.
 *
 * The index of a widget is the number following the last dash on its
 * name, e.g. `mywidget-123` has index `123`.
 *
 * Indexes are used in radio group bindings: see
 * machinery_bind_settings_radio_group() for details.
 *
 * @param widget  A GTK widget
 * @return        The index of the widget
 */
gint
machinery_widget_get_index(GtkWidget *widget)
{
    const gchar *name, *trail;

    g_return_val_if_fail(GTK_IS_WIDGET(widget), 0);

    name  = gtk_widget_get_name(widget);
    trail = strchr(name, '-');
    if (trail == NULL) {
        return 0;
    }
    return atoi(trail + 1);
}

/**
 * Get the active radio button in a group.
 *
 * Traverse the group until the active radio button is found.
 * If there are no active buttons, `NULL` is returned.
 *
 * @param radio_group  The first radio button of the group
 * @return             The active radio button or `NULL`
 */
GtkRadioButton *
machinery_radio_group_get_active(GtkRadioButton *radio_group)
{
    GSList *node;
    GtkRadioButton *radio_button;

    g_return_val_if_fail(GTK_IS_RADIO_BUTTON(radio_group), NULL);

    node = gtk_radio_button_get_group(radio_group);
    while (node != NULL) {
        radio_button = node->data;
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio_button))) {
            return radio_button;
        }
        node = node->next;
    }

    return NULL;
}

/**
 * Ensure all GTK types are available to GtkBuilder based apps.
 **/
void
machinery_gtk_init(void)
{
    g_debug("machinery_gtk_init");
    g_type_ensure(MACHINERY_TYPE_PUSH_BUTTON);
}
