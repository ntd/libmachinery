/**
 * @file  ethercat.c
 * @brief EtherCAT master stack
 *
 * The master stack is based on IgH EtherCAT stack: see its [home
 * page](https://www.etherlab.org/en/ethercat/) for further details.
 *
 * A typical EtherCAT loop looks like the following:
 * ```
 * app_ethercat_init(app);
 *
 * // Automatically add all online slaves to this EtherCAT instance.
 * // For more advanced usage, you can manually call
 * // app_ethercat_add_slave() multiple times instead.
 * app_ethercat_autoadd_slaves(app);
 *
 * app_ethercat_start(app);
 *
 * // Get the images of the sync-managers needed by the app.
 * // Consult the slave configuration to see which data is handled
 * // by each sync-manager.
 * image_slave1_sm0 = app_ethercat_image(app, 1, 0);
 * image_slave1_sm1 = app_ethercat_image(app, 1, 1);
 * image_slave2_sm0 = app_ethercat_image(app, 2, 0);
 *
 * app_timer_init(app, 5000); // 5 msec
 * for (;;) {
 *     app_ethercat_receive(app);
 *     // ... do something with the images ...
 *     app_ethercat_send(app);
 *     app_timer_wait(app);
 * }
 * app_timer_free(app);
 * app_ethercat_stop(app);
 * app_ethercat_free(app);
 * ```
 */

#include "machinery.h"
#include "ethercat-internal.h"

#include <ecrt.h>
#include <sched.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef enum {
    SIZE_1BYTES,
    SIZE_2BYTES,
    SIZE_4BYTES,
    SIZE_SENTINEL,
} Size;

#define IDX(map)    ((map) >> 16)
#define SUB(map)    (((map) & 0xFF00) >> 8)
#define BITS(map)   ((map) & 0xFF)
#define BYTES(map)  ((BITS(map) + 7) / 8)

/* Using a function for SIZE() because it seems more appropriate */
static int SIZE(uint32_t map)
{
    int y = BYTES(map);
    return y == 1 ? SIZE_1BYTES : y == 2 ? SIZE_2BYTES : y == 4 ? SIZE_4BYTES : SIZE_SENTINEL;
}


typedef struct {
    const MachinerySlaveConfig *config;
    ec_slave_config_t *         slave_config;
    ec_sdo_request_t *          sdo[SIZE_SENTINEL];
    uint32_t                    sdo_map;
    int *                       offsets;
} Slave;

/* Internal struct, pointed by app->ethercat */
typedef struct {
    ec_master_t *master;
    unsigned     nslaves;
    Slave *      slaves;
    ec_domain_t *domain;
    uint8_t *    image;
} EtherCAT;


static int
register_syncmanager(ec_domain_t *domain, ec_slave_config_t *sc, ec_sync_info_t *sm)
{
    int i, j, k, offset;
    ec_pdo_info_t *pdo;
    ec_pdo_entry_info_t *entry;
    unsigned int bitpos;

    offset = -1;
    for (i = 0; i < sm->n_pdos; ++i) {
        pdo = sm->pdos + i;
        for (j = 0; j < pdo->n_entries; ++j) {
            entry = pdo->entries + j;
            k = ecrt_slave_config_reg_pdo_entry(sc, entry->index, entry->subindex, domain, &bitpos);
            if (k < 0) {
                warning("Failed to register PDO %04X:%02X",
                        entry->index, entry->subindex);
                return k;
            }
            /* The first valid value is used as syncmanager offset */
            if (offset < 0) {
                offset = k;
            }
        }
    }

    return offset;
}

static Slave *
slave_new(EtherCAT *ethercat, const MachinerySlaveConfig *config)
{
    Slave *slave;

    ++ethercat->nslaves;
    ethercat->slaves = realloc(ethercat->slaves, sizeof(Slave) * ethercat->nslaves);

    slave = ethercat->slaves + ethercat->nslaves - 1;
    slave->config = config;
    slave->offsets = malloc(sizeof(int) * config->nsyncs);

    return slave;
}

static void
register_slave(EtherCAT *ethercat, ec_slave_config_t *sc, const MachinerySlaveConfig *config)
{
    Slave *slave;
    int i;
    ec_sync_info_t *sm;

    slave = slave_new(ethercat, config);
    slave->slave_config = sc;

    /* Create SDO download/upload contexts. Ignore errors here, as there
     * are many EtherCAT devices without SDO support.
     * The `$1000:00` object has been chosen as dummy entry because
     * it is always present if SDO support is available. */
    slave->sdo_map = 0;
    slave->sdo[SIZE_1BYTES] = ecrt_slave_config_create_sdo_request(sc, 0x1000, 0x00, 1);
    slave->sdo[SIZE_2BYTES] = ecrt_slave_config_create_sdo_request(sc, 0x1000, 0x00, 2);
    slave->sdo[SIZE_4BYTES] = ecrt_slave_config_create_sdo_request(sc, 0x1000, 0x00, 4);

    for (i = 0; i < config->nsyncs; ++i) {
        sm = config->syncs + i;
        slave->offsets[i] = register_syncmanager(ethercat->domain, sc, sm);
    }
}

static ActionStatus
add_slave(EtherCAT *ethercat, uint16_t alias, uint16_t position,
          const MachinerySlaveConfig *config)
{
    ec_slave_config_t *sc = ecrt_master_slave_config(ethercat->master,
                                                     alias, position,
                                                     config->vendor_id,
                                                     config->product_code);
    if (sc == NULL) {
        warning("Failed to get slave %u-%u configuration (should be a %s module)",
                alias, position, config->name);
        return ACTION_ERROR;
    }

    if (config->nsyncs <= 0) {
        /* No sync managers to configure: do nothing */
    } else if (config->syncs == NULL) {
        /* `nsyncs > 0` but `syncs == NULL` */
        warning("Sync-managers not configured for slave %u-%u (%s module)",
                alias, position, config->name);
        return ACTION_ERROR;
    } else if (ecrt_slave_config_pdos(sc, config->nsyncs, config->syncs) != 0) {
        warning("Failed to configure PDOs on slave %u-%u (%s module)",
                alias, position, config->name);
        return ACTION_ERROR;
    }

    register_slave(ethercat, sc, config);
    return ACTION_DONE;
}

static ActionStatus
autoadd_slave(EtherCAT *ethercat, unsigned pos)
{
    unsigned i;
    ec_slave_info_t info;
    MachinerySlaveConfig config;
    ActionStatus status;

    if (ecrt_master_get_slave(ethercat->master, pos, &info) != 0) {
        warning("ecrt_master_get_slave() for slave at position %u failed", pos);
        return ACTION_ERROR;
    }

    /* Fetch info online to build the configuration object */
    config.name = info.name;
    config.description = "";
    config.vendor_id = info.vendor_id;
    config.product_code = info.product_code;
    config.revision_number = info.revision_number;
    config.nsyncs = info.sync_count;
    config.syncs = calloc(info.sync_count, sizeof(ec_sync_info_t));
    for (i = 0; i < info.sync_count; ++i) {
        if (ecrt_master_get_sync_manager(ethercat->master, pos, i, config.syncs + i) != 0) {
            warning("ecrt_master_get_sync_manager() for sync manager %u on slave at position %u failed",
                    i, pos);
            free(config.syncs);
            return ACTION_ERROR;
        }
    }

    /* Add the new slave */
    status = add_slave(ethercat, info.alias, info.position, &config);
    free(config.syncs);
    return status;
}

Slave *
get_slave(EtherCAT *ethercat, unsigned nslave)
{
    if (nslave > ethercat->nslaves) {
        warning("Cannot get slave data: nslave > nslaves (%u > %u)",
                nslave, ethercat->nslaves);
        return NULL;
    }

    return ethercat->slaves + nslave;
}

static ec_sdo_request_t *
get_sdo_request(Slave *slave, unsigned nslave, Size size)
{
    ec_sdo_request_t *sr;
    sr = slave->sdo[size];
    if (sr == NULL) {
        warning("SDO not supported by slave %u", nslave);
    }
    return sr;
}

static ec_sdo_request_t *
get_sdo_request_for_map(Slave *slave, unsigned nslave, uint32_t map)
{
    int size = SIZE(map);
    if (size == SIZE_SENTINEL) {
        warning("Map 0x%X: %u data bits not supported yet", map, BITS(map));
        return NULL;
    }

    return get_sdo_request(slave, nslave, size);
}

static int
map_changed(Slave *slave, uint32_t map, const char *dir)
{
    if (slave->sdo_map != 0 && slave->sdo_map != map) {
        warning("Incoming SDO %s request with an old request still running (new map = 0x%X, old map = 0x%X): new request ignored",
                dir, map, slave->sdo_map);
        return 1;
    }
    return 0;
}

static void
ethercat_send(EtherCAT *ethercat)
{
    ecrt_domain_queue(ethercat->domain);
    ecrt_master_send(ethercat->master);
}

static void
ethercat_receive(EtherCAT *ethercat)
{
    ecrt_master_receive(ethercat->master);
    ecrt_domain_process(ethercat->domain);
}


/**
 * Initialize an application instance for EtherCAT interaction.
 *
 * This should be called before any other EtherCAT related call. Any blocking
 * error will generate a relevant message and will quit the current process.
 *
 * @param app The App instance to initialize
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_init(App *app)
{
    EtherCAT *ethercat;

    if (app->ethercat == NULL) {
        ethercat = malloc(sizeof(EtherCAT));
        ethercat->master = NULL;
        ethercat->nslaves = 0;
        ethercat->slaves = NULL;
        ethercat->domain = NULL;
        ethercat->image = NULL;
        app->ethercat = ethercat;
    } else {
        ethercat = app->ethercat;
    }

    ethercat->master = ecrt_request_master(0);
    if (ethercat->master == NULL) {
        warning("ecrt_request_master() failed");
        return ACTION_ERROR;
    }

    ethercat->domain = ecrt_master_create_domain(ethercat->master);
    if (ethercat->domain == NULL) {
        warning("ecrt_master_create_domain() failed");
        return ACTION_ERROR;
    }

    return ACTION_DONE;
}

/**
 * Finalize the EtherCAT communication of an application.
 *
 * This is the counterpart of app_ethercat_init().
 *
 * @param app The instance to free
 */
void
app_ethercat_free(App *app)
{
    EtherCAT *ethercat = app->ethercat;

    if (ethercat != NULL) {
        if (ethercat->slaves != NULL) {
            int i;
            for (i = 0; i < ethercat->nslaves; ++i) {
                free(ethercat->slaves[i].offsets);
            }
            free(ethercat->slaves);
        }
        if (ethercat->master != NULL) {
            ecrt_release_master(ethercat->master);
            ethercat->master = NULL;
        }
        free(ethercat);
        app->ethercat = NULL;
    }
}

/**
 * Get the EtherCAT master.
 *
 * If non-NULL, the returned pointer can be safely casted to
 * `ec_master_t *`.
 *
 * @param app The application instance
 * @return The ec_master_t pointer or NULL on errors.
 */
void *
app_ethercat_get_master(App *app)
{
    EtherCAT *ethercat;

    if (app == NULL || app->ethercat == NULL) {
        return NULL;
    }

    ethercat = app->ethercat;
    return ethercat->master;
}

/**
 * Manually add a single slave.
 *
 * Adds a single slave with a specific configuration, without needing to have
 * it online. This is in contrast to app_ethercat_autoadd_slaves, where all
 * connected slaves are added and autoconfigured.
 *
 * The `config` parameter can be created by hand or you can reuse one
 * of the predefined configurations: check `ethercat-slaves.h` for the
 * available ones.
 *
 * @param app       The application instance
 * @param alias     The alias code, or 0 if positions are used
 * @param position  The slave position, or 0 if aliases are used
 * @param config    The configuration record, i.e. a pointer to a
 *                  MachinerySlaveConfig struct
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_add_slave(App *app, uint16_t alias, uint16_t position, const void *config)
{
    EtherCAT *ethercat = app->ethercat;
    return add_slave(ethercat, alias, position, config);
}

/**
 * Autoconfigure the connected slaves.
 *
 * Fetches slaves information online (they must be already powered up and
 * connected to the master) and autoconfigure them.
 *
 * How the PDOs are mapped into the process image depends on the default slave
 * settings. For complex modules (e.g. servodrives) the defaults are often not
 * correct: on these cases, it is more convenient to manually add them one by
 * one with app_ethercat_add_slave() and a custom configuration.
 *
 * @param app The application instance
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_autoadd_slaves(App *app)
{
    EtherCAT *ethercat = app->ethercat;
    ec_master_info_t master;
    unsigned i;
    ActionStatus status;

    if (ecrt_master(ethercat->master, &master) != 0) {
        warning("ecrt_master() failed");
        return ACTION_ERROR;
    }

    for (i = 0; i < master.slave_count; ++i) {
        status = autoadd_slave(ethercat, i);
        if (status != ACTION_DONE) {
            return status;
        }
    }

    return ACTION_DONE;
}

/**
 * Configure a slave with custom SDO requests.
 *
 * This function must be called before the EtherCAT thread has been started
 * (i.e. before clling app_ethercat_start() or similar).
 *
 * `nslave` is the 0-based order in which the slave has been added with
 * app_ethercat_add_slave(), so it is `0` for the first added slave, `1`
 * for the second one and so on.
 *
 * After `nslave` you must pass pairs of `uint32_t` indicating the map
 * and the value to set via SDO requests. Check the
 * app_ethercat_sdo_download() API to know what the map is. The last map
 * must be `((uint32_t ) 0)` to stop the parsing. The more descriptive
 * constant `NULLMAP` can be used instead.
 *
 * The following example configure 3 entries on the 8th slave: 2810:05,
 * 2410:01 and 3310:03 (16, 8 and 16 bits respectively).
 * ```
 * app_ethercat_configure_slave(app, 7,
 *                              (uint32_t) 0x28100510, (uint32_t) 0x302,
 *                              (uint32_t) 0x24100108, (uint32_t) 0,
 *                              (uint32_t) 0x33100310, (uint32_t) 10000,
 *                              NULLMAP);
 * ```
 *
 * @param app The application instance
 * @param nslave 0-index based slave number
 * @param ... A list of uint32_t pairs, ended with `(uint32_t ) 0`
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_configure_slave(App *app, unsigned nslave, ...)
{
    Slave *slave;
    ec_slave_config_t *sc;
    va_list args;
    uint32_t map, value;
    uint8_t data[4];
    size_t size;
    ActionStatus status;

    if ((slave = get_slave(app->ethercat, nslave)) == NULL) {
        return ACTION_ERROR;
    }

    sc = slave->slave_config;
    status = ACTION_DONE;

    va_start(args, nslave);
    while ((map = va_arg(args, uint32_t)) != NULLMAP) {
        value = va_arg(args, uint32_t);
        size = BYTES(map);
        switch (size) {
        case 1:
            EC_WRITE_U8(data, value);
            break;
        case 2:
            EC_WRITE_U16(data, value);
            break;
        case 4:
            EC_WRITE_U32(data, value);
            break;
        default:
            warning("ecrt_slave_config_sdo: %u bits data not supported (0x%08X)",
                    BITS(map), map);
            status = ACTION_ERROR;
            continue;
        }
        if (ecrt_slave_config_sdo(sc, IDX(map), SUB(map), data, size)) {
            warning("ecrt_slave_config_sdo() failed for 0x%08X", map);
            status = ACTION_ERROR;
        }
    }
    va_end(args);

    return status;
}

/**
 * Configure the watchdog of a slave.
 *
 * This function must be called before the EtherCAT thread has been started
 * (i.e. before clling app_ethercat_start() or similar).
 *
 * `nslave` is the 0-based order in which the slave has been added with
 * app_ethercat_add_slave(), so it is `0` for the first added slave, `1`
 * for the second one and so on.
 *
 * Check `ecrt_slave_config_watchdog()` to know how the arguments are used.
 *
 * @param app The application instance
 * @param nslave 0-index based slave number
 * @param divider Watchdog divider
 * @param intervals Watchdog intervals
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_set_watchdog(App *app, unsigned nslave,
                            uint16_t divider, uint16_t intervals)
{
    Slave *slave;
    ec_slave_config_t *sc;

    if ((slave = get_slave(app->ethercat, nslave)) == NULL) {
        return ACTION_ERROR;
    }

    sc = slave->slave_config;
    ecrt_slave_config_watchdog(sc, divider, intervals);
    return ACTION_DONE;
}

/**
 * Start the EtherCAT slaves.
 *
 * Configures all slaves. This should be called only after having added
 * all the slaves.
 *
 * This function blocks until all slaves have been configured or until
 * the 100 seconds limit is hit. For a non-blocking approach, check
 * app_ethercat_start_async().
 *
 * @param app The application instance
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_start(App *app)
{
    ActionStatus result;
    EtherCAT *ethercat = app->ethercat;
    int n;
    ec_slave_info_t slave;

    result = app_ethercat_start_async(app);
    if (result != ACTION_DONE) {
        return result;
    }

    /* Give 200000×500 usec (i.e. 100 seconds) to bring up
     * the full EtherCAT chain */
    for (n = 0; n < 200000; ++n) {
        ethercat_send(ethercat);
        usleep(500);
        ethercat_receive(ethercat);
        if (app_ethercat_online(app) == ACTION_DONE) {
            return ACTION_DONE;
        }
    }

    /* Show which slaves did not go OP */
    for (n = 0; n < ethercat->nslaves; ++n) {
        ecrt_master_get_slave(ethercat->master, n, &slave);
        if (slave.al_state != EC_AL_STATE_OP) {
            warning("Slave %s still in %u", slave.name, slave.al_state);
        }
    }
    return ACTION_WAITING;
}

/**
 * Start the EtherCAT slaves.
 *
 * Starts the slave configuration process. This should be called only
 * after having added all the slaves.
 *
 * To know when the configuration has been completed, call
 * app_ethercat_online().
 *
 * @param app The application instance
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_start_async(App *app)
{
    EtherCAT *ethercat = app->ethercat;
    struct sched_param param;

    if (ecrt_master_activate(ethercat->master) != 0) {
        warning("ecrt_master_activate() failed");
        return ACTION_ERROR;
    }

    ethercat->image = ecrt_domain_data(ethercat->domain);
    if (ethercat->image == NULL) {
        warning("ecrt_domain_data() failed");
        return ACTION_ERROR;
    }

    param.sched_priority = sched_get_priority_max(SCHED_FIFO);
    if (sched_setscheduler(0, SCHED_FIFO, &param) == -1) {
        warning("sched_setscheduler() failed");
        /* Do not error out: the code can still work
         * return ACTION_ERROR;
         */
    }

    return ACTION_DONE;
}

/**
 * Check if all the slaves are in OP state.
 *
 * It should be called periodically after app_ethercat_start_async() to
 * check when all slaves have been configured.
 *
 * @param app The application instance
 * @return `ACTION_DONE` if all slaves in OP state, `ACTION_WAITING` otherwise.
 */
ActionStatus
app_ethercat_online(App *app)
{
    EtherCAT *ethercat = app->ethercat;
    ec_master_state_t state;

    ecrt_master_state(ethercat->master, &state);
    return state.al_states == EC_AL_STATE_OP ? ACTION_DONE : ACTION_WAITING;
}

/**
 * Stop the EtherCAT thread.
 *
 * Reset all slaves to their INIT state.
 *
 * @param app The application instance
 */
void
app_ethercat_stop(App *app)
{
    EtherCAT *ethercat = app->ethercat;
    ethercat->image = NULL;
    ecrt_master_deactivate(ethercat->master);
}

/**
 * Get the process image of a specific sync manager.
 *
 * Get the image of a single sync-manager. This function must be called
 * after the EtherCAT thread has been started (i.e. after the
 * app_ethercat_start() or app_ethercat_start_async() call).
 *
 * `nslave` is the 0-based order in which the slave has been added with
 * app_ethercat_add_slave(), so it is `0` for the first added slave, `1`
 * for the second one and so on.
 *
 * The EtherCAT standard ensures that the data returned by each
 * sync-manager is properly packed: consult the slave configuration to
 * see which data is handled by each sync-manager.
 *
 * @param app The application instance
 * @param nslave 0-index based slave number
 * @param nsync 0-index based sync-manager number
 * @return The sync manager image or NULL on errors.
 */
void *
app_ethercat_image(App *app, unsigned nslave, unsigned nsync)
{
    EtherCAT *ethercat = app->ethercat;
    Slave *slave;

    if (ethercat->image == NULL) {
        warning("Cannot get slave data image: EtherCAT not started yet");
        return NULL;
    }

    if ((slave = get_slave(ethercat, nslave)) == NULL) {
        return NULL;
    }

    if (nsync > slave->config->nsyncs) {
        warning("Cannot get the image: nsync > nsyncs (%u > %u)",
                nsync, slave->config->nsyncs);
        return NULL;
    }

    return ethercat->image + slave->offsets[nsync];
}

/**
 * Set the SDO timeout.
 *
 * If the request cannot be processed in the specified time, it will be
 * marked as failed.
 *
 * @param app    The application instance
 * @param nslave 0-index based slave number, see app_ethercat_image()
 * @param ms     New timeout value (in milliseconds)
 * @return       `ACTION_DONE` on success or `ACTION_ERROR` on errors
 */
ActionStatus
app_ethercat_sdo_timeout(App *app, unsigned nslave, uint32_t ms)
{
    Slave *slave;
    ec_sdo_request_t *sr;
    Size size;

    if ((slave = get_slave(app->ethercat, nslave)) == NULL) {
        return ACTION_ERROR;
    }

    for (size = SIZE_1BYTES; size < SIZE_SENTINEL; ++size) {
        if ((sr = get_sdo_request(slave, nslave, size)) == NULL) {
            return ACTION_ERROR;
        }
        ecrt_sdo_request_timeout(sr, ms);
    }

    return ACTION_DONE;
}

/**
 * Runtime SDO upload.
 *
 * Similar to app_ethercat_sdo_download(), but used for uploading data,
 * i.e. reading from the devices.
 *
 * @param app    The application instance
 * @param nslave 0-index based slave number, see app_ethercat_image()
 * @param map    Entry identification, see app_ethercat_sdo_download()
 * @param value  Where to store the result.
 * @return       `ACTION_COMPLETE` on completion,
 *               `ACTION_WAITING` if there is a pending operation,
 *               `ACTION_ERROR` on errors
 */
ActionStatus
app_ethercat_sdo_upload(App *app, unsigned nslave, uint32_t map, void *value)
{
    Slave *slave;
    ec_sdo_request_t *sr;
    uint8_t *src;

    if ((slave = get_slave(app->ethercat, nslave)) == NULL ||
        (sr = get_sdo_request_for_map(slave, nslave, map)) == NULL ||
        map_changed(slave, map, "upload")) {
        return ACTION_ERROR;
    }

    if (slave->sdo_map == 0) {
        /* Start the SDO request */
        ecrt_sdo_request_index(sr, IDX(map), SUB(map));
        ecrt_sdo_request_read(sr);
        slave->sdo_map = map;
        return ACTION_WAITING;
    }

    switch (ecrt_sdo_request_state(sr)) {

    case EC_REQUEST_SUCCESS:
        slave->sdo_map = 0;
        src = ecrt_sdo_request_data(sr);
        switch (BYTES(map)) {
        case 1:
            * (uint8_t *) value = EC_READ_U8(src);
            break;
        case 2:
            * (uint16_t *) value = EC_READ_U16(src);
            break;
        case 4:
            * (uint32_t *) value = EC_READ_U32(src);
            break;
        default:
            warning("SDO upload 0x%X: %u bits data not supported",
                    map, BITS(map));
            return ACTION_ERROR;
        }
        return ACTION_COMPLETE;

    case EC_REQUEST_ERROR:
        warning("SDO upload 0x%X failed: check kernel logs", map);
        slave->sdo_map = 0;
        return ACTION_ERROR;

    default:
        return ACTION_WAITING;
    }
}

/**
 * Runtime SDO download.
 *
 * Available after slave activation. Only numerical data are supported,
 * up to 32 bits.
 *
 * To simplify the code and support as many slaves as possible, the SDO
 * handling is exclusive per slave, i.e. there can be no more than one
 * SDO request (download or upload) on each slave at any given time.
 *
 * This function must be called repeatedly without changing the
 * arguments until `ACTION_COMPLETE` or `ACTION_ERROR` is returned. You
 * can integrate it into your loop to avoid blocking, e.g.
 * ```
 * static ActionStatus status = ACTION_WAITING;
 * switch (status) {
 * case ACTION_ERROR:
 *     printf("Error during SDO download!\n");
 *     break;
 * case ACTION_COMPLETE:
 *     printf("SDO download completed successfully.\n");
 *     break;
 * default:
 *     status = app_ethercat_sdo_download(...);
 *     break;
 * }
 * ```
 *
 * The `map` argument is a 32 bit unsigned that univoquely identifies an
 * object dictionary entry: the first 16 bit are the index, then 8 bit
 * for the subindex and the last 8 bit for data size, e.g. `0x10000010`
 * should be splitted into `0x1000 0x00 0x10` and identifies the 16 bit
 * value (last field) of the register $1000:00 (first two fields).
 *
 * @param app    The application instance
 * @param nslave 0-index based slave number, see app_ethercat_image()
 * @param map    Entry identification
 * @param value  The value to write in the dictionary. The real number of
 *               bytes to transfer is defined by map. This is an uint32_t
 *               just for convenience, so it is easy to use constants.
 * @return       `ACTION_COMPLETE` on completion,
 *               `ACTION_WAITING` if there is a pending operation,
 *               `ACTION_ERROR` on errors
 */
ActionStatus
app_ethercat_sdo_download(App *app, unsigned nslave, uint32_t map, uint32_t value)
{
    Slave *slave;
    ec_sdo_request_t *sr;

    if ((slave = get_slave(app->ethercat, nslave)) == NULL ||
        (sr = get_sdo_request_for_map(slave, nslave, map)) == NULL ||
        map_changed(slave, map, "download")) {
        return ACTION_ERROR;
    }

    if (slave->sdo_map == 0) {
        /* Spawn a new SDO request */
        uint8_t *dst = ecrt_sdo_request_data(sr);
        switch (BYTES(map)) {
        case 1:
            EC_WRITE_U8(dst, value);
            break;
        case 2:
            EC_WRITE_U16(dst, value);
            break;
        case 4:
            EC_WRITE_U32(dst, value);
            break;
        default:
            warning("SDO download 0x%X: %u bits data not supported",
                    map, BITS(map));
            return ACTION_ERROR;
        }
        ecrt_sdo_request_index(sr, IDX(map), SUB(map));
        ecrt_sdo_request_write(sr);
        slave->sdo_map = map;
        return ACTION_WAITING;
    }

    switch (ecrt_sdo_request_state(sr)) {

    case EC_REQUEST_SUCCESS:
        slave->sdo_map = 0;
        return ACTION_COMPLETE;

    case EC_REQUEST_ERROR:
        warning("SDO download 0x%X failed: check kernel logs", map);
        slave->sdo_map = 0;
        return ACTION_ERROR;

    default:
        return ACTION_WAITING;
    }
}

/**
 * Update the slaves data with the process image data.
 *
 * @param app The application instance
 */
void
app_ethercat_send(App *app)
{
    EtherCAT *ethercat = app->ethercat;
    ethercat_send(ethercat);
}

/**
 * Update the process image data with the slaves data.
 *
 * @param app The application instance
 * @return `ACTION_DONE` on success or `ACTION_ERROR` on errors.
 */
ActionStatus
app_ethercat_receive(App *app)
{
    ec_domain_state_t domain_state;
    EtherCAT *ethercat = app->ethercat;
    ethercat_receive(ethercat);
    ecrt_domain_state(ethercat->domain, &domain_state);
    return domain_state.wc_state == EC_WC_INCOMPLETE ? ACTION_ERROR : ACTION_DONE;
}
