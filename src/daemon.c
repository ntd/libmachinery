/**
 * @file  daemon.c
 * @brief Function related to daemon handling
 */

#include "machinery.h"

#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libdaemon/dfork.h>
#include <libdaemon/dsignal.h>
#include <libdaemon/dlog.h>
#include <libdaemon/dpid.h>

static const char *
get_pidfile(void)
{
    return daemon_pid_file_ident;
}

/**
 * Initialize an application instance for daemon handling.
 *
 * This should be called before any other daemon related call. Any blocking
 * error will generate a relevant message and will quit the current process.
 *
 * The module argument specifies what "component" of the app is daemonizing,
 * e.g. "map" or "cycle". You can pass NULL if there is only one daemon per
 * application.
 *
 * @param app    An App instance to initialize
 * @param module The name of the caller module or NULL
 */
void
app_daemon_init(App *app, const char *module)
{
    char *path;

    if (daemon_reset_sigs(-1) < 0) {
        die_with_errno("daemon_reset_sigs");
        return;
    }

    if (daemon_unblock_sigs(-1) < 0) {
        die_with_errno("daemon_unblock_sigs");
        return;
    }

    /* Set the full module name into app->module */
    if (module != NULL) {
        app->module = malloc_printf("%s-%s", app->name, module);
    } else {
        app->module = strdup(app->name);
    }

    /* Obtain the path to the pid file */
    path = getenv("XDG_RUNTIME_DIR");
    if (path != NULL && path[0] != '\0') {
        app->pid = malloc_printf("%s/%s.pid", path, app->module);
    } else {
        /* XDG_RUNTIME_DIR undefined. It could happen e.g. when you `su` into
         * another user account: https://github.com/systemd/systemd/issues/825
         * In this case, try to use a popular fallback value. */
        long uid;
        /* Use a long here because uid_t could be signed or unsigned: see e.g.
         * https://stackoverflow.com/a/21370396/186347 */
        uid = (long) getuid();
        if (uid == 0) {
            /* For root user, avoid creating the `0` subdir */
            app->pid = malloc_printf("/var/run/%s.pid", app->module);
        } else {
            app->pid = malloc_printf("/var/run/%ld/%s.pid", uid, app->module);
        }
    }

    daemon_pid_file_ident = app->pid;
    daemon_pid_file_proc  = get_pidfile;
    daemon_log_ident      = app->module;

    info("PID file: '%s'", app->pid);
}

/**
 * Finalize the daemon handling section of an application.
 *
 * This is the counterpart of app_init_daemon().
 *
 * @param app The instance to free
 */
void
app_daemon_free(App *app)
{
    if (app->pid != NULL) {
        daemon_retval_send(APP_INVALID);
        daemon_signal_done();
        daemon_pid_file_remove();

        if (app->module != NULL) {
            free(app->module);
            app->module = NULL;
        }

        free(app->pid);
        app->pid = NULL;
    }
}

/**
 * Kill any other instance of this application daemon.
 *
 * If there is no daemon, this function resolves to a no-op.
 *
 * @param app An App instance
 */
void
app_daemon_kill(App *app)
{
    daemon_pid_file_kill_wait(SIGTERM, 5);
}

/**
 * Initialize an application instance for daemon handling.
 *
 * The current process is daemonized. The control is returned to the caller
 * only for the daemonized process. Any blocking error will generate a
 * relevant message and will quit the process.
 *
 * @param app    The App instance to daemonize
 * @return 0 if there already is a preexisting daemon running or 1 if the
 *         process has been daemonized successfully.
 */
int
app_daemon_start(App *app)
{
    pid_t pid;

    if (daemon_pid_file_is_running() >= 0) {
        return 0;
    }

    if (daemon_retval_init() < 0) {
        die_with_errno("daemon_retval_init");
    }

    pid = daemon_fork();
    if (pid < 0) {
        daemon_retval_done();
        die_with_errno("daemon_fork");
    } else if (pid > 0) {
        quit((AppStatus) daemon_retval_wait(5));
    }

    if (daemon_close_all(-1) < 0) {
        daemon_retval_send(APP_FAILURE);
        die_with_errno("daemon_close_all");
    }

    if (daemon_pid_file_create() < 0) {
        daemon_retval_send(APP_FAILURE);
        die_with_errno("daemon_pid_file_create");
    }

    if (daemon_signal_init(SIGINT, SIGTERM, SIGQUIT, SIGHUP, 0) < 0) {
        daemon_retval_send(APP_FAILURE);
        die_with_errno("daemon_signal_init");
    }

    daemon_retval_send(APP_SUCCESS);
    return 1;
}
