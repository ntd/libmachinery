#include "machinery.h"

#include <ctype.h>
#include <stdio.h>

int
main(int argc, char *argv[])
{
    char *ptr, *tail, *line = NULL;
    size_t len = 0U;
    uint32_t hash;

    while (getline(&line, &len, stdin) >= 0) {
        for (tail = ptr = line; *ptr != '\0'; ++ptr) {
            tail = isspace((unsigned char) *ptr) ? tail : ptr;
        }
        *(tail+1) = '\0';
        hash = get_hash(line);
        printf("[0x%08x] = '%s',\n", hash, line);
    }

    return 0;
}
