/**
 * @mainpage The machinery library
 *
 * `libmachinery` is a (not so) general purpose library for speeding up the
 * developmenent of CANopen based machineries. It provides what already used
 * in multiple projects of the same type, e.g.:
 *
 * * a basic logging system;
 * * memory sharing between processes;
 * * extended scripting support via the Lua language;
 * * a master CANopen stack;
 * * a master EtherCAT stack.
 *
 * The library makes extensive use of some GNU/Linux subsystem, e.g. shared
 * memory objects and syslog, so it is not intended to be portable across
 * multiple platforms.
 */

#include "machinery.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**
 * Concatenate two strings and store the result in newly allocated memory.
 *
 * @param str1 The initial string
 * @param str2 The final string
 * @return     The newly allocated string
 */
char *
malloc_strcat(const char *str1, const char *str2)
{
    size_t l1    = strlen(str1);
    size_t l2    = strlen(str2);
    char *result = malloc(l1 + l2 + 1);
    memcpy(result, str1, l1);
    memcpy(result + l1, str2, l2 + 1);
    return result;
}

/**
 * Allocate and populate a string using a printf interface.
 *
 * Allocates a new string with malloc and populates it using the provided
 * format and arguments, much like printf() works. The caller is responsible
 * to free the result with free() when no longer needed.
 *
 * @param format The format string, as in printf() and the like
 * @param ...    Additional arguments
 * @return       The newly allocated string with the result
 */
char *
malloc_printf(const char *format, ...)
{
    va_list args1, args2;
    size_t size;
    char *string;

    va_start(args1, format);
    va_copy(args2, args1);

    size = vsnprintf(NULL, 0, format, args1) + 1;
    string = malloc(size);
    vsnprintf(string, size, format, args2);

    va_end(args2);
    va_end(args1);

    return string;
}

/**
 * A generic hash function for short strings.
 *
 * This can be used to transform a short string to a 32 bit integer, useful
 * for hashing a small set of predefined strings.
 *
 * DO NOT USE FOR CUSTOM STRING! The collision probability is quite high.
 *
 * @param text The subject string
 * @return     The hash of \p text
 */
uint32_t
get_hash(const char *text)
{
    uint32_t hash = 5381;
    const char *p;

    for (p = text; *p != '\0'; ++p) {
        hash = hash * 33 + *p;
    }

    return hash;
}
