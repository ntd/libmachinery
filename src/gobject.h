#ifndef MACHINERY_GOBJECT_H_
#define MACHINERY_GOBJECT_H_

#include <glib-object.h>
#include <glib.h>
#include <glib/gi18n.h>

#define P_(String)  C_("Property", String)

#endif /* MACHINERY_GOBJECT_H_ */
