/**
 * @file  lua.c
 * @brief Lua scripting and interaction
 *
 * The behavior of the cycle and user interface is driven by Lua scripts.
 */

#include "machinery.h"

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <stdlib.h>
#include <string.h>


static int
panic_callback(lua_State *L)
{
    char *text, *str1, *str2;
    int level;
    lua_Debug info;

    text = malloc_printf("lua: script error (%s)\n", lua_tostring(L, -1));
    for (level = 1; lua_getstack(L, level, &info) == 1; ++level) {
        if (lua_getinfo(L, "nSl", &info) == 0)
            break;
        str1 = text;
        str2 = malloc_printf("#%d %s (%s), %s:%d\n",
                             level, info.name, info.namewhat,
                             info.short_src, info.currentline);
        text = malloc_strcat(str1, str2);
        free(str1);
        free(str2);
    }
    die(text);
    free(text);
    return 0;
}

static void
ensure_in_lua_path(lua_State *L, const char *folder)
{
    const char *path;

    lua_getglobal(L, "package");
    lua_getfield(L, -1, "path");
    path = lua_tostring(L, -1);

    if (strstr(path, folder) == NULL) {
        /* `folder` not found inside `path` */
        size_t path_len, folder_len;
        char *new_path, *dst;

        path_len = strlen(path);
        folder_len = strlen(folder);
        dst = new_path = malloc(path_len + folder_len + 7 + 1);

        /* Append a pattern to `folder`, i.e. ";$folder/?.lua" */
        strcpy(dst, path);      dst += path_len;
        *dst = ';';             ++dst;
        strcpy(dst, folder);    dst += folder_len;
        strcpy(dst, "/?.lua");

        lua_pushstring(L, new_path);
        lua_setfield(L, -3, "path");
        free(new_path);
    }

    lua_pop(L, 2);
}

/**
 * Set the name of the global library in Lua space.
 *
 * By default, library functions passed to Lua are stored into a table named
 * with the global application name set by app_init(). This function allows to
 * customize that table name: see app_lua_init() for more information.
 *
 * @param app  An App instance
 * @param name Name of the Lua table
 */
void
app_lua_set_name(App *app, const char *name)
{
    if (app->lua_name != NULL) {
        free(app->lua_name);
    }
    app->lua_name = name == NULL ? NULL : strdup(name);
}

/**
 * Initialize an application instance for Lua scripting.
 *
 * This should be called before any other Lua related call. Any blocking
 * error will generate a relevant message and will quit the current process.
 *
 * The passed in script should return a function without arguments that does
 * not return anything. The following is a perfectly valid script:
 *
 *     return function () end
 *
 * You can optionally pass a set of C functions in the \p library argument.
 * A typical implementation would look like the following:
 *
 *     static const luaL_Reg library[] = {
 *         { "first_function",  first_function },
 *         { "second_function", second_function },
 *         ...
 *         { NULL, NULL }
 *     };
 *     app_lua_init(app, "myscript.lua", library);
 *
 * The library functions are stored in a global table that will named with
 * whatever you specified in app_lua_set_name() or (if you did not call it
 * beforehand) with the global application name set by app_init().
 *
 * The application instance (@p app) is pushed into the Lua global space as
 * `APP`, so you can retrieve the app pointer from the Lua state with e.g.:
 *
 *     App *app;
 *     lua_getglobal(L, "APP");
 *     app = lua_touserdata(L, -1);
 *
 * @param app     An App instance to initialize
 * @param script  Path to a valid Lua script
 * @param library A lua_CFunction function
 */
void
app_lua_init(App *app, const char *script, const void *library)
{
    lua_State *L = luaL_newstate();
    app->lua = L;

    luaL_openlibs(app->lua);

    /* Add PKGDATADIR to the Lua path so `require 'dsp402'` will work */
    ensure_in_lua_path(L, PKGDATADIR);

    if (luaL_loadfile(app->lua, script) != 0) {
        die("Script '%s' failed to load", script);
        return;
    }

    if (library != NULL) {
        lua_newtable(L);
        luaL_setfuncs(L, library, 0);
        lua_setglobal(L, app->lua_name != NULL ? app->lua_name : app->name);
    }

    lua_atpanic(L, panic_callback);
    /* Push app pointer into the global space */
    lua_pushlightuserdata(L, app);
    lua_setglobal(L, "APP");

    lua_pushstring(L, script);
    lua_call(L, 1, 1);
    if (! lua_isfunction(L, 1)) {
        die("The script '%s' does not return a function", script);
        return;
    }
}

/**
 * Finalize the Lua scripting section of an application.
 *
 * This is the counterpart of app_lua_init().
 *
 * @param app The instance to free
 */
void
app_lua_free(App *app)
{
    if (app->lua != NULL) {
        lua_pop(app->lua, 1);
        lua_close(app->lua);
        app->lua = NULL;
    }
}

/**
 * Execute the Lua script initialized by app_lua_init().
 *
 * It is expected that the Lua script will return a function and this API will
 * execute that function. app_lua_run() can be called multiple times.
 *
 * As an example consider the following Lua script:
 *
 *     local n = 1
 *     return function ()
 *         print(n)
 *         n = n + 1
 *     end
 *
 * The first call to app_lua_run() will print 1, the second 2 and so on.
 *
 * @param app The App instance with a Lua script bound to it
 */
void
app_lua_run(App *app)
{
    lua_pushvalue(app->lua, 1);
    lua_call(app->lua, 0, 0);
}
