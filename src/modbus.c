#include "modbus.h"
/* Hack to include libmodbus modbus.h instead of this very same file */
#include <modbus/modbus.h>
#include <errno.h>


typedef struct {
    gchar *     devnet;
    guint       baudport;
    guint       slave_id;
    guint       rate;

    modbus_t *  handle;
    GSourceFunc callback;
    GThread *   thread;
    gchar *     error;
    gboolean    quit;
} MachineryModbusPrivate;


G_DEFINE_TYPE_WITH_PRIVATE(MachineryModbus, machinery_modbus, G_TYPE_OBJECT)

enum {
    PROP_0,
    PROP_DEVNET,
    PROP_BAUDPORT,
    PROP_SLAVE_ID,
    PROP_RATE,
    NUM_PROPERTIES,
};


static GParamSpec * props[NUM_PROPERTIES] = { NULL, };


/* Backward compatibility with modbus 3.0 */
static int
set_response_timeout(modbus_t *ctx, uint32_t sec, uint32_t usec)
{
#if LIBMODBUS_VERSION_CHECK(3, 1, 2)
    return modbus_set_response_timeout(ctx, sec, usec);
#else
    struct timeval timeout;
    timeout.tv_sec  = sec;
    timeout.tv_usec = usec;
    modbus_set_response_timeout(ctx, &timeout);
    return 0;
#endif
}

static void
set_error(MachineryModbusPrivate *data, const gchar *format, ...)
{
    gchar *error;
    va_list args;

    if (g_atomic_pointer_get(&data->error) != NULL) {
        /* Set the error only if there are no old errors */
        return;
    }

    va_start(args, format);
    error = g_strdup_vprintf(format, args);
    va_end(args);
    g_atomic_pointer_set(&data->error, error);
}

static void
handle_error(MachineryModbusPrivate *data)
{
    set_error(data, "Devnet '%s:%d' (%d): %s",
              data->devnet, data->baudport, errno, modbus_strerror(errno));
}

static void
handle_close(MachineryModbusPrivate *data)
{
    if (data->handle == NULL) {
        /* libmodbus handler already destroyed */
        return;
    }

    modbus_close(data->handle);
    modbus_free(data->handle);
    data->handle = NULL;
}

static gboolean
handle_open(MachineryModbusPrivate *data)
{
    guint id;

    if (data->handle != NULL) {
        /* libmodbus handle already created */
        return TRUE;
    }

    if (g_file_test(data->devnet, G_FILE_TEST_EXISTS)) {
        data->handle = modbus_new_rtu(data->devnet, data->baudport, 'N', 8, 1);
    } else {
        data->handle = modbus_new_tcp(data->devnet, data->baudport);
    }

    if (data->handle == NULL) {
        handle_error(data);
        return FALSE;
    }

    id = data->slave_id;
    if (modbus_connect(data->handle) == -1 ||
        set_response_timeout(data->handle, 0, 500000) == -1 ||
        (id > 0 && modbus_set_slave(data->handle, id) == -1)) {
        handle_error(data);
        handle_close(data);
        return FALSE;
    }

    return TRUE;
}

static gpointer
thread_loop(gpointer user_data)
{
    MachineryModbusPrivate *data = (MachineryModbusPrivate *) user_data;
    gint64 now, clock = g_get_monotonic_time();
    GSourceFunc callback = g_atomic_pointer_get(&data->callback);
    guint rate = g_atomic_int_get(&data->rate);

    while (! g_atomic_int_get(&data->quit)) {
        if (callback(data->handle) == G_SOURCE_REMOVE) {
            break;
        }
        clock += rate;
        now    = g_get_monotonic_time();
        if (now > clock) {
            g_info("Modbus thread too slow");
            clock = now;
        } else {
            g_usleep(clock - now);
        }
    }

    return NULL;
}

static gboolean
thread_stop(MachineryModbusPrivate *data)
{
    if (data->thread == NULL) {
        /* Only stop the thread if there is a thread */
        return FALSE;
    }

    g_atomic_int_set(&data->quit, TRUE);
    g_thread_join(data->thread);
    data->thread = NULL;
    data->quit   = FALSE;
    return TRUE;
}

static gboolean
thread_start(MachineryModbusPrivate *data)
{
    if (data->thread != NULL) {
        /* Start the thread only if not already started */
        return TRUE;
    }

    if (data->callback == NULL) {
        set_error(data, "Modbus: thread callback not set");
        return FALSE;
    }

    if (! handle_open(data)) {
        return FALSE;
    }

    data->thread = g_thread_new("modbus", thread_loop, data);
    return data->thread != NULL;
}

static void
set_devnet(MachineryModbus *modbus, const gchar *devnet)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    if (g_strcmp0(data->devnet, devnet) != 0) {
        thread_stop(data);
        handle_close(data);
        g_free(data->devnet);
        data->devnet = g_strdup(devnet);
    }
}

static const gchar *
get_devnet(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    return data->devnet;
}

static void
set_baudport(MachineryModbus *modbus, guint baudport)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    if (data->baudport != baudport) {
        thread_stop(data);
        handle_close(data);
        data->baudport = baudport;
    }
}

static guint
get_baudport(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    return data->baudport;
}

static void
set_slave_id(MachineryModbus *modbus, guint slave_id)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    if (data->slave_id != slave_id) {
        thread_stop(data);
        data->slave_id = slave_id;
        /* Try to set the slave id on the fly */
        if (slave_id > 0 && data->handle != NULL &&
            modbus_set_slave(data->handle, slave_id) == -1) {
            handle_error(data);
        }
    }
}

static guint
get_slave_id(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    return data->slave_id;
}

static void
set_rate(MachineryModbus *modbus, guint rate)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    if (data->rate != rate) {
        thread_stop(data);
        data->rate = rate;
    }
}

static guint
get_rate(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    return data->rate;
}

static void
get_property(GObject *object, guint prop_id,
             GValue *value, GParamSpec *pspec)
{
    MachineryModbus *modbus = (MachineryModbus *) object;

    switch (prop_id) {

    case PROP_DEVNET:
        g_value_set_string(value, get_devnet(modbus));
        break;

    case PROP_BAUDPORT:
        g_value_set_uint(value, get_baudport(modbus));
        break;

    case PROP_SLAVE_ID:
        g_value_set_uint(value, get_slave_id(modbus));
        break;

    case PROP_RATE:
        g_value_set_uint(value, get_rate(modbus));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
set_property(GObject *object, guint prop_id,
             const GValue *value, GParamSpec *pspec)
{
    MachineryModbus *modbus = (MachineryModbus *) object;

    switch (prop_id) {

    case PROP_DEVNET:
        set_devnet(modbus, g_value_get_string(value));
        break;

    case PROP_BAUDPORT:
        set_baudport(modbus, g_value_get_uint(value));
        break;

    case PROP_SLAVE_ID:
        set_slave_id(modbus, g_value_get_uint(value));
        break;

    case PROP_RATE:
        set_rate(modbus, g_value_get_uint(value));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
finalize(GObject *object)
{
    MachineryModbus *modbus = (MachineryModbus *) object;
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    thread_stop(data);
    handle_close(data);
    g_free(data->devnet);
}

static void
machinery_modbus_class_init(MachineryModbusClass *modbus_class)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) modbus_class;
    gobject_class->get_property = get_property;
    gobject_class->set_property = set_property;
    gobject_class->finalize = finalize;

    props[PROP_DEVNET] =
        g_param_spec_string("devnet", P_("Device/Network address"),
                          P_("Serial device or network address where the Modbus server is listening"),
                          "localhost", G_PARAM_READWRITE);
    props[PROP_BAUDPORT] =
        g_param_spec_uint("baudport", P_("Baud Rate/TCP Port"),
                          P_("Serial device speed or TCP port of the Modbus server"),
                          0, G_MAXUINT, 502, G_PARAM_READWRITE);
    props[PROP_SLAVE_ID] =
        g_param_spec_uint("slave-id", P_("Slave ID"),
                          P_("Modbus ID of the slave device you want to be connected to"),
                          0, G_MAXUINT, 0, G_PARAM_READWRITE);
    props[PROP_RATE] =
        g_param_spec_uint("rate", P_("Rate"),
                          P_("Delay (in microseconds) between each call to your callback"),
                          0, G_MAXUINT, 100000, G_PARAM_READWRITE);

    g_object_class_install_properties(gobject_class, NUM_PROPERTIES, props);
}

static void
machinery_modbus_init(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data = machinery_modbus_get_instance_private(modbus);
    set_devnet(modbus, "localhost");
    set_baudport(modbus, 502);
    data->slave_id = 0;
    data->rate     = 100000;
    data->handle   = NULL;
    data->callback = NULL;
    data->error    = NULL;
    data->quit     = FALSE;
}


/**
 * machinery_modbus_new:
 *
 * Creates a new MachineryModbus instance. The newly created object has
 * exactly one reference and must be freed with g_object_unref() when
 * no longer needed.
 *
 * Returns: a newly created #MachineryModbus instance.
 *
 * Since: 1.0
 **/
MachineryModbus *
machinery_modbus_new(void)
{
    return g_object_new(MACHINERY_TYPE_MODBUS, NULL);
}

/**
 * machinery_modbus_new_full:
 * @devnet: serial device or network address
 * @baudport: baud rate of the serial device or TCP port
 *
 * Creates a new MachineryModbus instance with specific devnet and
 * baudport properties.
 *
 * |[
 * // Example code for a Modbus/TCP connection
 * modbus = machinery_modbus_new_full("127.0.0.1", 502);
 *
 * // Example code for a Modbus/RTU connection
 * modbus = machinery_modbus_new_full("/dev/ttyS0", 19200);
 * ]|
 *
 * Returns: a newly created #MachineryModbus instance.
 *
 * Since: 1.0
 **/
MachineryModbus *
machinery_modbus_new_full(const gchar *devnet, guint baudport)
{
    return g_object_new(MACHINERY_TYPE_MODBUS,
                        "devnet", devnet, "baudport", baudport,
                        NULL);
}

/**
 * machinery_modbus_get_handle:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the internal modbus handle, a `modbus_t *` pointer in the usual
 * case libmodbus is used. The returned value is owned by `modbus` and
 * should not be modified or freed.
 *
 * This function is provided for debugging or other special purposes,
 * such as using MachineryModbus without leveraging the internal thread.
 *
 * Returns: the internal modbus handle.
 *
 * Since: 1.0
 **/
gpointer
machinery_modbus_get_handle(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), NULL);

    data = machinery_modbus_get_instance_private(modbus);

    /* Ensure the handle has been properly initialized */
    if (! handle_open(data)) {
        return NULL;
    }

    return data->handle;
}

/**
 * machinery_modbus_set_devnet:
 * @modbus: a #MachineryModbus instance
 * @devnet: the new serial device or network address
 *
 * Sets a new serial device (for Modbus/RTU connections) or network
 * address (for Modbus/TCP connections).
 *
 * WARNING: if devnet has changed, this function will possibly stop the
 * working thread and close any old Modbus connection.
 *
 * |[
 * // Example code for a Modbus/TCP connection
 * modbus = machinery_modbus_new();
 * machinery_modbus_set_devnet(modbus, "127.0.0.1");
 * machinery_modbus_set_baudport(modbus, 502);
 *
 * // Example code for a Modbus/RTU connection
 * modbus = machinery_modbus_new();
 * machinery_modbus_set_devnet(modbus, "/dev/ttyS0");
 * machinery_modbus_set_baudport(modbus, 19200);
 * ]|
 *
 * Since: 1.0
 **/
void
machinery_modbus_set_devnet(MachineryModbus *modbus, const gchar *devnet)
{
    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));
    set_devnet(modbus, devnet);
    g_object_notify_by_pspec((GObject *) modbus, props[PROP_DEVNET]);
}

/**
 * machinery_modbus_get_devnet:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the serial device or network address of @modbus. The returned
 * string is owned by @modbus and must not be modified or freed.
 *
 * Returns: the current serial device or network address.
 *
 * Since: 1.0
 **/
const gchar *
machinery_modbus_get_devnet(MachineryModbus *modbus)
{
    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), NULL);
    return get_devnet(modbus);
}

/**
 * machinery_modbus_set_baudport:
 * @modbus: a #MachineryModbus instance
 * @baudport: the new baud rate or TCP port
 *
 * Sets a new baud rate (for Modbus/RTU connections) or TCP port (for
 * Modbus/TCP connections).
 *
 * WARNING: if baudport has changed, this function will possibly stop
 * the working thread and close any old Modbus connection.
 *
 * Since: 1.0
 **/
void
machinery_modbus_set_baudport(MachineryModbus *modbus, guint baudport)
{
    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));
    set_baudport(modbus, baudport);
    g_object_notify_by_pspec((GObject *) modbus, props[PROP_BAUDPORT]);
}

/**
 * machinery_modbus_get_baudport:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the baud rate (for serial connections) or the TCP port (for
 * network connections) of @modbus.
 *
 * Returns: the baud rate or TCP port.
 *
 * Since: 1.0
 **/
guint
machinery_modbus_get_baudport(MachineryModbus *modbus)
{
    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), 0);
    return get_baudport(modbus);
}

/**
 * machinery_modbus_set_slave_id:
 * @modbus: a #MachineryModbus instance
 * @slave_id: the new slave id
 *
 * Sets a new slave id on this Modbus instance. Passing 0 as slave_id
 * disable any slave ID setting: it will be your full responsibility to
 * set the proper ID inside the thread callback.
 *
 * This call can generate an error if the ID is invalid: use
 * machinery_modbus_get_error() to check for this condition.
 *
 * WARNING: if the slave id has changed, this function will possibly
 * stop the working thread. Use modbus_set_slave() directly inside your
 * callback instead if you need to change the slave from the thread.
 *
 * Since: 1.0
 **/
void
machinery_modbus_set_slave_id(MachineryModbus *modbus, guint slave_id)
{
    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));
    set_slave_id(modbus, slave_id);
    g_object_notify_by_pspec((GObject *) modbus, props[PROP_SLAVE_ID]);
}

/**
 * machinery_modbus_get_slave_id:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the slave id on this Modbus instance.
 *
 * Returns: the slave ID.
 *
 * Since: 1.0
 **/
guint
machinery_modbus_get_slave_id(MachineryModbus *modbus)
{
    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), 0);
    return get_slave_id(modbus);
}

/**
 * machinery_modbus_set_rate:
 * @modbus: a #MachineryModbus instance
 * @rate: the new rate, in microseconds
 *
 * Sets the rate at which your callback will be triggered from the
 * working thread.
 *
 * WARNING: if the rate has changed, this function will possibly stop
 * the working thread.
 *
 * Since: 1.0
 **/
void
machinery_modbus_set_rate(MachineryModbus *modbus, guint rate)
{
    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));
    set_rate(modbus, rate);
    g_object_notify_by_pspec((GObject *) modbus, props[PROP_RATE]);
}

/**
 * machinery_modbus_get_rate:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the rate (in microseconds) at which your callback will be
 * triggered from the working thread.
 *
 * Returns: the rate, in microseconds.
 *
 * Since: 1.0
 **/
guint
machinery_modbus_get_rate(MachineryModbus *modbus)
{
    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), 0);
    return get_rate(modbus);
}

/**
 * machinery_modbus_set_callback:
 * @modbus: a #MachineryModbus instance
 * @callback: the new callback
 *
 * Sets the callback to call repeatedly from the working thread: see
 * machinery_modbus_start() for a full example. The callback must return
 * G_SOURCE_CONTINUE to keep going or G_SOURCE_REMOVE to abort the
 * internal loop and consequentially stop the working thread.
 *
 * BEWARE: the callback is called from a different thread! If you need
 * to pass data to the main thread it is your responsibility to
 * synchronize in some way the data access (using e.g. locks or signals).
 *
 * WARNING: if the callback has changed, this function will possibly
 * stop the working thread.
 *
 * Since: 1.0
 **/
void
machinery_modbus_set_callback(MachineryModbus *modbus, GSourceFunc callback)
{
    MachineryModbusPrivate *data;

    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));

    data = machinery_modbus_get_instance_private(modbus);
    if (data->callback != callback) {
        thread_stop(data);
        data->callback = callback;
    }
}

/**
 * machinery_modbus_start:
 * @modbus: a #MachineryModbus instance
 *
 * Starts the working thread. The Modbus connection will be initialized
 * accordingly before. If the thread is already running, this function
 * does nothing.
 *
 * The working thread will call the callback set with
 * machinery_modbus_set_callback() at the rate specified with
 * machinery_modbus_set_rate(). The pointer passed to the callback is
 * a modbus_t context: see libmodbus documentation to know what you can
 * do with it.
 *
 * |[
 * static gboolean io_callback(gpointer data)
 * {
 *     // WARNING! Here we are inside the working thread: we cannot
 *     // blindly change global variables without locks
 *     modbus_t *handle = (modbus_t *) data;
 *     uint8_t di[4];
 *     int n;
 *
 *     g_debug("This is the working thread");
 *
 *     if (modbus_read_input_bits(handle, 0, 4, di) == -1) {
 *         // Connection error: stop the thread
 *         g_warning("Error while reading inputs: %s", modbus_strerror(errno));
 *         return G_SOURCE_REMOVE;
 *     }
 *     g_print(" %d %d %d %d\r", di[0], di[1], di[2], di[3]);
 *     return G_SOURCE_CONTINUE;
 * }
 *
 * int main()
 * {
 *     MachineryModbus *io = machinery_modbus_new_full("127.0.0.1", 502);
 *     machinery_modbus_set_callback(io, io_callback);
 *     machinery_modbus_set_rate(io, 100000); // Call io_thread every 0.1 s
 *
 *     // Fork happens here
 *     machinery_modbus_start(io);
 *
 *     g_debug("This is the main thread");
 *     // Just sleep a couple of seconds
 *     g_usleep(2000000);
 *
 *     machinery_modbus_stop(io);
 *     g_object_unref(io);
 *
 *     return 0;
 * }
 *
 * Returns: TRUE on success, FALSE on errors.
 *
 * Since: 1.0
 **/
gboolean
machinery_modbus_start(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), FALSE);

    data = machinery_modbus_get_instance_private(modbus);
    return thread_start(data);
}

/**
 * machinery_modbus_stop:
 * @modbus: a #MachineryModbus instance
 *
 * Stops the working thread. This function will block until the thread
 * has been effectively destroyed. If the thread has been already
 * stopped, this function does nothing.
 *
 * Since: 1.0
 **/
void
machinery_modbus_stop(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data;

    g_return_if_fail(MACHINERY_IS_MODBUS(modbus));

    data = machinery_modbus_get_instance_private(modbus);
    thread_stop(data);
}

/**
 * machinery_modbus_is_running:
 * @modbus: a #MachineryModbus instance
 *
 * Checks if the working thread of @modbus is running.
 *
 * Returns: TRUE if the working thread is running, FALSE otherwise.
 *
 * Since: 1.0
 **/
gboolean
machinery_modbus_is_running(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), FALSE);

    data = machinery_modbus_get_instance_private(modbus);
    return data->thread != NULL;
}

/**
 * machinery_modbus_get_error:
 * @modbus: a #MachineryModbus instance
 *
 * Gets the last error message or NULL on no errors. This function also
 * clears the internal error string, i.e. subsequent calls will return
 * NULL.
 *
 * The returned string must be freed with g_free() when no longer needed.
 *
 * Returns: the active error message or NULL on no errors.
 *
 * Since: 1.0
 **/
gchar *
machinery_modbus_get_error(MachineryModbus *modbus)
{
    MachineryModbusPrivate *data;
    gchar *error;

    g_return_val_if_fail(MACHINERY_IS_MODBUS(modbus), NULL);

    data = machinery_modbus_get_instance_private(modbus);
    error = g_atomic_pointer_get(&data->error);
    g_atomic_pointer_set(&data->error, NULL);

    return error;
}
