#include "grbl.h"
#include <libserialport.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

#define GRBL_DEVICE(data) \
    ((data)->port != NULL ? sp_get_port_name((data)->port) : "")
#define DEFINE_STATUS(s,e) \
    g_hash_table_insert(statuses, (s), GINT_TO_POINTER(e))
#define PATTERN \
    "^<(\\w+).*\\|WPos:([-+0-9.]+),([-+0-9.]+),([-+0-9.]+)"


typedef struct {
    gchar *         device;
    guint           baudrate;
    guint           timeout;
    struct sp_port *port;
    volatile gchar *error;
    volatile gint   alarm;
    volatile MachineryGrblStatus
                    status;
    GMutex          status_lock;
    GMutex          lock;
    GCond           cond;
    gchar *         command;
    GThread *       thread;
    gdouble         x;
    gdouble         y;
    gdouble         z;
} MachineryGrblPrivate;

typedef enum {
    GRBL_LINE_NONE,
    GRBL_LINE_OK,
    GRBL_LINE_ERROR,
    GRBL_LINE_ALARM,
    GRBL_LINE_STATUS,
    GRBL_LINE_UNKNOWN,
} GrblLine;


G_DEFINE_TYPE_WITH_PRIVATE(MachineryGrbl, machinery_grbl, G_TYPE_OBJECT)

enum {
    PROP_0,
    PROP_DEVICE,
    PROP_BAUDRATE,
    NUM_PROPERTIES,
};


static GParamSpec * props[NUM_PROPERTIES] = { NULL, };
static GRegex *     regex = NULL;
static GHashTable * statuses = NULL;


static void
set_error(MachineryGrblPrivate *data, const gchar *format, ...)
{
    /* Set the error only if there are no old errors */
    if (g_atomic_pointer_get(&data->error) == NULL) {
        gchar *error;
        va_list args;
        va_start(args, format);
        error = g_strdup_vprintf(format, args);
        va_end(args);
        g_atomic_pointer_set(&data->error, error);
    }
}

static gboolean
parse_status_line(MachineryGrblPrivate *data, const gchar *response)
{
    GMatchInfo *info;
    gchar *text;
    gpointer hash;
    gdouble x, y, z;
    MachineryGrblStatus status;

    /* Initialize statuses hash table, if not already done */
    if (statuses == NULL) {
        statuses = g_hash_table_new(g_str_hash, g_str_equal);
        DEFINE_STATUS("Idle",  MACHINERY_GRBL_STATUS_IDLE);
        DEFINE_STATUS("Run",   MACHINERY_GRBL_STATUS_RUN);
        DEFINE_STATUS("Hold",  MACHINERY_GRBL_STATUS_HOLD);
        DEFINE_STATUS("Jog",   MACHINERY_GRBL_STATUS_JOG);
        DEFINE_STATUS("Alarm", MACHINERY_GRBL_STATUS_ALARM);
        DEFINE_STATUS("Door",  MACHINERY_GRBL_STATUS_DOOR);
        DEFINE_STATUS("Check", MACHINERY_GRBL_STATUS_CHECK);
        DEFINE_STATUS("Home",  MACHINERY_GRBL_STATUS_HOME);
        DEFINE_STATUS("Sleep", MACHINERY_GRBL_STATUS_SLEEP);
    }

    /* Initialize regular expression, if not already done */
    if (regex == NULL) {
        regex = g_regex_new(PATTERN, G_REGEX_OPTIMIZE, 0, NULL);
    }

    if (! g_regex_match(regex, response, 0, &info)) {
        g_match_info_free(info);
        return FALSE;
    }

    text = g_match_info_fetch(info, 1);
    hash = g_hash_table_lookup(statuses, text);
    if (hash == NULL) {
        set_error(data, "GRBL: unrecognized status (%s)", text);
        status = MACHINERY_GRBL_STATUS_UNKNOWN;
    } else {
        status = GPOINTER_TO_INT(hash);
    }
    g_free(text);

    text = g_match_info_fetch(info, 2);
    x = g_ascii_strtod(text, NULL);
    g_free(text);

    text = g_match_info_fetch(info, 3);
    y = g_ascii_strtod(text, NULL);
    g_free(text);

    text = g_match_info_fetch(info, 4);
    z = g_ascii_strtod(text, NULL);
    g_free(text);

    g_match_info_free(info);

    g_mutex_lock(&data->status_lock);
    data->status = status;
    data->x = x;
    data->y = y;
    data->z = z;
    g_mutex_unlock(&data->status_lock);

    return TRUE;
}

static GrblLine
parse_line(MachineryGrblPrivate *data, const gchar *response)
{
    if (g_str_has_prefix(response, "ok")) {
        return GRBL_LINE_OK;
    } else if (g_str_has_prefix(response, "error:")) {
        set_error(data, "GRBL: error '%s' returned", response);
        return GRBL_LINE_ERROR;
    } else if (g_str_has_prefix(response, "ALARM:")) {
        g_atomic_int_set(&data->alarm, atoi(response + 6));
        return GRBL_LINE_ALARM;
    } else if (parse_status_line(data, response)) {
        return GRBL_LINE_STATUS;
    }
    return GRBL_LINE_UNKNOWN;
}

static void
serial_error(MachineryGrblPrivate *data)
{
    char *message = sp_last_error_message();
    set_error(data, "Device '%s' (%d): %s",
              GRBL_DEVICE(data), sp_last_error_code(), message);
    sp_free_error_message(message);
}

static void
serial_close(MachineryGrblPrivate *data)
{
    sp_close(data->port);
    sp_free_port(data->port);
    data->port = NULL;
}

static gboolean
serial_open(MachineryGrblPrivate *data)
{
    if (data->port != NULL) {
        /* Port already open */
        return TRUE;
    }

    if (sp_get_port_by_name(data->device, &data->port) != SP_OK ||
        sp_open(data->port, SP_MODE_READ_WRITE) != SP_OK ||
        sp_set_baudrate(data->port, data->baudrate) != SP_OK ||
        sp_set_bits(data->port, 8) != SP_OK ||
        sp_set_stopbits(data->port, 1) != SP_OK ||
        sp_set_parity(data->port, SP_PARITY_NONE) != SP_OK ||
        sp_set_flowcontrol(data->port, SP_FLOWCONTROL_RTSCTS) != SP_OK)
    {
        serial_error(data);
        serial_close(data);
        return FALSE;
    }

    return TRUE;
}

static gchar *
serial_receive(MachineryGrblPrivate *data, guint timeout)
{
    GString *string;
    gchar    byte;
    int      status;

    /* Open the serial communication, if not already done */
    if (! serial_open(data)) {
        return NULL;
    }

    string = g_string_sized_new(10);
    for (;;) {
        if (timeout > 0) {
            status = sp_blocking_read(data->port, &byte, 1, timeout);
        } else {
            status = sp_nonblocking_read(data->port, &byte, 1);
        }
        if (status < 0) {
            /* Communication error: return NULL */
            serial_error(data);
            g_string_free(string, TRUE);
            return NULL;
        } else if (status == 0) {
            /* Timeout: return NULL */
            g_string_free(string, TRUE);
            return NULL;
        }
        /* The time up to the response start must be within `timeout`.
         * Subsequent bytes must be sent within 0,1 seconds. */
        timeout = 100;
        if (byte == '\n') {
            break;
        } else if (byte != '\r') {
            string = g_string_append_c(string, byte);
        }
    }

    gchar *escaped = g_strescape(string->str, NULL);
    g_debug("Receive: '%s'", escaped);
    g_free(escaped);

    return g_string_free(string, FALSE);
}

static gboolean
serial_send(MachineryGrblPrivate *data, const gchar *text)
{
    int    status;
    size_t count;

    if (text == NULL) {
        return TRUE;
    }

    /* Open the serial communication, if not already done */
    if (! serial_open(data)) {
        return FALSE;
    }

    count  = strlen(text);
    status = sp_blocking_write(data->port, text, count, 200);

    if (status < 0) {
        serial_error(data);
        return FALSE;
    } else if (status != count) {
        set_error(data, "Device '%s': %s",
                  GRBL_DEVICE(data), "Timeout in send");
        return FALSE;
    }

    gchar *escaped = g_strescape(text, NULL);
    g_debug("   Send: '%s'", escaped);
    g_free(escaped);

    return TRUE;
}

static gboolean
serial_submit(MachineryGrblPrivate *data, const gchar *request,
              guint timeout, GrblLine right, GrblLine wrong)
{
    GrblLine line;
    gchar *response;
    gint64 start;
    guint elapsed;

    if (! serial_send(data, request)) {
        return FALSE;
    }

    start = g_get_monotonic_time();
    elapsed = 0;
    for (;;) {
        response = serial_receive(data, elapsed >= timeout ? 0 : timeout - elapsed);
        if (response == NULL) {
            /* Timeout condition */
            return FALSE;
        }

        line = parse_line(data, response);
        g_free(response);

        if (line == right) {
            return TRUE;
        } else if (line == wrong) {
            return FALSE;
        }

        elapsed = (g_get_monotonic_time() - start) / 1000;
    }

    return FALSE;
}

static void
set_device(MachineryGrbl *grbl, const gchar *device)
{
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    if (g_strcmp0(data->device, device) != 0) {
        serial_close(data);
        data->device = g_strdup(device);
    }
}

static const gchar *
get_device(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    return data->device;
}

static void
set_baudrate(MachineryGrbl *grbl, guint baudrate)
{
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    if (data->baudrate != baudrate) {
        serial_close(data);
        data->baudrate = baudrate;
    }
}

static const guint32
get_baudrate(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    return data->baudrate;
}

static void
get_property(GObject *object, guint prop_id,
             GValue *value, GParamSpec *pspec)
{
    MachineryGrbl *grbl = (MachineryGrbl *) object;

    switch (prop_id) {

    case PROP_DEVICE:
        g_value_set_string(value, get_device(grbl));
        break;

    case PROP_BAUDRATE:
        g_value_set_uint(value, get_baudrate(grbl));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
set_property(GObject *object, guint prop_id,
             const GValue *value, GParamSpec *pspec)
{
    MachineryGrbl *grbl = (MachineryGrbl *) object;

    switch (prop_id) {

    case PROP_BAUDRATE:
        set_baudrate(grbl, g_value_get_uint(value));
        break;

    case PROP_DEVICE:
        set_device(grbl, g_value_get_string(value));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
finalize(GObject *object)
{
    MachineryGrbl *grbl = (MachineryGrbl *) object;
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    g_mutex_clear(&data->status_lock);
    g_mutex_clear(&data->lock);
    g_cond_clear(&data->cond);
    machinery_grbl_stop(grbl);
    if (data->device != NULL) {
        g_free(data->device);
        data->device = NULL;
    }
}

static void
machinery_grbl_class_init(MachineryGrblClass *grbl_class)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) grbl_class;
    gobject_class->get_property = get_property;
    gobject_class->set_property = set_property;
    gobject_class->finalize = finalize;

    props[PROP_DEVICE] =
        g_param_spec_string("device", P_("Device"),
                          P_("Serial device where GRBL is connected"),
                          "/dev/ttyS0", G_PARAM_READWRITE);
    props[PROP_BAUDRATE] =
        g_param_spec_uint("baudrate", P_("Baud Rate"),
                          P_("Speed of the connection to the GRBL device"),
                          0, G_MAXUINT, 115200, G_PARAM_READWRITE);

    g_object_class_install_properties(gobject_class, NUM_PROPERTIES, props);
}

static void
machinery_grbl_init(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    set_device(grbl, "/dev/ttyS0");
    set_baudrate(grbl, 115200);
    data->timeout = 1000;
    data->port    = NULL;
    data->error   = NULL;
    data->alarm   = 0;
    data->status  = MACHINERY_GRBL_STATUS_UNKNOWN;
    data->command = NULL;
    data->thread  = NULL;
    data->x       = 0;
    data->y       = 0;
    data->z       = 0;

    g_mutex_init(&data->status_lock);
    g_mutex_init(&data->lock);
    g_cond_init(&data->cond);
}

static gpointer
thread_loop(gpointer user_data)
{
    MachineryGrbl *grbl = (MachineryGrbl *) user_data;
    MachineryGrblPrivate *data = machinery_grbl_get_instance_private(grbl);
    gchar *command;

    g_mutex_lock(&data->lock);
    for (;;) {
        /* Wait until a command has been requested */
        g_cond_wait(&data->cond, &data->lock);
        command = data->command;

        /* Clear any pending lines */
        serial_submit(data, NULL, 0, GRBL_LINE_NONE, GRBL_LINE_NONE);

        if (command == NULL) {
            /* Special case: kill thread */
            serial_close(data);
            g_mutex_unlock(&data->lock);
            return NULL;
        } else if (command[0] == '?') {
            /* Special case: update status */
            serial_submit(data, command, data->timeout,
                          GRBL_LINE_STATUS, GRBL_LINE_NONE);
        } else {
            /* Wait for an ok/error response */
            serial_submit(data, command, data->timeout,
                          GRBL_LINE_OK, GRBL_LINE_ERROR);
        }

        g_free(command);
        data->command = NULL;
    }
}

static gboolean
thread_submit(MachineryGrblPrivate *data, const gchar *command, guint timeout)
{
    /* Return immediately if not able to lock the mutex */
    if (! g_mutex_trylock(&data->lock)) {
        return FALSE;
    }

    data->command = g_strdup(command);
    data->timeout = timeout;
    g_cond_signal(&data->cond);
    g_mutex_unlock(&data->lock);
    return TRUE;
}

static void
thread_stop(MachineryGrblPrivate *data)
{
    if (data->thread != NULL) {
        /* Ensure the NULL command has been submitted */
        while (! thread_submit(data, NULL, 1000)) {
            g_thread_yield();
        }
        g_thread_join(data->thread);
        data->thread = NULL;
    }
}


/**
 * machinery_grbl_new:
 *
 * Creates a new MachineryGrbl instance. The newly created object has
 * exactly one reference and must be freed with g_object_unref() when
 * no longer needed.
 *
 * Returns: a newly created #MachineryGrbl instance.
 *
 * Since: 1.0
 **/
MachineryGrbl *
machinery_grbl_new(void)
{
    return g_object_new(MACHINERY_TYPE_GRBL, NULL);
}

/**
 * machinery_grbl_new_full:
 * @device: serial device
 * @baudrate: baud rate of the serial device
 *
 * Creates a new MachineryGrbl instance with specific @device and
 * @baudrate.
 *
 * Returns: a newly created #MachineryGrbl instance.
 *
 * Since: 1.0
 **/
MachineryGrbl *
machinery_grbl_new_full(const gchar *device, guint baudrate)
{
    return g_object_new(MACHINERY_TYPE_GRBL,
                        "device", device, "baudrate", baudrate,
                        NULL);
}

/**
 * machinery_grbl_set_device:
 * @grbl: a #MachineryGrbl instance
 * @device: the new device
 *
 * Sets a new device on this GRBL instance. If the new device is
 * different from the old one, this will consecuentially closes any
 * previously open serial communication.
 *
 * Since: 1.0
 **/
void
machinery_grbl_set_device(MachineryGrbl *grbl, const gchar *device)
{
    g_return_if_fail(MACHINERY_IS_GRBL(grbl));
    set_device(grbl, device);
    g_object_notify_by_pspec((GObject *) grbl, props[PROP_DEVICE]);
}

/**
 * machinery_grbl_get_device:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the device of @grbl. The returned string is owned by @grbl and
 * must not be modified or freed.
 *
 * Returns: the current device.
 *
 * Since: 1.0
 **/
const gchar *
machinery_grbl_get_device(MachineryGrbl *grbl)
{
    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), NULL);
    return get_device(grbl);
}

/**
 * machinery_grbl_set_baudrate:
 * @grbl: a #MachineryGrbl instance
 * @baudrate: the new baudrate
 *
 * Sets a new baud rate on this GRBL instance. If the baud rate is
 * different from the old one, this will consecuentially closes any
 * previously open serial communication.
 *
 * Since: 1.0
 **/
void
machinery_grbl_set_baudrate(MachineryGrbl *grbl, guint baudrate)
{
    g_return_if_fail(MACHINERY_IS_GRBL(grbl));
    set_baudrate(grbl, baudrate);
    g_object_notify_by_pspec((GObject *) grbl, props[PROP_BAUDRATE]);
}

/**
 * machinery_grbl_get_baudrate:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the baud rate of @grbl.
 *
 * Returns: the current baud rate.
 *
 * Since: 1.0
 **/
guint
machinery_grbl_get_baudrate(MachineryGrbl *grbl)
{
    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), 0);
    return get_baudrate(grbl);
}

/**
 * machinery_grbl_start:
 * @grbl: a #MachineryGrbl instance
 *
 * Starts the thread handling the GRBL device. If the thread is already
 * started, it does nothing.
 *
 * Return: TRUE on success, FALSE otherwise.
 *
 * Since: 1.0
 **/
gboolean
machinery_grbl_start(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), FALSE);

    /* Start the thread only if not already started */
    data = machinery_grbl_get_instance_private(grbl);
    if (data->thread != NULL) {
        return TRUE;
    }

    if (serial_open(data)) {
        data->thread = g_thread_new("grbl", thread_loop, grbl);
    }

    return data->thread != NULL;
}

/**
 * machinery_grbl_stop:
 * @grbl: a #MachineryGrbl instance
 *
 * Stops the thread handling the GRBL device. If the thread has been
 * already stopped, it does nothing.
 *
 * This function will block until the thread has been fully stopped.
 *
 * Since: 1.0
 **/
void
machinery_grbl_stop(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;

    g_return_if_fail(MACHINERY_IS_GRBL(grbl));

    data = machinery_grbl_get_instance_private(grbl);
    thread_stop(data);
}

/**
 * machinery_grbl_command:
 * @grbl: a #MachineryGrbl instance
 * @timeout: timeout limit, in milliseconds
 * @format: a printf format like string
 * ...: additional arguments
 *
 * Starts a new command. This should be called repeatedly until the
 * command has been sent, i.e. when TRUE is returned. This is by design
 * and it is intended to be integrated into your event loop.
 *
 * The string formatting is locale-independent, that is the locale is
 * switched to C before the serialization (so that %f always uses points
 * as decimal separators).
 *
 * If you want to block until the command has been sent, use the
 * following construct:
 * |[[
 * // Block until the command has been sent
 * while (! machinery_grbl_command(grbl, 1000, "G0X2")) {
 *     g_thread_yield();
 *  }
 * ]]|
 *
 * Since: 1.0
 **/
gboolean
machinery_grbl_command(MachineryGrbl *grbl, guint timeout,
                       const gchar *format, ...)
{
    MachineryGrblPrivate *data;
    gchar *command, *old_locale;
    gboolean done;
    va_list args;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), FALSE);


    /* Set the new locale to C */
    old_locale = g_strdup(setlocale(LC_ALL, NULL));
    setlocale(LC_ALL, "C");

    va_start(args, format);
    command = g_strdup_vprintf(format, args);
    va_end(args);

    /* Restore the old locale */
    if (old_locale != NULL) {
        setlocale(LC_ALL, old_locale);
        g_free(old_locale);
    }

    data = machinery_grbl_get_instance_private(grbl);
    done = thread_submit(data, command, timeout);
    g_free(command);

    return done;
}

/**
 * machinery_grbl_ready:
 * @grbl: a #MachineryGrbl instance
 *
 * Checks if the GRBL thread is ready to accept new commands.
 *
 * Returns: TRUE if the GRBL thread is idle, FALSE otherwise.
 *
 * Since: 1.0
 **/
gboolean
machinery_grbl_ready(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), FALSE);

    data = machinery_grbl_get_instance_private(grbl);
    if (! g_mutex_trylock(&data->lock)) {
        return FALSE;
    }

    g_mutex_unlock(&data->lock);
    return TRUE;
}

/**
 * machinery_grbl_get_error:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the last error message on serial communications or NULL on no
 * errors. This function also clears the internal error string, i.e.
 * subsequent calls will return NULL.
 *
 * The returned string must be freed with g_free() when no longer needed.
 *
 * Returns: the active error message or NULL on no errors.
 *
 * Since: 1.0
 **/
gchar *
machinery_grbl_get_error(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    gchar *error;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), NULL);

    data = machinery_grbl_get_instance_private(grbl);
    error = (gchar *) g_atomic_pointer_get(&data->error);
    g_atomic_pointer_set(&data->error, NULL);

    return error;
}

/**
 * machinery_grbl_alarm:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the active GRBL alarm on axis, as officially documented here:
 * https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#alarm-message
 *
 * To reset the alarm, the GRBL firmware usually requires the unlock
 * command, i.e. sending `"$X\n"`.
 *
 * This function also clears the internal alarm code, i.e. subsequent
 * calls will return 0.
 *
 * Returns: the last GRBL alarm.
 *
 * Since: 1.0
 **/
gint
machinery_grbl_alarm(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    gint alarm;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), 0);

    data  = machinery_grbl_get_instance_private(grbl);
    alarm = g_atomic_int_get(&data->alarm);
    g_atomic_int_set(&data->alarm, 0);
    return alarm;
}

/**
 * machinery_grbl_status:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the last status of the axis.
 *
 * Returns: the axis status or MACHINERY_GRBL_STATUS_UNKNOWN on errors.
 *
 * Since: 1.0
 **/
MachineryGrblStatus
machinery_grbl_status(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    MachineryGrblStatus status;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), MACHINERY_GRBL_STATUS_UNKNOWN);

    data = machinery_grbl_get_instance_private(grbl);

    g_mutex_lock(&data->status_lock);
    status = data->status;
    g_mutex_unlock(&data->status_lock);

    return status;
}

/**
 * machinery_grbl_x:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the X coordinate acquired in the last update-status action.
 *
 * Returns: the X coordinate.
 *
 * Since: 1.0
 **/
gdouble
machinery_grbl_x(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    gdouble x;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), 0);

    data = machinery_grbl_get_instance_private(grbl);

    g_mutex_lock(&data->status_lock);
    x = data->x;
    g_mutex_unlock(&data->status_lock);

    return x;
}

/**
 * machinery_grbl_y:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the Y coordinate acquired in the last update-status action.
 *
 * Returns: the Y coordinate.
 *
 * Since: 1.0
 **/
gdouble
machinery_grbl_y(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    gdouble y;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), 0);

    data = machinery_grbl_get_instance_private(grbl);

    g_mutex_lock(&data->status_lock);
    y = data->y;
    g_mutex_unlock(&data->status_lock);

    return y;
}

/**
 * machinery_grbl_z:
 * @grbl: a #MachineryGrbl instance
 *
 * Gets the Z coordinate acquired in the last update-status action.
 *
 * Returns: the Z coordinate.
 *
 * Since: 1.0
 **/
gdouble
machinery_grbl_z(MachineryGrbl *grbl)
{
    MachineryGrblPrivate *data;
    gdouble z;

    g_return_val_if_fail(MACHINERY_IS_GRBL(grbl), 0);

    data = machinery_grbl_get_instance_private(grbl);

    g_mutex_lock(&data->status_lock);
    z = data->z;
    g_mutex_unlock(&data->status_lock);

    return z;
}
