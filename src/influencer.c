/**
 * @file  influencer.c
 * @brief Queue for threads conditioning
 *
 * Conditioning a thread from another thread with a queue-like API.
 */

#include "machinery.h"
#include "gobject.h"


typedef struct {
    const gchar *action;
    const gchar *channel;
    gdouble data;
} Influence;


/**
 * Initialize an application instance for influencer support.
 *
 * This should be called before any other influencer related calls.
 * Initialization and finalization calls are expected to be performed in the
 * main thread.
 *
 * Once initialized, push and pop calls can be raised in different threads.
 * The goal is to allow some sort of "conditioning" between threads, e.g. to
 * allow the GUI thread to force some output state in manual mode.
 *
 * The passed in strings are **not** copied, so you are expected to keep them
 * alive as long as you use them (statically allocated strings are fine).
 *
 *     static char *DO_CHANGE = "digital-output-change";
 *     static char *AO_CHANGE = "analog-output-change";
 *     static char *QUIT = "quit";
 *
 *
 *     // IN THE GUI THREAD
 *     // Change a digital output
 *     app_influencer_push(app, DO_CHANGE, "digital-channel-name", 1);
 *     // Change an analog output
 *     app_influencer_push(app, AO_CHANGE, "analog-channel-name", 1.234);
 *     // Quit the machinery thread
 *     app_influencer_push(app, QUIT, NULL, 0);
 *
 *
 *     // IN THE MACHINERY THREAD
 *     const char *action;
 *     const char *channel;
 *     double data;
 *     app_influencer_pop(app, &action, &channel, &data);
 *     if (action == DO_CHANGE) {
 *         // Handle digital output change action
 *         ...
 *     } else if (action == AO_CHANGE) {
 *         // Handle analog output change action
 *         ...
 *     } else if (action == QUIT) {
 *         // Handle quit action
 *         ...
 *     }
 *
 * @param app An App instance to initialize
 */
void
app_influencer_init(App *app)
{
    GAsyncQueue *influencer;

    info("influencer: construction");
    influencer = g_async_queue_new();
    if (influencer == NULL) {
        die_with_errno("g_async_queue_new");
        return;
    }

    app->influencer = influencer;
}

/**
 * Finalize the influencer section of an application.
 *
 * This is the counterpart of app_influencer_init().
 *
 * @param app The instance to free
 */
void
app_influencer_free(App *app)
{
    GAsyncQueue *queue = app->influencer;
    if (queue != NULL) {
        info("influencer: destruction");
        g_async_queue_unref(queue);
        app->influencer = NULL;
    }
}

/**
 * Push a new action into the influencer.
 *
 * The real meaning of the arguments is defined by the user code calling the
 * `app_influencer_pop()` API. Keep in mind the lifetime of the passed in
 * arguments must survive until that call, so you cannot usually pass stack
 * allocated data.
 *
 * @param app      The app instance
 * @param action   A string identifying the action
 * @param channel  Optional string identifying the channel, or NULL
 * @param data     Optional data value, or 0
 */
void
app_influencer_push(App *app, const char *action, const char *channel, double data)
{
    GAsyncQueue *queue = app->influencer;
    Influence *influence = g_new(Influence, 1);
    influence->action = action;
    influence->channel = channel;
    influence->data = data;
    g_async_queue_push(queue, influence);
}

/**
 * Pop the oldest action from the influencer.
 *
 * The real meaning of the arguments is defined by the user code calling the
 * `app_influencer_pop()` API. Keep in mind the lifetime of the passed in
 * arguments must survive until that call, so you cannot usually pass stack
 * allocated data.
 *
 * @param app      The app instance
 * @param action   A string identifying the action, must be valid
 * @param channel  Optional string identifying the channel, or NULL
 * @param data     Optional data value, or NULL
 */
void
app_influencer_pop(App *app, const char **action, const char **channel, double *data)
{
    GAsyncQueue *queue = app->influencer;
    Influence *influence = g_async_queue_try_pop(queue);
    if (influence != NULL) {
        *action = influence->action;
        if (channel != NULL) {
            *channel = influence->channel;
        }
        if (data != NULL) {
            *data = influence->data;
        }
        g_free(influence);
    } else {
        *action = NULL;
    }
}

/**
 * Return the number of actions queued by this influencer.
 *
 * @param app  The app instance
 * @return     The numbe of queued actions, or `0` if the queue is empty
 */
int
app_influencer_queued(App *app)
{
    GAsyncQueue *queue = app->influencer;
    return g_async_queue_length(queue);
}

