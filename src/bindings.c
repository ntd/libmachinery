/**
 * @file  bindings.c
 * @brief Binding helper functions.
 */

#include "machinery.h"
#include "gtk.h"
#include <stdlib.h>
#include <string.h>


/* In GtkSpinButton, to differentiate between button pressed inside up and
 * down panels and inside the entry box, I sperimentally found the event
 * mask is different only because button 1 and 3 motions (the mouse wheel)
 * are enabled in the entry box */
#define WHEEL_MASK              (GDK_BUTTON1_MOTION_MASK | GDK_BUTTON3_MOTION_MASK)
#define UPDOWN_EVENT(w,e)       (GTK_IS_SPIN_BUTTON(w) && (gdk_window_get_events((e)->window) & WHEEL_MASK) == 0)


typedef struct {
    GSettings *         settings;
    gchar *             prefix;
    GSettingsBindFlags  flags;
} SettingsContext;

typedef struct {
    GSettings *         settings;
    gchar *             key;
    gint                index;
    gchar *             property;
} BindArraySettings;

typedef struct {
    GSettings *         settings;
    gchar *             key;
    GtkWidget *         widget;
} BindSettings;

typedef struct {
    GtkDialog *         input_dialog;
    gchar *             title;
    gboolean            spin_hack;
} InputContext;


static void         settings_to_file_chooser        (BindSettings *     binding);
static void         settings_to_radio_group         (BindSettings *     binding);


static gdouble
variant_get_value(GVariant *variant)
{
    const gchar *type = g_variant_get_type_string(variant);

    switch (type[0]) {

        case 'y':
            return g_variant_get_byte(variant);

        case 'n':
            return g_variant_get_int16(variant);

        case 'q':
            return g_variant_get_uint16(variant);

        case 'i':
            return g_variant_get_int32(variant);

        case 'u':
            return g_variant_get_uint32(variant);

        case 'x':
            return g_variant_get_int64(variant);

        case 't':
            return g_variant_get_uint64(variant);

        case 'd':
            return g_variant_get_double(variant);
    }

    g_return_val_if_reached(0);
}

static GVariant *
variant_new_value(const gchar *type, gdouble value)
{
    switch (type[0]) {

        case 'y':
            return g_variant_new_byte((guint8) value);

        case 'n':
            return g_variant_new_int16((gint16) value);

        case 'q':
            return g_variant_new_uint16((guint16) value);

        case 'i':
            return g_variant_new_int32((gint32) value);

        case 'u':
            return g_variant_new_uint32((guint32) value);

        case 'x':
            return g_variant_new_int64((gint64) value);

        case 't':
            return g_variant_new_uint64((guint64) value);

        case 'd':
            return g_variant_new_double(value);
    }

    g_return_val_if_reached(NULL);
}

static void
settings_array_free(gpointer data)
{
    BindArraySettings *binding = data;
    g_free(binding->key);
    g_free(binding->property);
    g_free(binding);
}

static gboolean
settings_array_to_widget(GValue *property, GVariant *setting, gpointer user_data)
{
    const BindArraySettings *binding;
    GVariant *item;
    gdouble value;

    binding = user_data;
    item = g_variant_get_child_value(setting, binding->index);
    g_return_val_if_fail(item != NULL, FALSE);

    value = variant_get_value(item);
    g_variant_unref(item);

    g_value_set_double(property, value);

    return TRUE;
}

static GVariant *
settings_array_from_widget(const GValue *property,
                           const GVariantType *expected_type,
                           gpointer user_data)
{
    const BindArraySettings *binding;
    GVariantBuilder builder;
    GVariant *original, *item;
    GVariantIter *iter;
    gint i;

    binding = user_data;
    original = g_settings_get_value(binding->settings, binding->key);

    /* Not sure a GVariant array can be modified in place, so here we are just
     * recreating it and substituting the new item */
    g_variant_builder_init(&builder, expected_type);
    iter = g_variant_iter_new(original);
    for (i = 0; (item = g_variant_iter_next_value(iter)) != NULL; ++i) {
        if (i == binding->index) {
            const gchar *type = g_variant_get_type_string(item);
            gdouble value = g_value_get_double(property);
            GVariant *new_item = variant_new_value(type, value);
            g_variant_unref(item);
            item = new_item;
        }
        g_variant_builder_add_value(&builder, item);
    }
    g_variant_iter_free(iter);

    g_variant_unref(original);
    return g_variant_builder_end(&builder);
}

static void
bind_array_item(GtkWidget *widget, SettingsContext *context,
                const gchar *key, const gchar *property)
{
    gchar **token;
    BindArraySettings *binding;

    token = g_strsplit_set(key, "[", 0);
    g_return_if_fail(token[1] != NULL);
    binding = g_new(BindArraySettings, 1);
    binding->index = atoi(token[1]);
    binding->key = g_strdup(token[0]);
    binding->property = g_strdup(property);
    binding->settings = context->settings;

    g_settings_bind_with_mapping(context->settings, binding->key,
                                 G_OBJECT(widget), binding->property,
                                 context->flags,
                                 settings_array_to_widget,
                                 settings_array_from_widget,
                                 binding,
                                 settings_array_free);
    g_strfreev(token);
}

static void
bind_settings(GtkWidget *widget, SettingsContext *context)
{
    const gchar *name;
    gchar **token;

    /* Recursively traverse the children if this is a container */
    if (GTK_IS_CONTAINER(widget)) {
        GList *children = gtk_container_get_children(GTK_CONTAINER(widget));
        g_list_foreach(children, (GFunc) bind_settings, context);
        g_list_free(children);
    }

    name = gtk_widget_get_name(widget);
    if (name == NULL || ! g_str_has_prefix(name, context->prefix)) {
        return;
    }

    token = g_strsplit_set(name, ".", 0);

    if (GTK_IS_FILE_CHOOSER(widget)) {
        /* Only one identifier expected after the prefix: setting key */
        g_return_if_fail(token[1] != NULL && token[2] == NULL);
        machinery_bind_settings_file_chooser(GTK_FILE_CHOOSER(widget),
                                             context->settings, token[1]);
    } else {
        /* Two identifiers expected: setting key and widget property */
        g_return_if_fail(token[2] != NULL && token[3] == NULL);
        if (strchr(token[1], '[') != NULL) {
            bind_array_item(widget, context, token[1], token[2]);
        } else {
            g_settings_bind(context->settings,
                            token[1], G_OBJECT(widget), token[2],
                            context->flags);
        }
    }

    g_strfreev(token);
}

/**
 * Autobind a widget tree to a set of GSettings.
 *
 * Scan `widget` and its children looking for widgets which have their name
 * property following a specific pattern, that is:
 *
 *      widget-name = prefix "." setting-key "." property-name
 *      setting-key = key | array-key
 *      array-key   = key "[" number "]"
 *
 * `prefix` is the string passed in the arguments. `property-name` must be a
 * valid property of the widget. `key` is the name of a setting that must be
 * present in @p settings. The `array-key` feature can be used to bind the
 * widget property to a specific item inside an array.
 *
 * After successful execution, the widget property will be bound to the
 * setting, i.e. changing the property will change the setting and vice-versa.
 * See the *Binding* section in the official documentation of
 * [GSettings](https://developer.gnome.org/gio/stable/GSettings.html)
 * for further details.
 *
 * @param widget    The root of the widget tree to analyze
 * @param settings  The settings to bind
 * @param prefix    The conventional prefix on the widget name that
 *                  identifies a widget to bind
 * @param flags     The flags to use when calling g_settings_bind()
 */
void
machinery_bind_settings(GtkWidget *widget, GSettings *settings,
                        const gchar *prefix, GSettingsBindFlags flags)
{
    SettingsContext context;
    context.prefix = g_strconcat(prefix, ".", NULL);
    context.settings = settings;
    context.flags = flags;
    bind_settings(widget, &context);
    g_free(context.prefix);
}

static void
file_chooser_to_settings(BindSettings *binding)
{
    GtkFileChooser *file_chooser = GTK_FILE_CHOOSER(binding->widget);
    gchar *path = gtk_file_chooser_get_filename(file_chooser);

    /* Block signal handler to avoid mutual recursion */
    g_signal_handlers_block_by_func(binding->settings,
                                    settings_to_file_chooser,
                                    binding);
    g_settings_set_string(binding->settings, binding->key, path);
    g_signal_handlers_unblock_by_func(binding->settings,
                                      settings_to_file_chooser,
                                      binding);

    g_free(path);
}

static void
settings_to_file_chooser(BindSettings *binding)
{
    GtkFileChooser *file_chooser = GTK_FILE_CHOOSER(binding->widget);
    gchar *path = g_settings_get_string(binding->settings, binding->key);

    /* Block signal handler to avoid mutual recursion */
    g_signal_handlers_block_by_func(file_chooser,
                                    file_chooser_to_settings,
                                    binding);
    gtk_file_chooser_select_filename(file_chooser, path);
    g_signal_handlers_unblock_by_func(file_chooser,
                                      file_chooser_to_settings,
                                      binding);

    g_free(path);
}

/**
 * Bind a GtkFileChooser widget to a single GSettings.
 *
 * A property with the full pathname does not exist in GtkFileChooser, so is
 * is not possible to directly use g_settings_bind(). This function adds the
 * needed signal handlers to simulate such binding without using properties.
 *
 * @param file_chooser  The widget to bind
 * @param settings      A set of settings
 * @param key           The key of the specific setting to bind
 */
void
machinery_bind_settings_file_chooser(GtkFileChooser *file_chooser,
                                     GSettings *settings, const gchar *key)
{
    BindSettings *binding;
    gchar *signal_name;

    /* Create a BindSettings with the same lifetime of file_chooser */
    binding = g_new(BindSettings, 1);
    binding->settings = settings;
    binding->key = g_strdup(key);
    binding->widget = GTK_WIDGET(file_chooser);
    g_signal_connect_swapped(file_chooser, "destroy",
                             G_CALLBACK(g_free), binding);

    g_signal_connect_swapped(file_chooser, "file-set",
                             G_CALLBACK(file_chooser_to_settings), binding);

    signal_name = g_strdup_printf("changed::%s", key);
    g_signal_connect_swapped(settings, signal_name,
                             G_CALLBACK(settings_to_file_chooser), binding);
    g_free(signal_name);

    /* Force the initial update of settings -> file_chooser:
     * GSettings does this too automatically */
    settings_to_file_chooser(binding);
}

static void
radio_button_to_settings(GtkRadioButton *radio_button, BindSettings *binding)
{
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio_button))) {
        gint value = machinery_widget_get_index(GTK_WIDGET(radio_button));
        /* Block signal handler to avoid mutual recursion */
        g_signal_handlers_block_by_func(binding->settings,
                                        settings_to_radio_group,
                                        binding);
        g_settings_set_int(binding->settings, binding->key, value);
        g_signal_handlers_unblock_by_func(binding->settings,
                                          settings_to_radio_group,
                                          binding);
    }
}

static void
settings_to_radio_group(BindSettings *binding)
{
    GSList *node = gtk_radio_button_get_group(GTK_RADIO_BUTTON(binding->widget));
    gint value = g_settings_get_int(binding->settings, binding->key);
    GtkRadioButton *radio_button;

    while (node != NULL) {
        radio_button = node->data;
        if (machinery_widget_get_index(GTK_WIDGET(radio_button)) == value) {
            /* Block signal handler to avoid mutual recursion */
            g_signal_handlers_block_by_func(radio_button,
                                            radio_button_to_settings,
                                            binding);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio_button), TRUE);
            g_signal_handlers_unblock_by_func(radio_button,
                                              radio_button_to_settings,
                                              binding);
            return;
        }
        node = node->next;
    }
}

/**
 * Bind a GtkRadioButton group to an integer GSettings.
 *
 * Any radio button that is part of the group is expected to have a different
 * index: see machinery_widget_get_index() for details of what an index is.
 *
 * As an example, a typical group that contains three elements (e.g.
 * `radio-1`, `radio-2` and `radio-3`) will change the setting bound to it
 * (and vice-versa) to 1 2 or 3, depending on the radio selected.
 *
 * @param radio_group  The first radio button of the group
 * @param settings     A set of settings
 * @param key          The key of the specific setting to bind
 */
void
machinery_bind_settings_radio_group(GtkRadioButton *radio_group,
                                     GSettings *settings, const gchar *key)
{
    BindSettings *binding;
    gchar *signal_name;
    GSList *node;

    /* Create a BindSettings with the same lifetime of radio_group */
    binding = g_new(BindSettings, 1);
    binding->settings = settings;
    binding->key = g_strdup(key);
    binding->widget = GTK_WIDGET(radio_group);
    g_signal_connect_swapped(radio_group, "destroy",
                             G_CALLBACK(g_free), binding);

    node = gtk_radio_button_get_group(radio_group);
    while (node != NULL) {
        g_signal_connect(node->data, "toggled",
                         G_CALLBACK(radio_button_to_settings), binding);
        node = node->next;
    }

    signal_name = g_strdup_printf("changed::%s", key);
    g_signal_connect_swapped(settings, signal_name,
                             G_CALLBACK(settings_to_radio_group), binding);
    g_free(signal_name);

    /* Force the initial update of settings -> radio_group:
     * GSettings does this too automatically */
    settings_to_radio_group(binding);
}

static void
trigger_dialog(GtkEntry *entry, const InputContext *context)
{
    const gchar *prefill = gtk_entry_get_text(entry);
    gchar *message = gtk_widget_get_tooltip_text(GTK_WIDGET(entry));
    gchar *value = machinery_input_run(context->input_dialog,
                                       context->title,
                                       message, prefill);
    g_free(message);

    if (value != NULL) {
        gtk_entry_set_text(entry, value);
        g_free(value);
        if (GTK_IS_SPIN_BUTTON(entry)) {
            gtk_spin_button_update(GTK_SPIN_BUTTON(entry));
        }
    }
}

static gboolean
entry_button_release(GtkEntry *entry, GdkEventButton *event, const InputContext *context)
{
    /* If the spin button hack is enabled, ignore events in the up-down panels
     * of a GtkSpinButton */
    if (event->button == 1 &&
        (! context->spin_hack || ! UPDOWN_EVENT(entry, event))) {
        trigger_dialog(entry, context);
    }
    return FALSE;
}

static InputContext *
input_context_dup(const InputContext *context)
{
    InputContext *dup = g_new(InputContext, 1);
    dup->input_dialog = context->input_dialog;
    dup->title = g_strdup(context->title);
    dup->spin_hack = context->spin_hack;
    return dup;
}

static void
input_context_destroy(InputContext *context)
{
    g_free(context->title);
    g_free(context);
}

static void
bind_input_dialog(GtkWidget *widget, InputContext *context)
{
    if (GTK_IS_CONTAINER(widget)) {
        GList *children = gtk_container_get_children(GTK_CONTAINER(widget));
        g_list_foreach(children, (GFunc) bind_input_dialog, context);
        g_list_free(children);
    } else if (GTK_IS_ENTRY(widget) && gtk_editable_get_editable(GTK_EDITABLE(widget))) {
        /* `context` cannot be used directly because of its lifetime */
        InputContext *dup = input_context_dup(context);
        gtk_widget_add_events(widget, GDK_BUTTON_PRESS_MASK);
        g_signal_connect(widget, "button-release-event",
                         G_CALLBACK(entry_button_release), dup);
        g_signal_connect_swapped(widget, "destroy",
                                 G_CALLBACK(input_context_destroy), dup);
    }
}

/**
 * Show an input dialog when clicking each entry of a widget tree.
 *
 * Scan `widget` and its children looking for `GtkEntry` based widgets. When
 * found, bound a handler function that shows the specified `input_dialog`
 * whenever a button press event occurs inside the input area.
 *
 * The input dialog can be created with machinery_input_dialog_new(): it
 * contains a label and an entry, possibly with a virtual keyboard for easy
 * input on touchscreen devices. The dialog entry will be prefilled with the
 * actual value of the `GtkEntry` while the label will be filled with its
 * eventual tooltip text.
 *
 * @param widget       The root of the widget tree to analyze
 * @param input_dialog The input dialog to show
 * @param title        Title to set on the input dialog
 */
void
machinery_bind_input_dialog(GtkWidget *widget, GtkDialog *input_dialog,
                            const gchar *title)
{
    InputContext context;
    context.input_dialog = input_dialog;
    context.title = (gchar *) title;
    context.spin_hack = TRUE;
    bind_input_dialog(widget, &context);
}

/**
 * Show an input dialog when clicking each entry of a widget tree.
 *
 * Similar to machinery_bind_input_dialog(), but without the spinbutton
 * hack. That means clicking on the increase and decrease button trigger
 * the input dialog.
 *
 * @param widget       The root of the widget tree to analyze
 * @param input_dialog The input dialog to show
 * @param title        Title to set on the input dialog
 */
void
machinery_bind_input_dialog_without_spin_hack(GtkWidget *widget,
                                              GtkDialog *input_dialog,
                                              const gchar *title)
{
    InputContext context;
    context.input_dialog = input_dialog;
    context.title = (gchar *) title;
    context.spin_hack = FALSE;
    bind_input_dialog(widget, &context);
}
