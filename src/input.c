/**
 * @file  input.c
 * @brief A generic input dialog.
 *
 * An input dialog is a `GtkDialog` with an optional label, a `GtkEntry` box
 * and possibly a virtual keyboard for easy input on touchscreen devices.
 */

#include "machinery.h"
#include "gtk.h"
#include <gtk/gtkx.h>
#include <signal.h>

#define _(s)    ((char *) g_dgettext("gtk30", (s)))


static GtkDialog *
input_dialog(GtkWindow *parent, gint width, gint height)
{
    GtkWidget *widget;
    GtkDialog *dialog;
    GtkContainer *contents;

    widget = gtk_dialog_new_with_buttons(NULL, parent,
                                         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                         _("gtk-cancel"), GTK_RESPONSE_CANCEL,
                                         _("gtk-apply"), GTK_RESPONSE_APPLY,
                                         NULL);
    dialog = GTK_DIALOG(widget);

    gtk_widget_set_size_request(widget, width, height);
    gtk_dialog_set_default_response(dialog , GTK_RESPONSE_APPLY);

    contents = GTK_CONTAINER(gtk_dialog_get_content_area(dialog));
    gtk_container_set_border_width(contents, 6);
    gtk_box_set_spacing(GTK_BOX(contents), 6);

    widget = gtk_label_new(NULL);
    gtk_label_set_justify(GTK_LABEL(widget), GTK_JUSTIFY_CENTER);
    gtk_label_set_line_wrap(GTK_LABEL(widget), TRUE);
    gtk_label_set_max_width_chars(GTK_LABEL(widget), 0);
    gtk_widget_set_size_request(widget, width - 20, 0);
    gtk_container_add(contents, widget);

    widget = gtk_entry_new();
    gtk_entry_set_activates_default(GTK_ENTRY(widget), TRUE);
    gtk_entry_set_alignment(GTK_ENTRY(widget), 0.5);
    gtk_container_add(contents, widget);
    gtk_widget_show_all(GTK_WIDGET(contents));

    return dialog;
}

static void
attach_osk(GtkDialog *dialog, gint64 xid)
{
    if (xid != 0) {
        GtkWidget *widget, *contents;
        gint height;

        widget = gtk_socket_new();
        contents = gtk_dialog_get_content_area(dialog);

        gtk_window_get_size(GTK_WINDOW(dialog), NULL, &height);
        gtk_widget_set_size_request(widget, -1, height - 120);
        gtk_container_add(GTK_CONTAINER(contents), widget);
        gtk_socket_add_id(GTK_SOCKET(widget), (Window) xid);
        gtk_widget_show(widget);
    }
}

static gboolean
parse_command_output(GIOChannel *channel, GIOCondition cond, gpointer data)
{
    gchar *line = NULL;
    g_io_channel_read_line(channel, &line, NULL, NULL, NULL);
    if (line != NULL) {
        /* The OSK command is expected to return the XID (an integer)
         * on its own line */
        gint xid = atoi(line);
        g_free(line);
        if (xid > 0) {
            info("OSK command returned XID %d", xid);
            attach_osk((GtkDialog *) data, xid);
            return FALSE;
        }
    }
    return TRUE;
}

static void
kill_osk_process(GtkDialog *dialog, gpointer data)
{
    GPid pid = GPOINTER_TO_INT(data);
    if (pid != 0) {
        kill(pid, SIGTERM);
        g_spawn_close_pid(pid);
    }
}

static gboolean
trigger_osk_command(GtkDialog *dialog, const gchar *cmd)
{
    gchar **argv = NULL;
    GError *err = NULL;
    gint standard_output;
    GPid pid;

    g_shell_parse_argv(cmd, NULL, &argv, NULL);
    g_spawn_async_with_pipes(NULL, argv, NULL,
                             G_SPAWN_DO_NOT_REAP_CHILD | G_SPAWN_SEARCH_PATH,
                             NULL, NULL, &pid,
                             NULL, &standard_output, NULL,
                             &err);
    g_strfreev(argv);

    if (err != NULL) {
        warning("Unable to execute '%s': %s", cmd, err->message);
        g_error_free(err);
        return FALSE;
    }

    g_signal_connect(dialog, "destroy",
                     (GCallback) kill_osk_process, GINT_TO_POINTER(pid));

    g_io_add_watch(g_io_channel_unix_new(standard_output),
                   G_IO_IN, parse_command_output, dialog);
    return TRUE;
}


/**
 * Create a new input dialog.
 *
 * The dialog will be set transient for `parent`, if specified. Its size
 * request will be set to the specified `width` and `height`. If provided
 * (i.e. greater than 0), `keyboard_plug` must contain the window ID of the
 * external keyboard to show under the entry. Any keyboard supporting the
 * [XEmbed protocol](https://www.freedesktop.org/wiki/Specifications/xembed-spec/)
 * should work. [Onboard](https://launchpad.net/onboard) is the typical
 * example of such applications.
 *
 * @param parent         The parent window
 * @param width          Minimum width, in pixel
 * @param height         Minimum height, in pixel
 * @param keyboard_plug  The window ID of the virtual keyboard
 * @return               A newly created GtkDialog
 */
GtkDialog *
machinery_input_new(GtkWindow *parent, gint width, gint height, gint64 keyboard_plug)
{
    GtkDialog *dialog = input_dialog(parent, width, height);
    attach_osk(dialog, keyboard_plug);
    return dialog;
}

/**
 * Create a new input dialog.
 *
 * Similar to machinery_input_new(), but directly passing the command
 * to call instead of the XID of the virtual keyboard. The output of
 * the command is parsed line per line until a number is found: that
 * number will be considered the XID of the virtual keyboard.
 *
 * @param parent  The parent window
 * @param width   Minimum width, in pixel
 * @param height  Minimum height, in pixel
 * @param cmd     The command to call, e.g. "onboard -e",
 *                or %NULL for no virtual keyboard support
 * @return        A newly created GtkDialog
 */
GtkDialog *
machinery_input_new_with_command(GtkWindow *parent, gint width, gint height, const gchar *cmd)
{
    GtkDialog *dialog = input_dialog(parent, width, height);
    if (cmd != NULL) {
        trigger_osk_command(dialog, cmd);
    }
    return dialog;
}

/**
 * Run the input dialog.
 *
 * In this context, running means the dialog is shown and gtk_dialog_run() is
 * called.
 *
 * The dialog is not created every time but instead it is modified
 * dynamically before being shown. This is what `title`, `message` and
 * `prefill` are used for.
 *
 * @param input    The input dialog
 * @param title    The text to show in the title bar
 * @param message  The text to show in the label
 * @param prefill  The initial text to set in the entry box
 * @return         The final text set by the entry box: it must be freed with
 *                 g_free() when no longer needed.
 */
gchar *
machinery_input_run(GtkDialog *input,
                    const gchar *title, const gchar *message,
                    const gchar *prefill)
{
    GList *children;
    GtkLabel *label;
    GtkEntry *entry;
    gint response;

    /* The first child is the label, the second one is the entry */
    children = gtk_container_get_children(GTK_CONTAINER(gtk_dialog_get_content_area(input)));
    label = GTK_LABEL(children->data);
    entry = GTK_ENTRY(children->next->data);
    g_list_free(children);

    gtk_label_set_markup(label, message != NULL ? message : "");
    gtk_entry_set_text(entry, prefill != NULL ? prefill : "");
    gtk_widget_grab_focus(GTK_WIDGET(entry));
    gtk_window_set_title(GTK_WINDOW(input), title);
    response = gtk_dialog_run(input);
    gtk_widget_hide(GTK_WIDGET(input));

    return response == GTK_RESPONSE_APPLY ? g_strdup(gtk_entry_get_text(entry)) : NULL;
}
