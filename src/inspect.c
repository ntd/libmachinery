#include "machinery.h"
#include "share-private.h"

#include <stdio.h>

int
main(int argc, char *argv[])
{
    App app;
    Share *share;
    Zone *zone;
    const char *comma;

    if (argc <= 1 || argv[1] == NULL || *argv[1] == '\0') {
        printf("Fatal error: no application name provided.\n"
               "Usage: %s APPLICATION\n",
               argv[0]);
        return 1;
    }

    app_init(&app, argv[1]);
    app_share_init(&app, 0);
    share = app.share;

    printf("      Owner pid: %ld\n"
           " Header address: %p\n"
           "  Computed size: %zu\n",
           (long int) share->header->owner, share->header, app_share_size(&app));

    for (int n = 0; n < share->header->n_zones; ++n) {
        zone = ZONE(share, n);
        printf("---------------- ZONE %d\n"
               "      Zone size: %zu\n"
               "Revision number: %u\n"
               "   Heap address: %p\n"
               "   Watcher pids:",
               n, zone->size, zone->revision, share->heaps[n]);
        comma = "";
        for (int i = 0; i < MACHINERY_MAX_WATCHERS; ++i) {
            pid_t pid = zone->watcher[i];
            if (pid > 0) {
                printf("%s %ld", comma, (long int) pid);
                comma = ",";
            }
        }
        printf("\n");
    }

    app_share_free(&app);
    app_free(&app);

    return 0;
}
