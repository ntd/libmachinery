/**
 * @file  log.c
 * @brief Logging functions
 */

#include "machinery.h"

#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>

/**
 * Generic debug function.
 *
 * A printf() like command that outputs a debug message to the syslog
 * subsystem.
 *
 * @param format A printf() like format string
 * @param ... Additional arguments, if required by the format string
 */
void
debug(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vsyslog(LOG_DEBUG, format, args);
    va_end(args);
}

/**
 * Generic informational function.
 *
 * A printf() like command that outputs an info message to the syslog
 * subsystem.
 *
 * @param format A printf() like format string
 * @param ... Additional arguments, if required by the format string
 */
void
info(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vsyslog(LOG_INFO, format, args);
    va_end(args);
}

/**
 * Generic warning function.
 *
 * A printf() like command that outputs a warning message to the syslog
 * subsystem.
 *
 * @param format A printf() like format string
 * @param ... Additional arguments, if required by the format string
 */
void
warning(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vsyslog(LOG_WARNING, format, args);
    va_end(args);
}

/**
 * Exit from the current process with error.
 *
 * Logs an error message to the syslog subsystem and quits the current process
 * by calling quit(APP_FAILURE). This function never returns.
 *
 * @param format A printf() like format string
 * @param ... Additional arguments, if required by the format string
 */
void
die(const char *format, ...)
{
    if (format != NULL) {
        va_list args;
        va_start(args, format);
        vsyslog(LOG_ERR, format, args);
        va_end(args);
    }

    quit(APP_FAILURE);
}

/**
 * Quit the current process showing the last errno status.
 *
 * A helper function that wraps die() and shows a string describing the
 * current errno status. The caller argument, if not NULL, is prefixed to the
 * error message.
 *
 * @param caller Eventual prefix to prepend to the error message
 */
void
die_with_errno(const char *caller)
{
    char message[128];
    strerror_r(errno, message, sizeof(message));
    if (caller != NULL) {
        die("%s error: %s", caller, message);
    } else {
        die("%s", message);
    }
}
