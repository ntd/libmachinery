---
-- DSP402 CANopen support.
--
-- The implementation strictly follows what stated by CiA DSP 402 v.2.0.
-- Actually only profile position mode (with homing), velocity mode and
-- profile velocity mode are implemented.
--
-- It needs to be integrated into your real-time loop. The following
-- pseudo-code highlights the basic steps needed:
-- ```
-- -- EXAMPLE OF A PROFILE POSITION AXIS
-- do
--     -- STEP 1: INSTANTIATION (MANDATORY)
--     local handler = dsp402.new {
--         -- You need to provide these APIs in some way: check
--         -- the `dsp402.new()` documentation for details
--         status_word = ...,
--         control_word = ...,
--         target_point = ...,
--         sdo_download = ...,
--     }
--
--     -- STEP 2: LOOP INTEGRATION (MANDATORY)
--     -- The loop function needs to to be called on each iteration
--     add_to_my_loop(handler:profile_position())
--
--
--     -- STEP 3: CONFIGURATION (OPTIONAL)
--     -- `dsp402.configure()` is idempotent, so you can call it
--     -- multiple times without problems, possibly changing settings
--     local function configure()
--         handler:configure {
--             homing_method        = 19,
--             homing_speed_search  = 1000,
--             homing_speed_zero    = 100,
--             homing_acceleration  = 1000,
--             profile_velocity     = 1000,
--             profile_acceleration = 2000,
--             profile_deceleration = 2000,
--         }
--     end
--
--     -- STEP 4: ENJOY (MANDATORY :)
--     -- You can call any public api in your own code
--
--     -- Example: simple automata to move an axis in counterclockwise
--     -- direction slowly, clockwise quickly, and repeat forever
--     local start, slow_ccw, quick_cw
--     function start()
--         configure()
--         return slow_ccw
--     end
--
--     function slow_ccw()
--         handler:configure { profile_velocity = 200 }
--         handler:set_target(-500000)
--         return handler.runtime.done and quick_cw or slow_ccw
--     end
--
--     function quick_cw()
--         handler:configure { profile_velocity = 2000 }
--         handler:set_target(500000)
--         return handler.runtime.done and slow_ccw or quick_cw
--     end
--
--     local state = start
--     add_to_my_loop(function ()
--         state = state()
--     end)
-- end
-- ```
--
-- Using an axis in other modes is quite similar:
-- ```
-- -- EXAMPLE OF A VELOCITY MODE AXIS
-- do
--     -- STEP 1
--     local roller = dsp402.new { ... }
--
--     -- STEP 2
--     add_to_my_loop(roller:velocity())
--
--     local start, move, stop
--     function start()
--         -- STEP 3
--         roller:configure {
--             velocity_acceleration   = 500,
--             velocity_acceleration_u = 1,
--             velocity_deceleration   = 500,
--             velocity_deceleration_u = 1,
--         }
--         return move
--     end
--
--     -- STEP 4
--     function move()
--         roller:set_target(1000)
--         return roller.runtime.done and stop or move
--     end
--
--     function stop()
--         roller:set_target(0)
--         return stop
--     end
--
--     local state = start
--     add_to_my_loop(function ()
--         state = state()
--     end)
-- end
-- ```
local dsp402 = {}
dsp402.__index = dsp402

-- Compatibility layer for bitwise operators
local band, bor, bnot
if bit32 then
    -- Lua < 5.3
    band = bit32.band
    bor  = bit32.bor
    bnot = bit32.bnot
elseif bit then
    -- LuaJIT
    band = bit.band
    bor  = bit.bor
    bnot = bit.bnot
else
    -- Lua >= 5.3
    band = function (a, b) return a & b end
    bor  = function (a, b) return a | b end
    bnot = function (a) return ~a end
end

---
-- Status word to automata state map.
--
-- Only bits 0, 1, 2, 3, 5 and 6 are considered, so the status word is
-- masked with `b1101111` (`0x6F`) to cut out the irrelevant bits.
--
-- Excerpt from table 7 (Device state bits):
-----------------------------------------------
--     BIT     HEX VALUE  AUTOMATA STATE
-- 6 5 3 2 1 0
-- 0 x 0 0 0 0 0x00,0x20  Not ready to switch on
-- 1 x 0 0 0 0 0x40,0x60  Switch on disabled
-- 0 1 0 0 0 1 0x21       Ready to switch on
-- 0 1 0 0 1 1 0x23       Switched on
-- 0 1 0 1 1 1 0x27       Operation enabled
-- 0 0 0 1 1 1 0x07       Quick stop active
-- 0 x 1 1 1 1 0x0F,0x2F  Fault reaction active
-- 0 x 1 0 0 0 0x08,0x28  Fault
-----------------------------------------------
--
-- Format:
-- ```
-- [status word AND 0x6F] = state
-- ```
local map_status = {
    [0x00] = 'Not ready to switch on',
    [0x20] = 'Not ready to switch on',
    [0x40] = 'Switch on disabled',
    [0x60] = 'Switch on disabled',
    [0x21] = 'Ready to switch on',
    [0x23] = 'Switched on',
    [0x27] = 'Operation enabled',
    [0x07] = 'Quick stop active',
    [0x0F] = 'Fault reaction active',
    [0x2F] = 'Fault reaction active',
    [0x08] = 'Fault',
    [0x28] = 'Fault',
}

---
-- Transition needed to change states.
--
-- A transition defines a command (or a serie of commands) to change the
-- state of the drive. For example, inferring from figure 8 (State
-- machine), going from 'Switch on disabled' to 'Operation enabled'
-- requires 3 different steps:
-- - Action 2, command 'Shutdown': state becomes 'Ready to switch on'
-- - Action 3, command 'Switch on': state becomes 'Switched on'
-- - Action 4, command 'Enable operation': state becomes 'Operation enabled'
--
-- Transition 16 (switching from 'Quick stop active' to 'Operation
-- enabled' by sending a 'Enable operation' command) is possible only
-- when the drive keeps the quick stop (mode 5, 6, 7 and 8, see section
-- 10.3.5), otherwise the drive automatically goes to 'Switch on
-- disabled' when the quick stop has finished. Sending the 'Enable
-- operation' command on both cases should not be a problem though.
--
-- Format:
-- ```
-- [new state][old state] = command
-- ```
local map_transition = {
    ['Switch on disabled']     = {                    -- Transitions:
        ['Ready to switch on'] = 'Disable voltage',   -- 7
        ['Switched on']        = 'Disable voltage',   -- 10
        ['Operation enabled']  = 'Disable voltage',   -- 9
        ['Quick stop active']  = 'Disable voltage',   -- 12
        ['Fault']              = 'Fault reset',       -- 15
    },
    ['Ready to switch on']     = {
        ['Switch on disabled'] = 'Shutdown',          -- 2
        ['Switched on']        = 'Shutdown',          -- 6
        ['Operation enabled']  = 'Shutdown',          -- 8
        ['Quick stop active']  = 'Disable voltage',   -- 12+2
        ['Fault']              = 'Fault reset',       -- 15+2
    },
    ['Switched on']            = {
        ['Switch on disabled'] = 'Shutdown',          -- 2+3
        ['Ready to switch on'] = 'Switch on',         -- 3
        ['Operation enabled']  = 'Disable operation', -- 5
        ['Quick stop active']  = 'Enable operation',  -- 16+5
        ['Fault']              = 'Fault reset',       -- 15+2+3
    },
    ['Operation enabled']      = {
        ['Switch on disabled'] = 'Shutdown',          -- 2+3+4
        ['Ready to switch on'] = 'Switch on',         -- 3+4
        ['Switched on']        = 'Enable operation',  -- 4
        ['Quick stop active']  = 'Enable operation',  -- 16
        ['Fault']              = 'Fault reset',       -- 15+2+3+4
    },
    ['Quick stop active']      = {
        ['Switch on disabled'] = 'Shutdown',          -- 2+3+4+11
        ['Ready to switch on'] = 'Switch on',         -- 3+4+11
        ['Switched on']        = 'Enable operation',  -- 4+11
        ['Operation enabled']  = 'Quick stop',        -- 11
        ['Fault']              = 'Fault reset',       -- 15+2+3+4+11
    },
}

---
-- Masks to be applied to the control word.
--
-- Whenever a command is requested, some bits on the control word need
-- to be reset and others set, specifically bits 0, 1, 2, 3 and 7.
-- This table provides the needed reset and set mask to be applied for
-- each supported command.
--
-- Excerpt from table 4 (Device control commands):
-----------------------------------------------------------
--    BIT      RESET MASK    SET MASK    COMMAND
-- 7 3 2 1 0
-- 0 X 1 1 0  10001 (0x81) 00110 (0x06)  Shutdown
-- 0 ? 1 1 1  10000 (0x80) 00111 (0x07)  Switch on
-- 0 X X 0 X  10010 (0x82) 00000 (0x00)  Disable voltage
-- 0 X 0 1 X  10100 (0x84) 00010 (0x02)  Quick stop
-- 0 0 1 1 1  11000 (0x88) 00111 (0x07)  Disable operation
-- 0 1 1 1 1  10000 (0x80) 01111 (0x0F)  Enable operation
-- ^ X X X X  00000 (0x00) 10000 (0x80)  Fault reset
-----------------------------------------------------------
--
-- Format:
-- ```
-- [command] = { [0] = reset mask, [1] = set mask }
-- ```
local command_masks = {
    ['Shutdown']          = { [0] = 0x81, [1] = 0x06 },
    ['Switch on']         = { [0] = 0x80, [1] = 0x07 },
    ['Disable voltage']   = { [0] = 0x82, [1] = 0x00 },
    ['Quick stop']        = { [0] = 0x84, [1] = 0x02 },
    ['Disable operation'] = { [0] = 0x88, [1] = 0x07 },
    ['Enable operation']  = { [0] = 0x80, [1] = 0x0F },
    ['Fault reset']       = { [0] = 0x00, [1] = 0x80 },
}

---
-- Return the current drive state.
--
-- It returns one of the following strings:
--
-- - 'Not ready to switch on'
-- - 'Switch on disabled'
-- - 'Ready to switch on'
-- - 'Switched on'
-- - 'Operation enabled'
-- - 'Quick stop active'
-- - 'Fault reaction active'
-- - 'Fault'
--
-- Detailed description of each state can can be found in section
-- 10.1.1.1 (Drive states).
--
-- It raises an error in case of an unexpected status word.
--
-- @param self A dsp402 instance
-- @return     The current device state, e.g. 'Switched on'
local function current_state(self)
    local status = self.api.status_word()
    local state  = map_status[band(status, 0x6F)]
    assert(state, string.format('Status word is 0x%04X: unexpected', status))
    return state
end

local function set_fault(self)
    if self.runtime.fault then return end
    self.runtime.fault = true
    if self.api.fault then self.api.fault() end
end

---
-- Transition to a new state.
--
-- This function must be called repeatedly until it returns true. In
-- the meantime it is allowed to change new_state, also before the
-- transition has been completed.
--
-- It raises an error in case of an unexpected transition requested.
--
-- @param self  A dsp402 instance
-- @param state The new state, e.g. 'Operation enabled'
-- @return      true on success, false on transition still running
local function switch(self, state)
    local old_state = current_state(self)

    -- Fault handling
    if old_state == 'Fault reaction active' then
        -- Wait until the fault condition is handled properly
        set_fault(self)
        return false
    elseif old_state == 'Fault' then
        -- This is set to `false` only by `dsp402:reset_fault()`
        if self.runtime.fault ~= false then
            set_fault(self)
            -- In external reset mode, the state machine blocks waiting
            -- for a `dsp402:reset_fault()` call
            if self.api.fault then return false end
        end
    else
        -- When not in fault mode, clear the `self.runtime.fault` state
        self.runtime.fault = nil
    end

    if old_state == state then
        -- Transition successful
        return true
    elseif old_state == 'Not ready to switch on' then
        -- Wait until the driver has started
        return false
    end

    local command = (map_transition[state] or {})[old_state]
    assert(command, string.format('Transition from %s to %s not supported', old_state, state))

    local masks = command_masks[command]
    assert(masks, string.format('Command %s unrecognized', command))

    local word = self.api.control_word()
    word = bor(word, masks[1])
    word = band(word, bnot(masks[0]))
    self.api.control_word(word)
    return false
end

---
-- Individually change the action bits in the control word.
--
-- The new state can be true to enable the bit, false to disable it or
-- nil to leave it unchanged.
--
-- @param self A dsp402 instance
-- @param bit4 The new bit4 state
-- @param bit5 The new bit5 state
-- @param bit6 The new bit6 state
-- @param halt The new bit8 state
-- @return     true
local function control(self, bit4, bit5, bit6, halt)
    local s, r = 0, 0
    if bit4 == true  then s = bor(s, 0x0010) end
    if bit5 == true  then s = bor(s, 0x0020) end
    if bit6 == true  then s = bor(s, 0x0040) end
    if halt == true  then s = bor(s, 0x0100) end
    if bit4 == false then r = bor(r, 0x0010) end
    if bit5 == false then r = bor(r, 0x0020) end
    if bit6 == false then r = bor(r, 0x0040) end
    if halt == false then r = bor(r, 0x0100) end

    local word = self.api.control_word()
    word = bor(word, s)
    word = band(word, bnot(r))
    self.api.control_word(word)

    return true
end

---
-- Ensure a list of settings is set to some specific value.
--
-- This function iterates over `map` (an associative array of setting
-- names and object dictionary entries) ensuring that all the settings
-- are synchronized, eventually performing SDO writes to clear the
-- inconsistencies.
--
-- The values of the settings you want to change must be previously
-- configured with `dsp402.configure()`. The settings will be applied
-- during the initial iterations of the loop function.
--
-- @param self A dsp402 instance
-- @param map  Associative array of settings
-- @return     true if all settings are in sync, false otherwise
local function synchronize(self, map)
    assert(self.api.sdo_download, 'sdo_download() function not defined')
    for name, entry in pairs(map) do
        local value = self.request[name]
        if value and self.runtime[name] ~= value then
            -- Consider errors (i.e. when sdo_download() returns false)
            -- successful, otherwise we enter an infinite loop
            if self.api.sdo_download(entry, value) ~= nil then
                self.runtime[name] = value
            end
            return false
        end
    end
    return true
end

---
-- Settings shared across different modes.
--
-- You can always configure the following settings:
--
-- - oc_shutdown: section 10.3.3
-- - oc_disable_operation: section 10.3.4
-- - oc_quick_stop: section 10.3.5
-- - oc_halt: section 10.3.6
-- - oc_fault_reaction: section 10.3.7
-- - modes_of_operation: section 10.3.8
--
-- @param self A dsp402 instance
-- @return     true if all settings are in sync, false otherwise
-- @see        dsp402.configure()
local function common_settings(self)
    return synchronize(self, {
        oc_shutdown          = 0x605B0010,
        oc_disable_operation = 0x605C0010,
        oc_quick_stop        = 0x605A0010,
        oc_halt              = 0x605D0010,
        oc_fault_reaction    = 0x605E0010,
        modes_of_operation   = 0x60600008,
    })
end

---
-- Execute the movement in profile position mode.
--
-- Helper function that performs the needed steps to set a new
-- destination in PP mode, yielding when waiting for a condition.
--
-- @param self A dsp402 instance
-- @return     true
local function move(self)
    -- Reset "New set-point" ...
    control(self, false)
    -- ... and wait for "Set-point acknowledge" off
    repeat
        coroutine.yield()
    until band(self.api.status_word(), 0x1000) == 0
    -- Set "New set-point" ...
    control(self, true, true, self.runtime.relative, false)
    -- ... and wait for "Set-point acknowledge" on
    repeat
        coroutine.yield()
    until band(self.api.status_word(), 0x1000) > 0
    -- Reset "New set-point" ...
    control(self, false)
    -- ... and wait "Target reached" on
    repeat
        coroutine.yield()
    until band(self.api.status_word(), 0x400) > 0
    return true
end

---
-- Helper function to log the automata state.
--
-- This function calls the step api function (if defined) whenever the
-- internal automata state changes.
--
-- @param self A dsp402 instance
-- @param n    The new state
local function step(self, n)
    if n == self.step then return end
    self.step = n
    if self.api.step then self.api.step(n) end
end

---
-- Create a new DSP402 object.
--
-- The `api` table is must contains the following keys:
--
-- - `status_word` (required)
--   A `function ()` that returns the status word of this device.
-- - `control_word` (required)
--   A `function (value)` to get or set the control word. If `value` is
--   not provided, this function must return the current value of the
--   control word. If `value` is provided, the control word must be set
--   to that value.
-- - `target_point` (required)
--   A `function (value)` to get or set the target point. If `value` is
--   not provided, this function must return the current target point.
--   If `value` is provided, the target point must be set to that value.
-- - `sdo_download` (required)
--   A `function (register, value)` for changing an object dictionary
--   entry. `register` is a 32 bit integer containing index (16 bit),
--   subindex (8 bit) and length (8 bit) of the entry while `value`
--   (a number) is the data to write. It must return `true` on success,
--   `false` on error or `nil` if still in progress.
--   ```
--   -- Write 123 to entry 605B:00, a 16 (0x10) bits register:
--   api.sdo_download(0x605B0010, 123)
--   ```
-- - `step` (optional)
--   A `function (n)` called on every transition of the internal state
--   machine, useful for debugging purposes. `n` is an integer
--   identifying the current state: see the code for details.
-- - `fault` (optional)
--   A `function ()` called whenever the internal state machine is set
--   to 'Fault' or 'Fault reaction active'. Keep in mind that, when this
--   callback is specified, the device is in external reset mode, i.e.
--   the fault mode must be reset manually by the callee with a
--   `dsp402:reset_fault()` call.
--
-- @param api Callbacks to use
-- @return    A table representing the DSP402 instance
function dsp402.new(api)
    -- Sanity checks
    assert(api.status_word, 'status_word() function not defined')
    assert(api.control_word, 'control_word() function not defined')
    assert(api.target_point, 'target_point() function not defined')

    local obj = setmetatable({}, dsp402)
    obj.api     = api
    obj.request = {}
    obj.runtime = {}
    return obj
end

---
-- Round function used internally.
--
-- This function is exposed because in some situation using the same
-- rounding algorithm can be useful.
--
-- @param value A number to be rounded to integer
-- @return      The rounded value
function dsp402.round(value)
    if value then
        return value >= 0 and math.floor(value + 0.5) or math.ceil(value - 0.5)
    else
        return value
    end
end

---
-- Configure settings.
--
-- Generic configuration entry point. You should call this method
-- providing meaningful values, e.g.:
-- ```
-- instance:configure {
--   homing_speed_search = 123,
--   homing_speed_zero   = 456,
--   homing_acceleration = 789,
--   home_offset         = 1234,
--   homing_method       = 5,
-- }
-- ```
-- The exact names you can use depends on the mode of operation
-- selected: check the documentation of the relevant loop function for
-- further details.
--
-- Keep in mind the order in which the settings are changed is not
-- predictable.
--
-- @param self   A dsp402 instance
-- @param params Associative array of settings and values
function dsp402:configure(params)
    for k, v in pairs(params) do
        self.request[k] = v
    end
end

---
-- Change the set point.
--
-- @param self   A dsp402 instance
-- @param target The new set point.
-- @return       true if the set point has changed, false otherwise
function dsp402:set_target(target)
    target = dsp402.round(target)
    if target == self.request.target then
        return false
    end
    self.runtime.done   = false
    self.request.target = target
    return true
end

---
-- Set the halt state.
--
-- If `status` is true, immediatley stops the axis with deceleration. If
-- `status` is false, the halt state is reset, eventually restarting the
-- axis movement.
--
-- @param      self    A dsp402 instance
-- @param[opt] status  The new status, if needed
-- @return             The halt flag status after this call
function dsp402:halt(status)
    if status ~= nil then
        self.request.halt = status
    end
    return self.request.halt
end

---
-- Set or get the internal "relative" flag.
--
-- In profile position, setting this flag to `true` makes all movements
-- relative to the original position.
--
-- @param      self    A dsp402 instance
-- @param[opt] status  The new status, if needed
-- @return             The relative flag status after this call
function dsp402:relative(status)
    if status ~= nil then
        self.runtime.relative = status
    end
    return self.runtime.relative
end

---
-- Set or get the internal "home" flag.
--
-- In profile position, resetting this flag triggers a homing before
-- the next movement.
--
-- @param      self    A dsp402 instance
-- @param[opt] status  The new status, if needed
-- @return             The home flag status after this call
function dsp402:home(status)
    if status ~= nil then
        self.runtime.home_done = status
    end
    return self.runtime.home_done
end

---
-- Reset the internal home flag.
--
-- On specific modes (e.g. in profile position) this triggers a
-- homing before the next movement.
--
-- DEPRECATED: use dsp402:home() instead.
--
-- @param self A dsp402 instance
function dsp402:reset_home()
    self:home(false)
end

---
-- Reset the internal fault flag.
--
-- @param self A dsp402 instance
function dsp402:reset_fault()
    self.runtime.fault = false
end

---
-- Loop function (profile position mode).
--
-- In addition to the common ones, you can configure these settings too:
--
-- - profile_velocity: section 12.3.6
-- - profile_acceleration: section 12.3.8
-- - profile_deceleration: section 12.3.9
-- - motion_profile_type: section 12.3.11
-- - home_offset: section 13.3.1
-- - homing_method: section 13.3.2
-- - homing_speed_search: section 13.3.3
-- - homing_speed_zero: section 13.3.3
-- - homing_acceleration: section 13.3.4
--
-- @param self A dsp402 instance
-- @return     A loop function
-- @see        dsp402.common_settings()
-- @see        dsp402.configure()
function dsp402:profile_position()
    return function ()
        if not common_settings(self) then
            -- Common configuration in progress...
            step(self, 1)
        elseif not synchronize(self, {
                profile_velocity     = 0x60810020,
                profile_acceleration = 0x60830020,
                profile_deceleration = 0x60840020,
                motion_profile_type  = 0x60860010,
                home_offset          = 0x607C0020,
                homing_method        = 0x60980008,
                homing_speed_search  = 0x60990120,
                homing_speed_zero    = 0x60990220,
                homing_acceleration  = 0x609A0020,
            }) then
            -- Mode configuration in progress...
            step(self, 2)
        elseif self.request.target == nil then
            -- nil target: stop the motor and any eventual action in progress
            self.worker = nil
            if switch(self, 'Switched on') then
                step(self, 4)
                control(self, false, false)
                self.runtime.target = self.request.target
                self.runtime.done   = true
            else
                step(self, 5)
            end
        elseif self.request.target == false then
            -- false target: stop and disable
            self.worker = nil
            if switch(self, 'Ready to switch on') then
                step(self, 16)
                control(self, false, false)
                self.runtime.target = self.request.target
                self.runtime.done   = true
            else
                step(self, 17)
            end
        elseif self.request.target == self.runtime.target then
            -- Target reached: nothing to do
            step(self, 3)
            self.runtime.done = true
        elseif self.runtime.home_done then
            -- New position requested
            if self.runtime.modes_of_operation ~= 1 then
                -- Switch from unknown mode
                step(self, 6)
                control(self, false)
                if switch(self, 'Switched on') then
                    self.request.modes_of_operation = 1
                end
            elseif not switch(self, 'Operation enabled') then
                -- Enable profile position mode
                step(self, 7)
            elseif self.request.target ~= self.api.target_point() then
                -- Set the new target
                step(self, 8)
                self.worker = nil
                self.api.target_point(self.request.target)
            elseif self.request.halt then
                control(self, nil, nil, nil, true)
                if self.worker then
                    step(self, 9)
                    self.worker = nil
                elseif band(self.api.status_word(), 0x400) > 0 then
                    -- Halting terminated successfully
                    step(self, 10)
                    self.runtime.target = nil
                    self.runtime.done   = true
                end
            elseif not self.worker then
                -- Create the worker coroutine
                step(self, 11)
                self.worker = coroutine.wrap(move)
            elseif self.worker(self) then
                -- Positioning terminated successfully
                step(self, 12)
                if self.runtime.relative then
                    -- In relative positioning, the destination
                    -- position is always 0
                    self.request.target = 0
                end
                self.runtime.target = self.request.target
                self.worker = nil
            else
                step(self, 13)
            end
        else
            -- Homing
            if self.runtime.modes_of_operation ~= 6 then
                -- Set homing mode
                step(self, 13)
                control(self, false)
                if switch(self, 'Switched on') then
                    self.request.modes_of_operation = 6
                end
            elseif switch(self, 'Operation enabled') and
                control(self, true) and
                band(self.api.status_word(), 0x1000) > 0 then
                step(self, 14)
                self.runtime.target    = nil
                self.runtime.home_done = true
                control(self, false)
            else
                step(self, 15)
            end
        end
    end
end

---
-- Loop function (velocity mode).
--
-- In addition to the common ones, you can configure these settings too:
--
-- - velocity_acceleration: section 18.3.16 (default in RpM/min)
-- - velocity_acceleration_u: section 18.3.16 (set to 60 if undefined)
-- - velocity_deceleration: section 18.3.17 (default in RpM/min)
-- - velocity_deceleration_u: section 18.3.17 (set to 60 if undefined)
--
-- @param self A dsp402 instance
-- @return     A loop function
-- @see        dsp402.common_settings()
-- @see        dsp402.configure()
function dsp402:velocity()
    return function ()
        local target = self.request.target
        if not common_settings(self) then
            -- Common configuration in progress...
            step(self, 1)
        elseif not synchronize(self, {
                velocity_acceleration   = 0x60480120,
                velocity_acceleration_u = 0x60480210,
                velocity_deceleration   = 0x60490120,
                velocity_deceleration_u = 0x60490210,
            }) then
            -- Mode configuration in progress...
            step(self, 2)
        elseif target == self.runtime.target then
            -- Target reached: nothing to do
            step(self, 3)
            self.runtime.done = true
        elseif not target then
            -- nil target: stop any eventual action in progress
            control(self, false)
            if switch(self, 'Switched on') then
                step(self, 4)
                control(self, false, false)
                self.runtime.target = target
            else
                step(self, 5)
            end
        elseif self.runtime.modes_of_operation ~= 2 then
            -- Switch from unknown mode
            step(self, 6)
            if switch(self, 'Switched on') then
                self.request.modes_of_operation = 2
            end
        elseif not switch(self, 'Operation enabled') then
            -- Enable velocity mode
            step(self, 7)
        elseif target ~= self.api.target_point() then
            -- Set the new target
            step(self, 8)
            self.api.target_point(target)
            -- Ensure RFG-disable is ON
            control(self, true)
        end
    end
end

---
-- Loop function (profile velocity mode).
--
-- In addition to the common ones, you can configure these settings too:
--
-- - profile_acceleration: section 12.3.8
-- - profile_deceleration: section 12.3.9
-- - motion_profile_type: section 12.3.11
--
-- @param self A dsp402 instance
-- @return     A loop function
-- @see        dsp402.common_settings()
-- @see        dsp402.configure()
function dsp402:profile_velocity()
    return function ()
        local target = self.request.target
        if not common_settings(self) then
            -- Common configuration in progress...
            step(self, 1)
        elseif not synchronize(self, {
                profile_acceleration = 0x60830020,
                profile_deceleration = 0x60840020,
                motion_profile_type  = 0x60860010,
            }) then
            -- Mode configuration in progress...
            step(self, 2)
        elseif target == self.runtime.target then
            -- Target reached: nothing to do
            step(self, 3)
            self.runtime.done = true
        elseif not target then
            -- nil target: stop any eventual action in progress
            if switch(self, 'Switched on') then
                step(self, 4)
                control(self, false, false)
                self.runtime.target = target
            else
                step(self, 5)
            end
        elseif self.runtime.modes_of_operation ~= 3 then
            -- Switch from unknown mode
            step(self, 6)
            if switch(self, 'Switched on') then
                self.request.modes_of_operation = 3
            end
        elseif not switch(self, 'Operation enabled') then
            -- Enable profile velocity mode
            step(self, 7)
        elseif target ~= self.api.target_point() then
            -- Set the new target
            step(self, 8)
            self.api.target_point(target)
        end
    end
end

return dsp402
