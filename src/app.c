/**
 * @file  app.c
 * @brief Methods for App instances
 *
 * Application instances handle the initialization and finalization of shared
 * memory stuff. This file contains the methods available to any App instance.
 */

#include "machinery.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>

/**
 * @struct App
 * @brief  Application instance.
 *
 * This should be considered an opaque object bound to a single
 * application. A single project can have multiple applications (AKA
 * processes) but all of them manage the same data.
 */

/**
 * Initialize an App instance.
 *
 * Given an already instantiated App struct, initializes all the relevant
 * fields to their default values.
 *
 * @param app The instance to initialize
 * @param name Descriptive name of the application
 */
void
app_init(App *app, const char *name)
{
    app->name       = strdup(name);
    app->module     = NULL;
    app->pid        = NULL;
    app->share      = NULL;
    app->lua        = NULL;
    app->lua_name   = NULL;
    app->canopen    = NULL;
    app->ethercat   = NULL;
    app->timer      = NULL;
    app->influencer = NULL;

    /* Initialize the log subsystem */
    openlog(app->name, LOG_CONS, LOG_USER);
}

/**
 * Finalize an App instance.
 *
 * This is the counterpart of the app_init() method. Free any allocated
 * resource and reset the values of every field.
 *
 * @param app The instance to free
 */
void
app_free(App *app)
{
    closelog();

    if (app->lua_name != NULL) {
        free(app->lua_name);
        app->lua_name = NULL;
    }

    if (app->name != NULL) {
        free(app->name);
        app->name = NULL;
    }
}
