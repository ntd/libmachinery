#ifndef MACHINERY_ETHERCAT_INTERNAL_H_
#define MACHINERY_ETHERCAT_INTERNAL_H_

#include <ecrt.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const char *    name;
    const char *    description;
    uint32_t        vendor_id;
    uint32_t        product_code;
    uint32_t        revision_number;
    ec_sync_info_t *syncs;
    unsigned        nsyncs;
} MachinerySlaveConfig;

#ifdef __cplusplus
}
#endif

#endif /* MACHINERY_ETHERCAT_INTERNAL_H_ */
