/**
 * @file  permission.c
 * @brief A basic permission mechanism based on a callback.
 */

#include "machinery.h"
#include "gtk.h"

#define PRIVATE(x) \
    machinery_permission_get_instance_private((MachineryPermission *) (x))


typedef GPermission      MachineryPermission;
typedef GPermissionClass MachineryPermissionClass;

/**
 * @private
 */
typedef struct {
    gchar *             password;
    PermissionCallback  callback;
} MachineryPermissionPrivate;


G_DEFINE_TYPE_WITH_PRIVATE(MachineryPermission, machinery_permission, G_TYPE_PERMISSION)

static void
machinery_permission_acquire_async(GPermission        *permission,
                                   GCancellable       *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer            user_data)
{
    callback((GObject *) permission, NULL, user_data);
}

static gboolean
machinery_permission_acquire_finish(GPermission  *permission,
                                    GAsyncResult *result,
                                    GError      **error)
{
    MachineryPermissionPrivate *priv = PRIVATE(permission);
    gchar *text = priv->callback();

    /* No need to be paranoid: just check the password in clear text */
    gboolean unlock = g_strcmp0(text, priv->password) == 0;

    if (unlock) {
        g_permission_impl_update(permission, TRUE, TRUE, TRUE);
    } else {
        gint code = text == NULL ? G_IO_ERROR_CANCELLED : G_IO_ERROR_PERMISSION_DENIED;
        *error = g_error_new_literal(G_IO_ERROR, code, "unauthorized");
    }

    return unlock;
}

static void
machinery_permission_release_async(GPermission        *permission,
                                   GCancellable       *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer            user_data)
{
    callback((GObject *) permission, NULL, user_data);
}

static gboolean
machinery_permission_release_finish(GPermission  *permission,
                                    GAsyncResult *result,
                                    GError      **error)
{
    g_permission_impl_update(permission, FALSE, TRUE, TRUE);
    return TRUE;
}

static void
machinery_permission_finalize(GObject *gobject)
{
    MachineryPermissionPrivate *priv = PRIVATE(gobject);

    g_free(priv->password);

    G_OBJECT_CLASS (machinery_permission_parent_class)->finalize (gobject);
}

static void
machinery_permission_class_init(MachineryPermissionClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS(class);

    object_class->finalize = machinery_permission_finalize;

    class->acquire_async = machinery_permission_acquire_async;
    class->acquire_finish = machinery_permission_acquire_finish;
    class->release_async = machinery_permission_release_async;
    class->release_finish = machinery_permission_release_finish;
}

static void
machinery_permission_init(MachineryPermission *permission)
{
    MachineryPermissionPrivate *priv = PRIVATE(permission);
    priv->password = NULL;
}

static GPermission *
machinery_permission_new(gboolean allowed,
                         const gchar *password,
                         PermissionCallback callback)
{
    GPermission *permission = g_object_new(machinery_permission_get_type(), NULL);
    MachineryPermissionPrivate *priv = PRIVATE(permission);

    g_permission_impl_update(permission, allowed, TRUE, TRUE);
    priv->password = g_strdup(password);
    priv->callback = callback;

    return permission;
}

static void
permission_set_sensitive(GPermission *permission, GParamSpec *pspec, GtkWidget *widget)
{
    gboolean state = g_permission_get_allowed(permission);
    gtk_widget_set_sensitive(widget, state);
}

/**
 * Bind the default permission infrastructure to a lock button.
 *
 * Creates an internal GPermission provider and sets it to \p lock_button.
 * You should pass the password in clear text and a callback for getting the
 * text input (usually throught an input dialog).
 *
 * The sensitiveness of the \p protected widget is set according to the current
 * authorization status.
 *
 * @param lock_button Where to bind the new GPermission instance
 * @param protected   Optional widget to sensitivize/desensitivize
 * @param password    Password in clear text
 * @param callback    Function to call for getting text input
 */
void
machinery_permission(GtkLockButton *lock_button, GtkWidget *protected,
                     const gchar *password, PermissionCallback callback)
{
    GPermission *permission = machinery_permission_new(FALSE, password, callback);
    gtk_lock_button_set_permission(lock_button, permission);
    if (protected != NULL) {
        g_signal_connect(permission, "notify",
                         G_CALLBACK(permission_set_sensitive), protected);
    }
}
