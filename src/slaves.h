#ifndef MACHINERY_SLAVES_H_
#define MACHINERY_SLAVES_H_

#include <canfestival.h>


#ifdef __cplusplus
extern "C" {
#endif

ActionStatus    dsp401_16di8do              (App *      app,
                                             uint8_t    slave_id,
                                             int        step);
ActionStatus    dsp401_16di16do             (App *      app,
                                             uint8_t    slave_id,
                                             int        step);
ActionStatus    dsp401_16di8do8do           (App *      app,
                                             uint8_t    slave_id,
                                             int        step);
ActionStatus    dsp402_profile_position     (App *      app,
                                             uint8_t    slave_id,
                                             int        step);
ActionStatus    dsp402_profile_velocity     (App *      app,
                                             uint8_t    slave_id,
                                             int        step);
ActionStatus    dsp402_velocity             (App *      app,
                                             uint8_t    slave_id,
                                             int        step);

#ifdef __cplusplus
}
#endif

#endif /* MACHINERY_SLAVES_H_ */
