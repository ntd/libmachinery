#ifndef SHARE_PRIVATE_H_
#define SHARE_PRIVATE_H_

#include <semaphore.h>

#define MACHINERY_SHM_FORMAT    "/%s"
#define MACHINERY_MAX_WATCHERS  30

#define SHARE(a)    ((Share *) (a->share))
#define ZONE(s,n)   ((Zone *) ((uint8_t *) (s)->header + sizeof(Header)) + (n))


typedef struct {
    pid_t       owner;
    int         n_zones;
} Header;

typedef struct {
    unsigned    revision;
    size_t      size;
    pid_t       watcher[MACHINERY_MAX_WATCHERS];
    sem_t       lock;
} Zone;

typedef struct {
    Header *    header;
    void **     heaps;
} Share;


#endif
