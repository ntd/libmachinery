/**
 * @file  timer.c
 * @brief General purpose periodic functions
 *
 * A timerfd based periodic timer to be used in controller loops.
 */

#include "machinery.h"

#include <stdlib.h>
#include <sys/time.h>
#include <sys/timerfd.h>


typedef struct {
    int fd;
    uint64_t missed;
    struct timespec last;
    double busy;
} Timer;


/**
 * Initialize the periodic timer of an application instance.
 *
 * This should be called before any other timer related methods. Any blocking
 * error will generate a relevant message and will quit the current process.
 *
 * The internal timer is initialized and will start after the provided delay.
 * Subsequent calls to app_timer_wait() will suspend the execution until the
 * next delay occurs. This allows an easy integration with any loop, e.g.:
 *
 *     app_timer_init(app, 20000);
 *     for (;;) {
 *         // Your code goes here
 *         // ...
 *         app_timer_wait(app);
 *     }
 *     app_timer_free(app);
 *
 * @param app  An App instance to initialize
 * @param usec The interval, in microseconds
 */
void
app_timer_init(App *app, long usec)
{
    Timer *timer;
    int fd;
    time_t sec;
    long nsec;
    struct itimerspec interval;
    struct timespec start;

    info("timer: construction");

    /* Better would be CLOCK_MONOTONIC_RAW, but `man 2 timerfd_create`
     * implies it is not supported yet */
    fd = timerfd_create(CLOCK_MONOTONIC, 0);
    if (fd < 0) {
        die_with_errno("timerfd_create");
        return;
    }

    /* Arm the timer */
    sec  = usec / 1000000;
    nsec = (usec - (sec * 1000000)) * 1000;
    interval.it_interval.tv_sec  = sec;
    interval.it_interval.tv_nsec = nsec;
    interval.it_value.tv_sec     = sec;
    interval.it_value.tv_nsec    = nsec;
    if (timerfd_settime(fd, 0, &interval, NULL) < 0) {
        die_with_errno("timerfd_settime");
        return;
    }
    if (clock_gettime(CLOCK_MONOTONIC_RAW, &start) < 0) {
        die_with_errno("clock_gettime");
        return;
    }

    app->timer = timer = malloc(sizeof(Timer));
    timer->missed = 0;
    timer->fd = fd;
    timer->last = start;
    timer->busy = 0.0;
}

/**
 * Finalize the timer section of an application.
 *
 * This is the counterpart of app_timer_init().
 *
 * @param app The instance to free
 */
void
app_timer_free(App *app)
{
    if (app->timer) {
        info("timer: destruction");
        Timer *timer = app->timer;
        close(timer->fd);
        free(timer);
        app->timer = NULL;
    }
}

/**
 * Wait the next period.
 *
 * Suspend the execution of the current process until the next period expires.
 * If the period is already expired, returns immediately and update the missed
 * counter.
 *
 * See app_timer_init() for code examples.
 *
 * @param app The application instance
 */
void
app_timer_wait(App *app)
{
    Timer *timer = app->timer;
    uint64_t missed = 0;
    struct timespec now;
    double busy;

    if (clock_gettime(CLOCK_MONOTONIC_RAW, &now) < 0) {
        die_with_errno("clock_gettime");
        return;
    }
    busy = now.tv_sec - timer->last.tv_sec +
           (now.tv_nsec - timer->last.tv_nsec) / 1000000000.0;
    if (busy > timer->busy) {
        timer->busy = busy;
    }

    if (read(timer->fd, &missed, sizeof(missed)) < 0) {
        die_with_errno("read");
        return;
    }
    timer->missed += missed;

    if (clock_gettime(CLOCK_MONOTONIC_RAW, &timer->last) < 0) {
        die_with_errno("clock_gettime");
        return;
    }
}

/**
 * Get the value of the missed counter.
 *
 * Whenever a call to app_timer_wait() happens after the periodic delay
 * already expired, an internal counter is updated accordingly. This value
 * is mainly useful for debugging.
 *
 * @param app The application instance
 * @return    The number of missed periods.
 */
long
app_timer_get_missed(App *app)
{
    Timer *timer = app->timer;
    return timer->missed;
}

/**
 * Get the busy time.
 *
 * Return the maximum busy time recorded by this timer. The busy time
 * is the time elapsed between two subsequent calls to app_timer_wait().
 * This is useful to track the "responsiveness" of the system, i.e. if
 * the hardware is underloaded or overloaded.
 *
 * @param app The application instance
 * @return    The maximum busy time, in seconds
 */
double
app_timer_get_busy(App *app)
{
    Timer *timer = app->timer;
    return timer->busy;
}

/**
 * Clear the busy time.
 *
 * Any subsequent call to app_timer_wait() will set it again. Check
 * app_timer_get_busy() to see what the busy time is.
 *
 * @param app The application instance
 */
void
app_timer_clear_busy(App *app)
{
    Timer *timer = app->timer;
    timer->busy = 0;
}
