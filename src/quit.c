/**
 * @file  quit.c
 * @brief Define the quit() function
 *
 * Directly using the exit() function from the code will make testing from
 * hard to impossible because wrapping a builtin is really difficult.
 *
 * This file implements an exit() surrogate, quit(), that can be easily
 * wrapped by [CMocka](https://cmocka.org/) for easy testing. It also avoids
 * some hard to solve problem, e.g. the exit() function is defined manually
 * (and not throught stdlib) because in there it has additional attributes
 * (i.e. noreturn) that will prevent the CMocka wrapping.
 */

#include "machinery.h"
#include <stdlib.h>

/**
 * @enum  AppStatus
 * @brief Return value of an application instance.
 *
 * This is a value returned to the operating system, where 0 (APP_SUCCESS)
 * indicates a succesful execution while any other value highlights a failure.
 */

static QuitHandler quit_handler = (QuitHandler) exit;

/**
 * Set a new quit handler.
 *
 * By default a quit() call will result in an exit() call. You can
 * provide your own custom handler for further customization.
 *
 * @param handler The new handler to be called by quit()
 * @see quit.c
 */
void
quit_set_handler(QuitHandler handler)
{
    quit_handler = handler;
}

/**
 * Quit the current process.
 *
 * By default this function never returns. You can customize the
 * behavior of this function by providing a custom quit handler: see
 * set_quit_handler() for more information.
 *
 * @param status The status this process will be ended with
 * @see quit.c
 */
void
quit(AppStatus status)
{
    quit_handler(status);
}
