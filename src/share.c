/**
 * @file  share.c
 * @brief Sharing memory between processes
 *
 * API for sharing memory across different processes. You should call
 * app_share_init() before any other call and app_share_free() when everything
 * is done.
 *
 * It is expected that there is one (and only one) process that calls
 * app_share_init() with `n_zones` greater than 0. Such "master" process will
 * handle the initial creation and allocation of the shared memory
 * infrastructure and its destruction upon termination.
 *
 * Sharing is handled by so called "zones", i.e. a contiguous region of shared
 * memory. Callers can access that zone for reading or writing by using the
 * app_share_read() and app_share_write() functions.
 *
 * Here is how the data is managed:
 * ~~~
 * Shared memory
 *  +---------+          +---------+
 *  | Header  | <------- |  Share  |
 *  +---------+          +---------+
 *  | Zone[0] |   +----- | heap[0] |
 *  +---------+   |      +---------+
 *  |   ...   |   |      |   ...   |
 *  +---------+   |      +---------+
 *  | Zone[n] |   |   +- | heap[n] |
 *  +---------+   |   |  +---------+
 *  |         | <-+   |
 *  |  heap   |       |
 *  | memory  |       |
 *  |         | <-----+
 *  +---------+
 * ~~~
 * The underlying implementation keeps one mutex for each zone, so reading or
 * writing to it will forbid access to other processes. It is hence advisable
 * to keep into the same zone only omogeneous data, i.e. data that should be
 * usually accessed as a whole.
 *
 * Notification of zone changes is handled by sending a `SIGUSR1` POSIX signal
 * to a configurable list of watchers. Use app_share_watch() to add the
 * current process to the watchers of a zone.
 *
 * \attention On Linux AFAIK POSIX signals can be sent only between processes
 * that are owned by the same user. You cannot for instance start the master
 * process as root and the other processes as normal user.
 */

#include "machinery.h"
#include "share-private.h"

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

#define MMAP(s,f)       mmap(0, (s), PROT_READ|PROT_WRITE, MAP_SHARED, (f), 0)
#define NAME(a)         malloc_printf(MACHINERY_SHM_FORMAT, (a)->name)


static bool
zone_init(Zone *zone, size_t size)
{
    int n;
    zone->revision = 0;
    for (n = 0; n < MACHINERY_MAX_WATCHERS; ++n) {
        zone->watcher[n] = 0;
    }
    if (sem_init(&zone->lock, 1, 1) != 0) {
        die_with_errno("sem_init");
        /* A 0 zone size avoids sem_destroy */
        zone->size = 0;
        return false;
    }
    zone->size = size;
    return true;
}

static void
zone_free(Zone *zone)
{
    if (zone->size > 0) {
        sem_destroy(&zone->lock);
        zone->size = 0;
    }
}

static void
zones_init(Share *share, int *sizes)
{
    int n_zones = share->header->n_zones;
    Zone *zone  = ZONE(share, 0);
    void *heap  = (uint8_t *) zone + sizeof(Zone) * n_zones;
    int n;

    share->heaps = calloc(n_zones, sizeof(void *));
    for (n = 0; n < n_zones; ++n) {
        if (sizes && ! zone_init(zone, sizes[n])) {
            /* Avoid initializing other zones on error */
            return;
        }

        share->heaps[n] = heap;
        heap += zone->size;
        ++zone;
    }
}

static void
zones_free(Share *share)
{
    int n;
    for (n = 0; n < share->header->n_zones; ++n) {
        zone_free(ZONE(share, n));
    }
}

static bool
lock(sem_t *sem, long timeout)
{
    if (timeout > 0) {
        struct timespec timestamp;
        long nanoseconds;

        /* Calculate the timestamp for timed out response */
        if (clock_gettime(CLOCK_REALTIME, &timestamp) == -1) {
            die_with_errno("clock_gettime");
        }
        nanoseconds = timestamp.tv_nsec + timeout;
        timestamp.tv_sec  += nanoseconds / 999999999;
        timestamp.tv_nsec += nanoseconds % 999999999;

        switch (sem_timedwait(sem, &timestamp)) {

        case 0:
            /* The mutex is free */
            break;

        case ETIMEDOUT:
            /* The mutex is busy */
            return false;

        default:
            /* Error */
            die_with_errno("sem_timedwait");
            return false;
        }
    } else if (sem_wait(sem) != 0) {
        die_with_errno("sem_wait");
        return false;
    }

    return true;
}

static bool
release(sem_t *sem)
{
    if (sem_post(sem) != 0) {
        die_with_errno("sem_post");
        return false;
    }
    return true;
}

static bool
masked_copy(uint8_t *dst, const uint8_t *src, size_t size, const uint8_t *mask)
{
    bool changed;

    if (mask == NULL) {
        /* Copy without mask: resolve to a memcpy() call */
        changed = memcmp(dst, src, size) != 0;
        if (changed) {
            memcpy(dst, src, size);
        }
    } else {
        /* Copy with mask */
        uint8_t *p_dst;
        const uint8_t *p_src, *p_mask;

        p_dst   = dst;
        p_src   = src;
        p_mask  = mask;
        changed = false;

        while (size --) {
            /* Transfer to dst only the bits in src specified by mask, e.g. if
             * dst is 0x12 and src is 0x34, when mask is 0x00 dst is left
             * untouched (i.e. 0x12), if mask is 0xFF dst is set to src (i.e.
             * 0x34) and if mask is 0xF0 dst is set to 0x32.
             *
             * To do this without branches in the cycle, I use the single
             * bitwise function:
             *
             *     dst = src AND mask OR dst AND NOT mask
             */
            changed = (changed || (*p_dst != *p_src));
            *p_dst = (*p_src & *p_mask) | (*p_dst & ~*p_mask);
            ++p_dst;
            ++p_src;
            ++p_mask;
        }
    }

    return changed;
}

static int
share_create(App *app, int n_zones, va_list zones)
{
    char *file;
    mode_t old;
    int shm, *sizes, n;
    size_t size, heap_size;
    Share *share;

    info("share: creating SHM object");
    file = NAME(app);
    old  = umask(0);
    shm  = shm_open(file, O_RDWR|O_CREAT|O_EXCL, 0660);
    umask(old);
    free(file);
    if (shm < 0) {
        die_with_errno("shm_open");
        return shm;
    }

    /* Collect dynamic sizes and compute the heap dimension */
    sizes     = alloca(sizeof(int) * n_zones);
    heap_size = 0;
    for (n = 0; n < n_zones; ++n) {
        sizes[n]   = va_arg(zones, int);
        heap_size += sizes[n];
    }

    /* Allocate the needed shared memory */
    size = sizeof(Header) + sizeof(Zone) * n_zones + heap_size;
    if (ftruncate(shm, size) != 0) {
        die_with_errno("ftruncate");
        return shm;
    }

    share = SHARE(app);
    share->header = MMAP(size, shm);
    if (share->header == MAP_FAILED || share->header == NULL) {
        share->header = NULL;
        die_with_errno("mmap");
        return shm;
    }
    share->header->owner   = getpid();
    share->header->n_zones = n_zones;
    zones_init(share, sizes);

    return shm;
}

static int
share_open(App *app)
{
    char *file;
    int shm;
    struct stat stats;
    Share *share;

    file = NAME(app);
    shm  = shm_open(file, O_RDWR, 0);
    free(file);
    if (shm < 0) {
        die_with_errno("shm_open");
        return shm;
    }

    if (fstat(shm, &stats) != 0) {
        die_with_errno("fstat");
        return shm;
    }

    share = SHARE(app);
    share->header = MMAP(stats.st_size, shm);
    if (share->header == MAP_FAILED || share->header == NULL) {
        share->header = NULL;
        die_with_errno("mmap");
        return shm;
    }

    zones_init(share, NULL);
    return shm;
}

/**
 * Initialize the share memory object of an application instance.
 *
 * This should be called before any other calls to app_share_...().
 *
 * The daemon handling low level I/O must be called with @p n_zones greater
 * than 0 and define every zone size in the following arguments, e.g.:
 *
 *     // Typical call from the I/O daemon
 *     app_share_init(app, 3,
 *                    sizeof(ZoneShared),
 *                    sizeof(ZoneInput),
 *                    sizeof(ZoneOutput));
 *
 * The other processes *must* call app_share_init() without zone definition
 * (leave @p n_zones set to 0): all the zones created by the daemon are
 * accessible though.
 *
 *     // Typical call from all other processes
 *     app_share_init(app, 0);
 *
 *     // Now it is possible to access those zones
 *     ZoneShared shared;
 *     ZoneInput  input;
 *     ZoneOutput output;
 *     app_share_read(app, 0, &shared, NULL, 0);
 *     app_share_read(app, 1, &input,  NULL, 0);
 *     app_share_read(app, 2, &output, NULL, 0);
 *
 * @param app    An App instance
 * @param create The creation flag
 */
void
app_share_init(App *app, int n_zones, ...)
{
    Share *share;
    int shm;

    share         = malloc(sizeof(Share));
    share->header = NULL;
    share->heaps  = NULL;
    app->share    = share;

    if (n_zones == 0) {
        shm = share_open(app);
    } else {
        va_list zones;
        va_start(zones, n_zones);
        shm = share_create(app, n_zones, zones);
        va_end(zones);
    }

    if (shm > 0) {
        /* The file can be closed after the mapping */
        close(shm);
    }
}

/**
 * Finalize the share memory object of an application instance.
 *
 * This is the counterpart of the app_share_init() method: it frees any
 * allocated resource and resets the values of every field, making this
 * function idempotent.
 *
 * @param app The instance to free
 */
void
app_share_free(App *app)
{
    Share *share = SHARE(app);
    if (share == NULL) {
        return;
    }

    if (share->header != NULL) {
        char *file;
        bool is_owner = share->header->owner == getpid();
        if (is_owner) {
            /* Free the zones before unmapping */
            zones_free(share);
        }
        munmap(share->header, app_share_size(app));
        if (is_owner) {
            info("share: destroying SHM object");
            file = NAME(app);
            shm_unlink(file);
            free(file);
        }
    }

    if (share->heaps != NULL) {
        free(share->heaps);
        share->heaps = NULL;
    }

    free(share);
    app->share = NULL;
}

/**
 * Calculate the total shared memory size needed by this instance.
 *
 * @param app The instance to free
 * @return    The requested size, in bytes
 */
size_t
app_share_size(App *app)
{
    Share  *share  = SHARE(app);
    Header *header = share->header;
    size_t  size = 0;
    if (header != NULL) {
        int n;
        Zone *zone = ZONE(share, 0);
        for (n = 0; n < header->n_zones; ++n) {
            size += zone->size + sizeof(Zone);
            ++zone;
        }
        size += sizeof(Header);
    }
    return size;
}

/**
 * Add the current process as watcher to a specific zone
 *
 * Whenever a write operation occurs, the machinery library automatically
 * notifies a list of processes (called watchers) by sending a `SIGUSR1`
 * signal to them.
 *
 * This function adds the current process to the list of watchers of the @p
 * n_zone zone. Keep in mind there is a hardcoded limit to the maximum number
 * of watchers (currently 30 per zone).
 *
 * @param app     An App instance
 * @param n_zone  Zone number (0 = first zone)
 * @return        `true` if the call succeeded, `false` on errors
 */
bool
app_share_watch(App *app, int n_zone)
{
    Zone *zone = ZONE(SHARE(app), n_zone);
    int n;

    if (! lock(&zone->lock, 0)) {
        warning("share: unable to lock during app_share_watch");
        return false;
    }

    for (n = 0; n < MACHINERY_MAX_WATCHERS; ++n) {
        if (zone->watcher[n] == 0) {
            zone->watcher[n] = getpid();
            break;
        }
    }

    release(&zone->lock);
    if (n >= MACHINERY_MAX_WATCHERS) {
        warning("share: maximum number of watchers reached (%d)", n);
        return false;
    }

    return true;
}

/**
 * Read data from a zone.
 *
 * This will automatically handle locking and unlocking of the shared memory.
 * On I/O errors, an exception will be raised with die_with_errno().
 *
 * It is possible to selectively read only some byte (leaving the others
 * untouched) by specifying a mask. For example, considering a zone of two
 * bytes, the following pseudocode will read only the second one:
 *
 *     uint8_t *mask = "\x00\xFF";
 *     app_share_read(app, zone, dst, mask, 0);
 *
 * Furthermore a specific timeout (in nanoseconds) can be set for avoiding
 * too slow reads. If unspecified (timeout == 0) the read will block until the
 * completion of the operation (possibly forever).
 *
 * @param app     An App instance
 * @param n_zone  Zone number (0 = first zone)
 * @param dst     Where to write the data
 * @param mask    A bitmask that specifies what data should be read, `NULL`
 *                for reading the whole zone
 * @param timeout Maximum allowed time (in nanoseconds), `0` for perpetually
 *                blocking read
 * @return `true` if the call succeeded, `false` otherwise
 */
bool
app_share_read(App *app, int n_zone, void *dst, const void *mask, long timeout)
{
    Share *share = SHARE(app);
    Zone *zone   = ZONE(share, n_zone);

    if (! lock(&zone->lock, timeout)) {
        warning("share: unable to lock during app_share_read");
        return false;
    }

    masked_copy(dst, share->heaps[n_zone], zone->size, mask);

    return release(&zone->lock);
}

/**
 * Write data to a zone.
 *
 * This will automatically handle locking and unlocking of the shared memory.
 * On I/O errors, an exception will be raised with die_with_errno().
 *
 * See app_share_read() for the meaning of the mask and timeout arguments.
 *
 * @param app     An App instance
 * @param n_zone  Zone number (0 = first zone)
 * @param src     Where to pick data from
 * @param mask    A bitmask that specifies what data should be written, `NULL`
 *                for writing the whole zone
 * @param timeout Maximum allowed time (in nanoseconds), `0` for perpetually
 *                blocking write
 * @return `true` if the call succeeded, `false` otherwise
 */
bool
app_share_write(App *app, int n_zone, const void *src,
                const void *mask, long timeout)
{
    Share *share = SHARE(app);
    Zone *zone = ZONE(share, n_zone);

    if (! lock(&zone->lock, timeout)) {
        warning("share: unable to lock during app_share_write");
        return false;
    }

    if (masked_copy(share->heaps[n_zone], src, zone->size, mask)) {
        /* Notify the whatchers that something has changed */
        pid_t pid;
        int n;
        for (n = 0; n < MACHINERY_MAX_WATCHERS; ++n) {
            pid = zone->watcher[n];
            if (pid > 0 && kill(pid, SIGUSR1) == -1) {
                info("share: PID %ld died (%s)",
                     (long) pid, strerror(errno));
                zone->watcher[n] = 0;
            }
        }
    }

    release(&zone->lock);
    return true;
}
