#ifndef MACHINERY_RAMP_H_
#define MACHINERY_RAMP_H_

#include "gobject.h"

#define MACHINERY_TYPE_RAMP         (machinery_ramp_get_type())
#define MACHINERY_RAMP(o)           (G_TYPE_CHECK_INSTANCE_CAST((o), MACHINERY_TYPE_RAMP, MachineryRamp))
#define MACHINERY_RAMP_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), MACHINERY_TYPE_RAMP, MachineryRampClass))
#define MACHINERY_IS_RAMP(o)        (G_TYPE_CHECK_INSTANCE_TYPE((o), MACHINERY_TYPE_RAMP))
#define MACHINERY_IS_RAMP_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), MACHINERY_TYPE_RAMP))
#define MACHINERY_RAMP_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MACHINERY_TYPE_RAMP, MachineryRampClass))


G_BEGIN_DECLS

typedef struct _MachineryRamp      MachineryRamp;
typedef struct _MachineryRampClass MachineryRampClass;

struct _MachineryRamp {
    GObject         parent;
};

struct _MachineryRampClass {
    GObjectClass    parent_class;
};


GType           machinery_ramp_get_type         (void);
MachineryRamp * machinery_ramp_new              (void);
void            machinery_ramp_set_min_speed    (MachineryRamp *ramp,
                                                 guint          speed);
guint           machinery_ramp_get_min_speed    (MachineryRamp *ramp);
void            machinery_ramp_set_max_speed    (MachineryRamp *ramp,
                                                 guint          speed);
guint           machinery_ramp_get_max_speed    (MachineryRamp *ramp);
gint            machinery_ramp_get_speed        (MachineryRamp *ramp);
void            machinery_ramp_set_acceleration (MachineryRamp *ramp,
                                                 guint          acceleration);
guint           machinery_ramp_get_acceleration (MachineryRamp *ramp);
void            machinery_ramp_set_deceleration (MachineryRamp *ramp,
                                                 guint          deceleration);
guint           machinery_ramp_get_deceleration (MachineryRamp *ramp);
void            machinery_ramp_move             (MachineryRamp *ramp,
                                                 gint32         destination);
gboolean        machinery_ramp_update_position  (MachineryRamp *ramp,
                                                 gint32         position);

G_END_DECLS

#endif /* MACHINERY_RAMP_H_ */
