/**
 * @file  ethercat-slaves.c
 * @brief Configurations for common EtherCAT slaves.
 *
 * This is a collection of configurations for some common EtherCAT
 * device. Anyone of them can be passed as `configuration` argument to
 * the app_ethercat_add_slave() function.
 */

#include "ethercat-slaves.h"
#include "ethercat-internal.h"


/* Common digital input data */

static ec_pdo_entry_info_t di_entries[] = {
    {0x6000, 0x01, 1}, /* Input */
    {0x6010, 0x01, 1}, /* Input */
    {0x6020, 0x01, 1}, /* Input */
    {0x6030, 0x01, 1}, /* Input */
    {0x6040, 0x01, 1}, /* Input */
    {0x6050, 0x01, 1}, /* Input */
    {0x6060, 0x01, 1}, /* Input */
    {0x6070, 0x01, 1}, /* Input */
    {0x6080, 0x01, 1}, /* Input */
    {0x6090, 0x01, 1}, /* Input */
    {0x60a0, 0x01, 1}, /* Input */
    {0x60b0, 0x01, 1}, /* Input */
    {0x60c0, 0x01, 1}, /* Input */
    {0x60d0, 0x01, 1}, /* Input */
    {0x60e0, 0x01, 1}, /* Input */
    {0x60f0, 0x01, 1}, /* Input */
};

static ec_pdo_info_t di_pdos[] = {
    {0x1a00, 1, di_entries + 0}, /* Channel 1 */
    {0x1a01, 1, di_entries + 1}, /* Channel 2 */
    {0x1a02, 1, di_entries + 2}, /* Channel 3 */
    {0x1a03, 1, di_entries + 3}, /* Channel 4 */
    {0x1a04, 1, di_entries + 4}, /* Channel 5 */
    {0x1a05, 1, di_entries + 5}, /* Channel 6 */
    {0x1a06, 1, di_entries + 6}, /* Channel 7 */
    {0x1a07, 1, di_entries + 7}, /* Channel 8 */
    {0x1a08, 1, di_entries + 8}, /* Channel 9 */
    {0x1a09, 1, di_entries + 9}, /* Channel 10 */
    {0x1a0a, 1, di_entries + 10}, /* Channel 11 */
    {0x1a0b, 1, di_entries + 11}, /* Channel 12 */
    {0x1a0c, 1, di_entries + 12}, /* Channel 13 */
    {0x1a0d, 1, di_entries + 13}, /* Channel 14 */
    {0x1a0e, 1, di_entries + 14}, /* Channel 15 */
    {0x1a0f, 1, di_entries + 15}, /* Channel 16 */
};


/* Common digital output data */

static ec_pdo_entry_info_t do_entries[] = {
    {0x7000, 0x01, 1}, /* Output */
    {0x7010, 0x01, 1}, /* Output */
    {0x7020, 0x01, 1}, /* Output */
    {0x7030, 0x01, 1}, /* Output */
    {0x7040, 0x01, 1}, /* Output */
    {0x7050, 0x01, 1}, /* Output */
    {0x7060, 0x01, 1}, /* Output */
    {0x7070, 0x01, 1}, /* Output */
    {0x7080, 0x01, 1}, /* Output */
    {0x7090, 0x01, 1}, /* Output */
    {0x70a0, 0x01, 1}, /* Output */
    {0x70b0, 0x01, 1}, /* Output */
    {0x70c0, 0x01, 1}, /* Output */
    {0x70d0, 0x01, 1}, /* Output */
    {0x70e0, 0x01, 1}, /* Output */
    {0x70f0, 0x01, 1}, /* Output */
};

static ec_pdo_info_t do_pdos[] = {
    {0x1600, 1, do_entries + 0}, /* Channel 1 */
    {0x1601, 1, do_entries + 1}, /* Channel 2 */
    {0x1602, 1, do_entries + 2}, /* Channel 3 */
    {0x1603, 1, do_entries + 3}, /* Channel 4 */
    {0x1604, 1, do_entries + 4}, /* Channel 5 */
    {0x1605, 1, do_entries + 5}, /* Channel 6 */
    {0x1606, 1, do_entries + 6}, /* Channel 7 */
    {0x1607, 1, do_entries + 7}, /* Channel 8 */
    {0x1608, 1, do_entries + 8}, /* Channel 9 */
    {0x1609, 1, do_entries + 9}, /* Channel 10 */
    {0x160a, 1, do_entries + 10}, /* Channel 11 */
    {0x160b, 1, do_entries + 11}, /* Channel 12 */
    {0x160c, 1, do_entries + 12}, /* Channel 13 */
    {0x160d, 1, do_entries + 13}, /* Channel 14 */
    {0x160e, 1, do_entries + 14}, /* Channel 15 */
    {0x160f, 1, do_entries + 15}, /* Channel 16 */
};


/* Common analog input data */

static ec_pdo_entry_info_t ai_entries[] = {
    {0x6000,  1,  1}, /* Under range */
    {0x6000,  2,  1}, /* Over range */
    {0x6000,  3,  2}, /* Limit1 */
    {0x6000,  5,  2}, /* Limit2 */
    {0x6000,  7,  1}, /* Over or under range */
    {0x0000,  0,  1}, /* Padding */
    {0x0000,  0,  5}, /* Padding */
    {0x6000, 14,  1}, /* Sync error */
    {0x6000, 15,  1}, /* TxPDO State */
    {0x6000, 16,  1}, /* TxPDO Toggle */
    {0x6000, 17, 16}, /* Value */

    {0x6010,  1,  1}, /* Under range */
    {0x6010,  2,  1}, /* Over range */
    {0x6010,  3,  2}, /* Limit1 */
    {0x6010,  5,  2}, /* Limit2 */
    {0x6010,  7,  1}, /* Over or under range */
    {0x0000,  0,  1}, /* Padding */
    {0x0000,  0,  5}, /* Padding */
    {0x6010, 14,  1}, /* Sync error */
    {0x6010, 15,  1}, /* TxPDO State */
    {0x6010, 16,  1}, /* TxPDO Toggle */
    {0x6010, 17, 16}, /* Value */

    {0x6020,  1,  1}, /* Under range */
    {0x6020,  2,  1}, /* Over range */
    {0x6020,  3,  2}, /* Limit1 */
    {0x6020,  5,  2}, /* Limit2 */
    {0x6020,  7,  1}, /* Over or under range */
    {0x0000,  0,  1}, /* Padding */
    {0x0000,  0,  5}, /* Padding */
    {0x6020, 14,  1}, /* Sync error */
    {0x6020, 15,  1}, /* TxPDO State */
    {0x6020, 16,  1}, /* TxPDO Toggle */
    {0x6020, 17, 16}, /* Value */

    {0x6030,  1,  1}, /* Under range */
    {0x6030,  2,  1}, /* Over range */
    {0x6030,  3,  2}, /* Limit1 */
    {0x6030,  5,  2}, /* Limit2 */
    {0x6030,  7,  1}, /* Over or under range */
    {0x0000,  0,  1}, /* Padding */
    {0x0000,  0,  5}, /* Padding */
    {0x6030, 14,  1}, /* Sync error */
    {0x6030, 15,  1}, /* TxPDO State */
    {0x6030, 16,  1}, /* TxPDO Toggle */
    {0x6030, 17, 16}, /* Value */
};

static ec_pdo_info_t ai_pdos[] = {
    {0x1a00, 11, ai_entries +  0}, /* AI Standard Channel 1 */
    {0x1a02, 11, ai_entries + 11}, /* AI Standard Channel 2 */
    {0x1a04, 11, ai_entries + 22}, /* AI Standard Channel 3 */
    {0x1a06, 11, ai_entries + 33}, /* AI Standard Channel 4 */
};


/* Common analog output data */

static ec_pdo_entry_info_t ao_entries[] = {
    {0x7000, 1, 16}, /* Analog output */
    {0x7010, 1, 16}, /* Analog output */
    {0x7020, 1, 16}, /* Analog output */
    {0x7030, 1, 16}, /* Analog output */
};

static ec_pdo_info_t ao_pdos[] = {
    {0x1600, 1, ao_entries + 0}, /* AO Standard Channel 1 */
    {0x1601, 1, ao_entries + 1}, /* AO Standard Channel 2 */
    {0x1602, 1, ao_entries + 2}, /* AO Standard Channel 3 */
    {0x1603, 1, ao_entries + 3}, /* AO Standard Channel 4 */
};


/**
 * EtherCAT coupler.
 *
 * This slave does not have any sync-manager.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_ek1100()
{
    const static MachinerySlaveConfig config = {
        "EK1100", "EtherCAT bus coupler",
        0x00000002, /* Vendor ID */
        0x044c2c52, /* Product code */
        0x00000000, /* Revision number */
        NULL,
        0
    };
    return &config;
}

/**
 * EtherCAT extension.
 *
 * This slave does not have any sync-manager.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_ek1110()
{
    const static MachinerySlaveConfig config = {
        "EK1110", "EtherCAT extension",
        0x00000002, /* Vendor ID */
        0x04562c52, /* Product code */
        0x00000000, /* Revision number */
        NULL,
        0
    };
    return &config;
}

/**
 * 8 digital inputs.
 *
 * This slave has one sync-manager (index 0) with 8 read bits
 * (for a total of 1 byte).
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el1808()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_INPUT, 8, di_pdos + 0, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL1808", "8 digital inputs module",
        0x00000002, /* Vendor ID */
        0x07103052, /* Product code */
        0x00120000, /* Revision number */
        syncs, 1
    };
    return &config;
}

/**
 * 16 digital inputs.
 *
 * This slave has one sync-manager (index 0) with 16 read-only bits
 * (for a total of 2 bytes).
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el1809()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_INPUT, 16, di_pdos + 0, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL1809", "16 digital inputs module",
        0x00000002, /* Vendor ID */
        0x07113052, /* Product code */
        0x00120000, /* Revision number */
        syncs, 1
    };
    return &config;
}

/**
 * 8 digital outputs.
 *
 * This slave has one sync-manager (index 0) with 8 read-write bits
 * (for a total of 1 byte).
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el2808()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 8, do_pdos + 0, EC_WD_ENABLE},
        {0xff}
    };
    static MachinerySlaveConfig config = {
        "EL2808", "8 digital outputs module",
        0x00000002, /* Vendor ID */
        0x0af83052, /* Product code */
        0x00120000, /* Revision number */
        syncs, 1
    };
    return &config;
}

/**
 * 16 digital outputs.
 *
 * This slave has two sync-managers (index 0 and 1) with 8 read-write
 * bits each (for a total of 2 bytes).
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el2809()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 8, do_pdos + 0, EC_WD_ENABLE},
        {1, EC_DIR_OUTPUT, 8, do_pdos + 8, EC_WD_ENABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL2809", "16 digital outputs module",
        0x00000002, /* Vendor ID */
        0x0af93052, /* Product code */
        0x00120000, /* Revision number */
        syncs, 2
    };
    return &config;
}

/**
 * 4 analog inputs.
 *
 * This slave has 4 sync-managers. The first three do not have image
 * data, the forth one (index 3) has a read-only image of 16 bytes.
 *
 * ```
 * // Sync-manager at index 3 (16 bytes read-only)
 * unsigned    under_range_1 : 1;
 * unsigned    over_range_1 : 1;
 * unsigned    limit1_1 : 2;
 * unsigned    limit2_1 : 2;
 * unsigned    over_or_under_range_1 : 1;
 * unsigned    :6;
 * unsigned    sync_error_1 : 1;
 * unsigned    txpdo_state_1 : 1;
 * unsigned    txpdo_toggle_1 : 1;
 * unsigned    value_1 : 16;
 * unsigned    under_range_2 : 1;
 * unsigned    over_range_2 : 1;
 * unsigned    limit1_2 : 2;
 * unsigned    limit2_2 : 2;
 * unsigned    over_or_under_range_2 : 1;
 * unsigned    :6;
 * unsigned    sync_error_2 : 1;
 * unsigned    txpdo_state_2 : 1;
 * unsigned    txpdo_toggle_2 : 1;
 * unsigned    value_2 : 16;
 * unsigned    under_range_3 : 1;
 * unsigned    over_range_3 : 1;
 * unsigned    limit1_3 : 2;
 * unsigned    limit2_3 : 2;
 * unsigned    over_or_under_range_3 : 1;
 * unsigned    :6;
 * unsigned    sync_error_3 : 1;
 * unsigned    txpdo_state_3 : 1;
 * unsigned    txpdo_toggle_3 : 1;
 * unsigned    value_3 : 16;
 * unsigned    under_range_4 : 1;
 * unsigned    over_range_4 : 1;
 * unsigned    limit1_4 : 2;
 * unsigned    limit2_4 : 2;
 * unsigned    over_or_under_range_4 : 1;
 * unsigned    :6;
 * unsigned    sync_error_4 : 1;
 * unsigned    txpdo_state_4 : 1;
 * unsigned    txpdo_toggle_4 : 1;
 * unsigned    value_4 : 16;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el3164()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 4, ai_pdos, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL3164", "4 analog inputs module 16 bits 0..10V",
        0x00000002, /* Vendor ID */
        0x0c5c3052, /* Product code */
        0x00140000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * 4 analog outputs.
 *
 * This slave has 4 sync-managers. The first two and the last one
 * do not have image data, the third one (index 2) has a read-write
 * image of 8 bytes.
 *
 * ```
 * // Sync-manager at index 2 (16 bytes read-only)
 * unsigned    value_1 : 16;
 * unsigned    value_2 : 16;
 * unsigned    value_3 : 16;
 * unsigned    value_4 : 16;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el4104()
{
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 4, ao_pdos, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL4104", "4 analog outputs module 16 bits 0..10V",
        0x00000002, /* Vendor ID */
        0x10083052, /* Product code */
        0x03fc0000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * Stepper driver in position mode without encoder.
 *
 * This slave has 4 sync-managers. The first two do not have image data,
 * the third one (index 2) has a read-write image of 16 bytes and the
 * forth one (index 3) has a read-only image of 14 bytes.
 *
 * ```
 * // Sync-manager at index 2 (16 bytes read-write)
 * unsigned    stm_enable : 1;
 * unsigned    stm_reset : 1;
 * unsigned    stm_reduce_torque : 1;
 * unsigned    : 8;
 * unsigned    stm_do1 : 1;
 * unsigned    : 4;
 * unsigned    pos_execute : 1;
 * unsigned    pos_emergency_stop : 1;
 * unsigned    : 14;
 * unsigned    pos_target : 32;
 * unsigned    pos_velocity : 16;
 * unsigned    pos_start_type : 16;
 * unsigned    pos_acceleration : 16;
 * unsigned    pos_deceleration : 16;
 *
 * // Sync-manager at index 3 (14 bytes read-only)
 * unsigned    stm_ready_to_enable : 1;
 * unsigned    stm_ready : 1;
 * unsigned    stm_warning : 1;
 * unsigned    stm_error : 1;
 * unsigned    stm_forward : 1;
 * unsigned    stm_backward : 1;
 * unsigned    stm_torque_reduced : 1;
 * unsigned    stm_stall : 1;
 * unsigned    : 3;
 * unsigned    stm_di1 : 1;
 * unsigned    stm_di2 : 1;
 * unsigned    stm_sync_error : 1;
 * unsigned    : 1;
 * unsigned    stm_pdo_toggle : 1;
 * unsigned    pos_busy : 1;
 * unsigned    pos_in_target : 1;
 * unsigned    pos_warning : 1;
 * unsigned    pos_error : 1;
 * unsigned    pos_calibrated : 1;
 * unsigned    pos_accelerate : 1;
 * unsigned    pos_decelerate : 1;
 * unsigned    pos_ready_to_execute : 1;
 * unsigned    : 8;
 * unsigned    pos_actual_position : 32;
 * unsigned    pos_actual_velocity : 16;
 * unsigned    pos_drive_time : 32;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el7047p()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1602, 6, entries + 0 */
        {0x7010, 0x01, 1}, /* Enable */
        {0x7010, 0x02, 1}, /* Reset */
        {0x7010, 0x03, 1}, /* Reduce torque */
        {0x0000, 0x00, 8}, /* Gap */
        {0x7010, 0x0c, 1}, /* Digital output 1 */
        {0x0000, 0x00, 4}, /* Gap */

        /* 0x1606, 8, entries + 6 */
        {0x7020, 0x01, 1}, /* Execute */
        {0x7020, 0x02, 1}, /* Emergency stop */
        {0x0000, 0x00, 14}, /* Gap */
        {0x7020, 0x11, 32}, /* Target position */
        {0x7020, 0x21, 16}, /* Velocity */
        {0x7020, 0x22, 16}, /* Start type */
        {0x7020, 0x23, 16}, /* Acceleration */
        {0x7020, 0x24, 16}, /* Deceleration */

        /* 0x1a03, 14, entries + 14 */
        {0x6010, 0x01, 1}, /* Ready to enable */
        {0x6010, 0x02, 1}, /* Ready */
        {0x6010, 0x03, 1}, /* Warning */
        {0x6010, 0x04, 1}, /* Error */
        {0x6010, 0x05, 1}, /* Moving positive */
        {0x6010, 0x06, 1}, /* Moving negative */
        {0x6010, 0x07, 1}, /* Torque reduced */
        {0x6010, 0x08, 1}, /* Motor stall */
        {0x0000, 0x00, 3}, /* Gap */
        {0x6010, 0x0c, 1}, /* Digital input 1 */
        {0x6010, 0x0d, 1}, /* Digital input 2 */
        {0x6010, 0x0e, 1}, /* Sync error */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6010, 0x10, 1}, /* TxPDO Toggle */

        /* 0x1a07, 12, entries + 28 */
        {0x6020, 0x01, 1}, /* Busy */
        {0x6020, 0x02, 1}, /* In target */
        {0x6020, 0x03, 1}, /* Warning */
        {0x6020, 0x04, 1}, /* Error */
        {0x6020, 0x05, 1}, /* Calibrated */
        {0x6020, 0x06, 1}, /* Accelerate */
        {0x6020, 0x07, 1}, /* Decelerate */
        {0x6020, 0x08, 1}, /* Ready to execute */
        {0x0000, 0x00, 8}, /* Gap */
        {0x6020, 0x11, 32}, /* Actual position */
        {0x6020, 0x21, 16}, /* Actual velocity */
        {0x6020, 0x22, 32}, /* Actual drive time */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1602, 6, entries + 0}, /* STM RxPDO-Map Control */
        {0x1606, 8, entries + 6}, /* POS RxPDO-Map Control */
        {0x1a03, 14, entries + 14}, /* STM TxPDO-Map Status */
        {0x1a07, 12, entries + 28}, /* POS TxPDO-Map Status */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 2, pdos + 0, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 2, pdos + 2, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL7047", "Stepper driver in position mode without encoder",
        0x00000002, /* Vendor ID */
        0x1b873052, /* Product code */
        0x00150000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * Beckhoff stepper driver (positioning interface mapping).
 *
 * This slave has 4 sync-managers. The first two do not have image data,
 * the third one (index 2) has a read-write image of 22 bytes and the
 * forth one (index 3) has a read-only image of 24 bytes.
 *
 * ```
 * // Sync-manager at index 2 (22 bytes read-write)
 * unsigned    latch_c : 1;
 * unsigned    latch_extern_rising : 1;
 * unsigned    set_counter : 1;
 * unsigned    latch_extern_falling : 1;
 * unsigned    : 12;
 * unsigned    counter_value : 32;
 * unsigned    stm_enable : 1;
 * unsigned    stm_reset : 1;
 * unsigned    stm_reduce_torque : 1;
 * unsigned    : 8;
 * unsigned    stm_do1 : 1;
 * unsigned    : 4;
 * unsigned    pos_execute : 1;
 * unsigned    pos_emergency_stop : 1;
 * unsigned    : 14;
 * unsigned    pos_target : 32;
 * unsigned    pos_velocity : 16;
 * unsigned    pos_start_type : 16;
 * unsigned    pos_acceleration : 16;
 * unsigned    pos_deceleration : 16;
 *
 * // Sync-manager at index 3 (24 bytes read-only)
 * unsigned    latch_c_valid : 1;
 * unsigned    latch_extern_valid : 1;
 * unsigned    set_counter_done : 1;
 * unsigned    counter_underflow : 1;
 * unsigned    counter_overflow : 1;
 * unsigned    : 2;
 * unsigned    extrapolation_stall : 1;
 * unsigned    input_a : 1;
 * unsigned    input_b : 1;
 * unsigned    input_c : 1;
 * unsigned    : 1;
 * unsigned    external_latch : 1;
 * unsigned    sync_error : 1;
 * unsigned    : 1;
 * unsigned    txpdo_toggle : 1;
 * unsigned    counter_value : 32;
 * unsigned    latch_value : 32;
 * unsigned    stm_ready_to_enable : 1;
 * unsigned    stm_ready : 1;
 * unsigned    stm_warning : 1;
 * unsigned    stm_error : 1;
 * unsigned    stm_forward : 1;
 * unsigned    stm_backward : 1;
 * unsigned    stm_torque_reduced : 1;
 * unsigned    stm_stall : 1;
 * unsigned    : 3;
 * unsigned    stm_di1 : 1;
 * unsigned    stm_di2 : 1;
 * unsigned    stm_sync_error : 1;
 * unsigned    : 1;
 * unsigned    stm_pdo_toggle : 1;
 * unsigned    pos_busy : 1;
 * unsigned    pos_in_target : 1;
 * unsigned    pos_warning : 1;
 * unsigned    pos_error : 1;
 * unsigned    pos_calibrated : 1;
 * unsigned    pos_accelerate : 1;
 * unsigned    pos_decelerate : 1;
 * unsigned    pos_ready_to_execute : 1;
 * unsigned    : 8;
 * unsigned    pos_actual_position : 32;
 * unsigned    pos_actual_velocity : 16;
 * unsigned    pos_drive_time : 32;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_el7047pi()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1601, 6, entries + 0 (48 bits) */
        {0x7000, 0x01, 1},  /* Enable latch C */
        {0x7000, 0x02, 1},  /* Enable latch extern on positive edge */
        {0x7000, 0x03, 1},  /* Set counter */
        {0x7000, 0x04, 1},  /* Enable latch extern on negative edge */
        {0x0000, 0x00, 12}, /* Gap */
        {0x7000, 0x11, 32}, /* Set counter value */

        /* 0x1602, 6, entries + 6 (16 bits) */
        {0x7010, 0x01, 1},  /* Enable */
        {0x7010, 0x02, 1},  /* Reset */
        {0x7010, 0x03, 1},  /* Reduce torque */
        {0x0000, 0x00, 8},  /* Gap */
        {0x7010, 0x0c, 1},  /* Digital output 1 */
        {0x0000, 0x00, 4},  /* Gap */

        /* 0x1606, 8, entries + 12 (112 bits) */
        {0x7020, 0x01, 1},  /* Execute */
        {0x7020, 0x02, 1},  /* Emergency stop */
        {0x0000, 0x00, 14}, /* Gap */
        {0x7020, 0x11, 32}, /* Target position */
        {0x7020, 0x21, 16}, /* Velocity */
        {0x7020, 0x22, 16}, /* Start type */
        {0x7020, 0x23, 16}, /* Acceleration */
        {0x7020, 0x24, 16}, /* Deceleration */

        /* 0x1a01, 17, entries + 20 (80 bits) */
        {0x6000, 0x01, 1},  /* Latch C valid */
        {0x6000, 0x02, 1},  /* Latch extern valid */
        {0x6000, 0x03, 1},  /* Set counter done */
        {0x6000, 0x04, 1},  /* Counter underflow */
        {0x6000, 0x05, 1},  /* Counter overflow */
        {0x0000, 0x00, 2},  /* Gap */
        {0x6000, 0x08, 1},  /* Extrapolation stall */
        {0x6000, 0x09, 1},  /* Status of input A */
        {0x6000, 0x0a, 1},  /* Status of input B */
        {0x6000, 0x0b, 1},  /* Status of input C */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6000, 0x0d, 1},  /* Status of external latch */
        {0x6000, 0x0e, 1},  /* Sync error */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6000, 0x10, 1},  /* TxPDO toggle */
        {0x6000, 0x11, 32}, /* Counter value */
        {0x6000, 0x12, 32}, /* Latch value */

        /* 0x1a03, 14, entries + 37 (16 bits) */
        {0x6010, 0x01, 1}, /* Ready to enable */
        {0x6010, 0x02, 1}, /* Ready */
        {0x6010, 0x03, 1}, /* Warning */
        {0x6010, 0x04, 1}, /* Error */
        {0x6010, 0x05, 1}, /* Moving positive */
        {0x6010, 0x06, 1}, /* Moving negative */
        {0x6010, 0x07, 1}, /* Torque reduced */
        {0x6010, 0x08, 1}, /* Motor stall */
        {0x0000, 0x00, 3}, /* Gap */
        {0x6010, 0x0c, 1}, /* Digital input 1 */
        {0x6010, 0x0d, 1}, /* Digital input 2 */
        {0x6010, 0x0e, 1}, /* Sync error */
        {0x0000, 0x00, 1}, /* Gap */
        {0x6010, 0x10, 1}, /* TxPDO Toggle */

        /* 0x1a07, 12, entries + 51 (96 bits) */
        {0x6020, 0x01, 1}, /* Busy */
        {0x6020, 0x02, 1}, /* In target */
        {0x6020, 0x03, 1}, /* Warning */
        {0x6020, 0x04, 1}, /* Error */
        {0x6020, 0x05, 1}, /* Calibrated */
        {0x6020, 0x06, 1}, /* Accelerate */
        {0x6020, 0x07, 1}, /* Decelerate */
        {0x6020, 0x08, 1}, /* Ready to execute */
        {0x0000, 0x00, 8}, /* Gap */
        {0x6020, 0x11, 32}, /* Actual position */
        {0x6020, 0x21, 16}, /* Actual velocity */
        {0x6020, 0x22, 32}, /* Actual drive time */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1601, 6, entries + 0}, /* ENC RxPDO-Map Control */
        {0x1602, 6, entries + 6}, /* STM RxPDO-Map Control */
        {0x1606, 8, entries + 12}, /* POS RxPDO-Map Control */
        {0x1a01, 17, entries + 20}, /* ENC TxPDO-Map Status */
        {0x1a03, 14, entries + 37}, /* STM TxPDO-Map Status */
        {0x1a07, 12, entries + 51}, /* POS TxPDO-Map Status */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 3, pdos + 0, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 3, pdos + 3, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "EL7047", "Stepper driver (positioning interface mapping)",
        0x00000002, /* Vendor ID */
        0x1b873052, /* Product code */
        0x00150000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * HDT brushless driver in profile position mode.
 *
 * This slave is a CiA402 compliant drive and it has 4 sync-managers.
 * The first two do not have image data, the third one (index 2) has a
 * read-write image of 16 bytes and the forth one (index 3) has a
 * read-only image of 12 bytes.
 *
 * ```
 * // Sync-manager at index 2 (16 bytes read-write)
 * unsigned     control_word : 16;
 * int          target_velocity : 16;
 * int          home_offset : 32;
 * int          target_position : 32;
 * unsigned     profile_velocity : 32;
 *
 * // Sync-manager at index 3 (12 bytes read-only)
 * unsigned     status_word: 16;
 * int          mode_of_operation : 8;
 * unsigned     error_register : 8;
 * int          position_demand_value : 32;
 * int          position_actual_value : 32;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_hdt_pp()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1600, 3, entries + 0 */
        {0x6040, 0x00, 16}, /* Control word */
        {0x6042, 0x00, 16}, /* Target velocity */
        {0x607c, 0x00, 32}, /* Home offset */

        /* 0x1601, 2, entries + 3 */
        {0x607a, 0x00, 32}, /* Target position */
        {0x6081, 0x00, 32}, /* Profile velocity */

        /* 0x1A00, 3, entries + 5 */
        {0x6041, 0x00, 16}, /* Status word */
        {0x6061, 0x00, 8},  /* Modes of operation display */
        {0x1001, 0x00, 8},  /* Error register */

        /* 0x1A01, 3, entries + 8 */
        {0x6062, 0x00, 32}, /* Position demand value */
        {0x6064, 0x00, 32}, /* Position actual value */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1600, 3, entries + 0}, /* RxPDO1 */
        {0x1601, 2, entries + 3}, /* RxPDO2 */
        {0x1a00, 3, entries + 5}, /* TxPDO1 */
        {0x1a01, 2, entries + 8}, /* TxPDO2 */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 2, pdos + 0, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 2, pdos + 2, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "HDT TOMCAT", "CiA402 brushless driver (profile position mode)",
        0x00000289, /* Vendor ID */
        0x000c0465, /* Product code */
        0x00000000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * HDT brushless driver with default mappings.
 *
 * This slave is a CiA402 compliant drive and it has 4 sync-managers.
 * The first two do not have image data, the third one (index 2) has a
 * read-write image of 24 bytes and the forth one (index 3) has a
 * read-only image of 20 bytes.
 *
 * ```
 * // Sync-manager at index 2 (24 bytes read-write)
 * unsigned     control_word : 16;
 * int          target_velocity : 16;
 * int          home_offset : 32;
 * int          target_position : 32;
 * unsigned     profile_velocity : 32;
 * unsigned     profile_acceleration : 32;
 * unsigned     profile_deceleration : 32;
 * int          interpolated_position : 32;
 *
 * // Sync-manager at index 3 (20 bytes read-only)
 * unsigned     status_word: 16;
 * int          mode_of_operation : 8;
 * unsigned     error_register : 8;
 * int          position_demand_value : 32;
 * int          position_actual_value : 32;
 * int          velocity_demand : 16;
 * int          control_effort : 16;
 * unsigned     limit_cw : 1;
 * unsigned     limit_ccw : 1;
 * unsigned     home : 1;
 * unsigned     :13;
 * unsigned     di0 : 1;
 * unsigned     di1 : 1;
 * unsigned     limit_cw_copy : 1;
 * unsigned     limit_ccw_copy : 1;
 * unsigned     home_copy : 1;
 * unsigned     di5 : 1;
 * unsigned     di6 : 1;
 * unsigned     di7 : 1;
 * unsigned     :8;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_hdt_full()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1600, 3, entries + 0 */
        {0x6040, 0x00, 16}, /* Control word */
        {0x6042, 0x00, 16}, /* Target velocity */
        {0x607c, 0x00, 32}, /* Home offset */

        /* 0x1601, 2, entries + 3 */
        {0x607a, 0x00, 32}, /* Target position */
        {0x6081, 0x00, 32}, /* Profile velocity */

        /* 0x1602, 2, entries + 5 */
        {0x6083, 0x00, 32}, /* Profile acceleration */
        {0x6084, 0x00, 32}, /* Profile deceleration */

        /* 0x1603, 1, entries + 7 */
        {0x60C1, 0x01, 32}, /* Interpolated data (position) */

        /* 0x1A00, 3, entries + 8 */
        {0x6041, 0x00, 16}, /* Status word */
        {0x6061, 0x00, 8},  /* Modes of operation display */
        {0x1001, 0x00, 8},  /* Error register */

        /* 0x1A01, 2, entries + 11 */
        {0x6062, 0x00, 32}, /* Position demand value */
        {0x6064, 0x00, 32}, /* Position actual value */

        /* 0x1A02, 2, entries + 13 */
        {0x6043, 0x00, 16}, /* Velocity demand */
        {0x6044, 0x00, 16}, /* Control effort */

        /* 0x1A02, 1, entries + 15 */
        {0x60FD, 0x00, 32}, /* Digital inputs */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1600, 3, entries + 0},  /* RxPDO1 */
        {0x1601, 2, entries + 3},  /* RxPDO2 */
        {0x1602, 2, entries + 5},  /* RxPDO3 */
        {0x1603, 1, entries + 7},  /* RxPDO4 */
        {0x1a00, 3, entries + 8},  /* TxPDO1 */
        {0x1a01, 2, entries + 11}, /* TxPDO2 */
        {0x1a02, 2, entries + 13}, /* TxPDO3 */
        {0x1a03, 1, entries + 15}, /* TxPDO4 */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 4, pdos + 0, EC_WD_ENABLE},
        {3, EC_DIR_INPUT, 4, pdos + 4, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "HDT TOMCAT", "CiA402 brushless driver",
        0x00000289, /* Vendor ID */
        0x000c0465, /* Product code */
        0x00000000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * HDT NTT brushless driver in profile position mode.
 *
 * Basically the same configuration of machinery_ethercat_slave_hdt_pp()
 * for NTT drivers.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_ntt_pp()
{
    const MachinerySlaveConfig *hdt = machinery_ethercat_slave_hdt_pp();
    static MachinerySlaveConfig config = {
        "HDT NTT", "CiA402 brushless driver (profile position mode)",
        0x00000289, /* Vendor ID */
        0x000c0442, /* Product code */
        0x00000000, /* Revision number */
        NULL, 0
    };
    config.syncs = hdt->syncs;
    config.nsyncs = hdt->nsyncs;
    return &config;
}

/**
 * HDT NTT brushless driver with default mappings.
 *
 * Basically the same configuration of machinery_ethercat_slave_hdt_full()
 * for NTT drivers.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_ntt_full()
{
    const MachinerySlaveConfig *hdt = machinery_ethercat_slave_hdt_full();
    static MachinerySlaveConfig config = {
        "HDT NTT", "CiA402 brushless driver (profile position mode)",
        0x00000289, /* Vendor ID */
        0x000c0442, /* Product code */
        0x00000000, /* Revision number */
        NULL, 0
    };
    config.syncs = hdt->syncs;
    config.nsyncs = hdt->nsyncs;
    return &config;
}

/**
 * Schneider electric frequency inverter in velocity mode.
 *
 * This slave is a CiA402 compliant drive and it has 4 sync-managers.
 * The first two do not have image data, the third one (index 2) has a
 * read-write image of 4 bytes and the forth one (index 3) has a
 * read-only image of 4 bytes.
 *
 * By default, only TxPDO1 and RxPDO1 are used.
 *
 * WARNING: for some reason, this driver does not implement acceleration
 * and deceleration according to CiA402. Instead it has two custom
 * entries, accessible in RW mode (0x203C:02 and 0x203C:03
 * respectively), that specify a "ramp time" (in 0,1 s units) as a 16
 * bit value. By default both are set to `30`, meaning the ramp time
 * needed for accelerating and decelerating is 3 seconds.
 *
 * Also, the setting of the mode of operation (0x6060:00) fails, most
 * likely because "velocity" is the only mode supported by this driver.
 * This failure is expected and harmless.
 *
 * ```
 * // Sync-manager at index 2 (4 bytes read-write)
 * unsigned     control_word : 16;
 * int          target_velocity : 16;
 *
 * // Sync-manager at index 3 (4 bytes read-only)
 * unsigned     status_word: 16;
 * int          actual_velocity : 16;
 * ```
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_atv320_vm()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1600, 2, entries + 0 */
        {0x6040, 0x00, 16}, /* Control word */
        {0x6042, 0x00, 16}, /* Target velocity */

        /* 0x1A00, 2, entries + 2 */
        {0x6041, 0x00, 16}, /* Status word */
        {0x6044, 0x00, 16}, /* Actual velocity */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1600, 2, entries + 0}, /* RxPDO1 */
        {0x1a00, 2, entries + 2}, /* TxPDO1 */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 1, pdos + 0, EC_WD_ENABLE},
        {3, EC_DIR_INPUT, 1, pdos + 1, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "ATV320", "CiA402 frequency inverter (velocity mode)",
        0x0800005a, /* Vendor ID */
        0x00000389, /* Product code */
        0x0001000e, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * LAM technologies DMS integrated stepper motor.
 *
 * This slave has 4 sync-managers. The first two do not have image data
 * while the third one (index 2) has a read-write image of 6 bytes and
 * the forth one (index 3) has a read-only image of 6 bytes.
 *
 * ```
 * // Sync-manager at index 2 (6 bytes read-write)
 * unsigned     control_word : 16;
 * int          target_position : 32;
 *
 * // Sync-manager at index 3 (6 bytes read-only)
 * unsigned     status_word: 16;
 * int          position_actual_value : 32;
 * ```
 *
 * For unknown reasons, the homing method 35 is not supported and the
 * method 37 (which should be equivalent) does not reset the position.
 *
 * This slave seems to not support the SDO information service so, when
 * uploading or downloading with the `ethercat` command, the `--type`
 * argument is mandatory.
 *
 * The homing acceleration (object 0x609A) is not implemented.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_dms()
{
    static ec_pdo_entry_info_t entries[] = {
        /* 0x1601, 2, entries + 0 */
        {0x6040, 0x00, 16}, /* Control word */
        {0x607a, 0x00, 32}, /* Target position */
        {0x6041, 0x00, 16}, /* Status word */
        {0x6064, 0x00, 32}, /* Position actual value */
    };
    static ec_pdo_info_t pdos[] = {
        {0x1601, 2, entries + 0}, /* RxPDO1 */
        {0x1a01, 2, entries + 2}, /* TxPDO1 */
    };
    static ec_sync_info_t syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 1, pdos + 0, EC_WD_ENABLE},
        {3, EC_DIR_INPUT, 1, pdos + 1, EC_WD_DISABLE},
        {0xff}
    };
    const static MachinerySlaveConfig config = {
        "DMS72E4350", "LAM technologies integrated stepper-motor",
        0x0000030c, /* Vendor ID */
        0x20724510, /* Product code */
        0x01000000, /* Revision number */
        syncs, 4
    };
    return &config;
}

/**
 * LAM technologies dms integrated stepper motor.
 *
 * Same as machinery_ethercat_slave_dms() but with a different product
 * code.
 *
 * @return Configuration to pass to app_ethercat_add_slave()
 */
const void *
machinery_ethercat_slave_dms4331()
{
    const MachinerySlaveConfig *dms = machinery_ethercat_slave_dms();
    static MachinerySlaveConfig config = {
        "DMS72E4331", "LAM technologies integrated stepper-motor",
        0x0000030c, /* Vendor ID */
        0x20724310, /* Product code */
        0x01000000, /* Revision number */
        NULL, 0
    };
    config.syncs = dms->syncs;
    config.nsyncs = dms->nsyncs;
    return &config;
}
