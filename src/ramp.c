#include "ramp.h"

#include <math.h>


typedef struct {
    guint   min_speed, max_speed;
    guint   acceleration, deceleration;
    gint32  destination;
    gint    speed;
    gint32  position;
    gint64  timestamp;
    guint   max_exit_speed;
} MachineryRampPrivate;


G_DEFINE_TYPE_WITH_CODE(MachineryRamp, machinery_ramp, G_TYPE_OBJECT,
                        G_ADD_PRIVATE(MachineryRamp));

enum {
    PROP_0,
    PROP_MIN_SPEED,
    PROP_MAX_SPEED,
    PROP_ACCELERATION,
    PROP_DECELERATION,
    NUM_PROPERTIES,
};


static GParamSpec *props[NUM_PROPERTIES] = { NULL, };


static void
update_max_stop_speed(MachineryRampPrivate *data)
{
    /* Recalculate the maximum exit speed, i.e. if the speed
     * is below this threshold, it can be safely set to 0 */
    data->max_exit_speed = data->min_speed * 0.95 +
                           data->max_speed * 0.05 +
                           sqrt(2.0 * data->deceleration);
}

static void
set_min_speed(MachineryRamp *ramp, guint speed)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    data->min_speed = speed;
    update_max_stop_speed(data);
}

static guint
get_min_speed(MachineryRamp *ramp)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    return data->min_speed;
}

static void
set_max_speed(MachineryRamp *ramp, guint speed)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    data->max_speed = speed;
    update_max_stop_speed(data);
}

static guint
get_max_speed(MachineryRamp *ramp)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    return data->max_speed;
}

static void
set_acceleration(MachineryRamp *ramp, guint acceleration)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    data->acceleration = acceleration;
}

static guint
get_acceleration(MachineryRamp *ramp)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    return data->acceleration;
}

static void
set_deceleration(MachineryRamp *ramp, guint deceleration)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    data->deceleration = deceleration;
    update_max_stop_speed(data);
}

static guint
get_deceleration(MachineryRamp *ramp)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    return data->deceleration;
}

static void
get_property(GObject *object, guint prop_id,
             GValue *value, GParamSpec *pspec)
{
    MachineryRamp *ramp = (MachineryRamp *) object;

    switch (prop_id) {

    case PROP_MIN_SPEED:
        g_value_set_uint(value, get_min_speed(ramp));
        break;

    case PROP_MAX_SPEED:
        g_value_set_uint(value, get_max_speed(ramp));
        break;

    case PROP_ACCELERATION:
        g_value_set_uint(value, get_acceleration(ramp));
        break;

    case PROP_DECELERATION:
        g_value_set_uint(value, get_deceleration(ramp));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
set_property(GObject *object, guint prop_id,
             const GValue *value, GParamSpec *pspec)
{
    MachineryRamp *ramp = (MachineryRamp *) object;

    switch (prop_id) {

    case PROP_MIN_SPEED:
        set_min_speed(ramp, g_value_get_uint(value));
        break;

    case PROP_MAX_SPEED:
        set_max_speed(ramp, g_value_get_uint(value));
        break;

    case PROP_ACCELERATION:
        set_acceleration(ramp, g_value_get_uint(value));
        break;

    case PROP_DECELERATION:
        set_deceleration(ramp, g_value_get_uint(value));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}

static void
machinery_ramp_class_init(MachineryRampClass *ramp_class)
{
    GObjectClass *gobject_class;

    gobject_class = (GObjectClass *) ramp_class;
    gobject_class->get_property = get_property;
    gobject_class->set_property = set_property;

    props[PROP_MIN_SPEED] =
        g_param_spec_uint("min-speed", P_("Minimum Speed"),
                          P_("Minimum speed to use on ramp start and stop"),
                          0, G_MAXUINT, 0, G_PARAM_WRITABLE);
    props[PROP_MAX_SPEED] =
        g_param_spec_uint("max-speed", P_("Maximum Speed"),
                          P_("Maximum reachable speed"),
                          0, G_MAXUINT, 1000, G_PARAM_WRITABLE);
    props[PROP_ACCELERATION] =
        g_param_spec_uint("acceleration", P_("Acceleration"),
                          P_("Slope of the ramp while increasing speed"),
                          0, G_MAXUINT, 500, G_PARAM_WRITABLE);
    props[PROP_DECELERATION] =
        g_param_spec_uint("deceleration", P_("Deceleration"),
                          P_("Slope of the ramp while decreasing speed"),
                          0, G_MAXUINT, 500, G_PARAM_WRITABLE);

    g_object_class_install_properties(gobject_class, NUM_PROPERTIES, props);
}

static void
machinery_ramp_init(MachineryRamp *ramp)
{
    MachineryRampPrivate *data = machinery_ramp_get_instance_private(ramp);
    data->speed = 0;
    data->position = 0;
    data->destination = 0;
    data->timestamp = 0;

    set_min_speed(ramp, 0);
    set_max_speed(ramp, 1000);
    set_acceleration(ramp, 500);
    set_deceleration(ramp, 500);
}

/**
 * machinery_ramp_new:
 *
 * Creates a new MachineryRamp instance. The object has exactly one reference
 * and must be freed with g_object_unref() when non longer needed.
 *
 * Returns: a newly created #MachineryRamp instance
 *
 * Since: 1.0
 **/
MachineryRamp *
machinery_ramp_new(void)
{
    return g_object_new(MACHINERY_TYPE_RAMP, NULL);
}

/**
 * machinery_ramp_set_min_speed:
 * @ramp: a #MachineryRamp instance
 * @speed: the new minimum speed
 *
 * Sets a new minimum speed on this ramp instance.
 *
 * Since: 1.0
 **/
void
machinery_ramp_set_min_speed(MachineryRamp *ramp, guint speed)
{
    g_return_if_fail(MACHINERY_IS_RAMP(ramp));
    set_min_speed(ramp, speed);
    g_object_notify_by_pspec(G_OBJECT(ramp), props[PROP_MIN_SPEED]);
}

/**
 * machinery_ramp_get_min_speed:
 * @ramp: a #MachineryRamp instance
 *
 * Gets the minimum speed of @ramp.
 *
 * Returns: the minimum speed.
 *
 * Since: 1.0
 **/
guint
machinery_ramp_get_min_speed(MachineryRamp *ramp)
{
    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), 0);
    return get_min_speed(ramp);
}

/**
 * machinery_ramp_set_max_speed:
 * @ramp: a #MachineryRamp instance
 * @speed: the new maximum speed
 *
 * Sets a new maximum speed on this ramp instance.
 *
 * Since: 1.0
 **/
void
machinery_ramp_set_max_speed(MachineryRamp *ramp, guint speed)
{
    g_return_if_fail(MACHINERY_IS_RAMP(ramp));
    set_max_speed(ramp, speed);
    g_object_notify_by_pspec(G_OBJECT(ramp), props[PROP_MAX_SPEED]);
}

/**
 * machinery_ramp_get_max_speed:
 * @ramp: a #MachineryRamp instance
 *
 * Gets the maximum speed of @ramp.
 *
 * Returns: the maximum speed.
 *
 * Since: 1.0
 **/
guint
machinery_ramp_get_max_speed(MachineryRamp *ramp)
{
    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), 0);
    return get_max_speed(ramp);
}

/**
 * machinery_ramp_get_speed:
 * @ramp: a #MachineryRamp instance
 *
 * Gets the actual speed of @ramp. Useful on a speed-based driving loop:
 * see machinery_ramp_update_position() for an example.
 *
 * The speed is a #gint because it has a direction component.
 *
 * Returns: the current speed.
 *
 * Since: 1.0
 **/
gint
machinery_ramp_get_speed(MachineryRamp *ramp)
{
    MachineryRampPrivate *data;

    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), 0);

    data = machinery_ramp_get_instance_private(ramp);
    return data->speed;
}

/**
 * machinery_ramp_set_acceleration:
 * @ramp: a #MachineryRamp instance
 * @acceleration: the new acceleration
 *
 * Sets a new acceleration on this ramp instance.
 *
 * Since: 1.0
 **/
void
machinery_ramp_set_acceleration(MachineryRamp *ramp, guint acceleration)
{
    g_return_if_fail(MACHINERY_IS_RAMP(ramp));
    set_acceleration(ramp, acceleration);
    g_object_notify_by_pspec(G_OBJECT(ramp), props[PROP_ACCELERATION]);
}

/**
 * machinery_ramp_get_acceleration:
 * @ramp: a #MachineryRamp instance
 *
 * Gets the acceleration of @ramp.
 *
 * Returns: the acceleration.
 *
 * Since: 1.0
 **/
guint
machinery_ramp_get_acceleration(MachineryRamp *ramp)
{
    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), 0);
    return get_acceleration(ramp);
}

/**
 * machinery_ramp_set_deceleration:
 * @ramp: a #MachineryRamp instance
 * @deceleration: the new deceleration
 *
 * Sets a new deceleration on this ramp instance.
 *
 * Since: 1.0
 **/
void
machinery_ramp_set_deceleration(MachineryRamp *ramp, guint deceleration)
{
    g_return_if_fail(MACHINERY_IS_RAMP(ramp));
    set_deceleration(ramp, deceleration);
    g_object_notify_by_pspec(G_OBJECT(ramp), props[PROP_DECELERATION]);
}

/**
 * machinery_ramp_get_deceleration:
 * @ramp: a #MachineryRamp instance
 *
 * Gets the deceleration of @ramp.
 *
 * Returns: the deceleration.
 *
 * Since: 1.0
 **/
guint
machinery_ramp_get_deceleration(MachineryRamp *ramp)
{
    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), 0);
    return get_deceleration(ramp);
}

/**
 * machinery_ramp_move:
 * @ramp: a #MachineryRamp instance
 * @destination: target destination
 *
 * Starts a movement command.
 *
 * Since: 1.0
 **/
void
machinery_ramp_move(MachineryRamp *ramp, gint32 destination)
{
    MachineryRampPrivate *data;

    g_return_if_fail(MACHINERY_IS_RAMP(ramp));

    data = machinery_ramp_get_instance_private(ramp);
    data->destination = destination;
    data->timestamp = g_get_monotonic_time();
}

/**
 * machinery_ramp_update_position:
 * @ramp: a #MachineryRamp instance
 * @position: actual position
 *
 * Function to call on a speed-based driving loop.
 * [[
 * guint speed;
 * guint32 position;
 *
 * machinery_ramp_move(ramp, destination);
 * for (;;) {
 *     position = ...;  // Read current position
 *     if (machinery_ramp_update_position(ramp, position)) {
 *         // Target destination reached
 *         break;
 *     }
 *     speed = machinery_ramp_get_speed(ramp);
 *     // Set the new speed and do other stuff
 *     ...
 * }
 * ]]
 *
 * Returns: TRUE on target position reached, FALSE otherwise.
 *
 * Since: 1.0
 **/
gboolean
machinery_ramp_update_position(MachineryRamp *ramp, gint32 position)
{
    MachineryRampPrivate *data;
    gint32 dp;
    gint sign, abs, min, max;
    gint64 timestamp, dt;

    g_return_val_if_fail(MACHINERY_IS_RAMP(ramp), FALSE);

    data = machinery_ramp_get_instance_private(ramp);
    timestamp = g_get_monotonic_time();
    dt = timestamp - data->timestamp;
    min = data->min_speed;
    max = data->max_speed;

    sign = data->speed >= 0 ? 1 : -1;
    abs = sign * data->speed;
    dp = sign * (data->destination - position);

    if (dp == 0 && abs < data->max_exit_speed) {
        /* Destination reached and speed close to the minimum */
        abs = 0;
    } else if (dp < 0) {
        /* Inversion required: decelerate */
        abs -= dt * data->deceleration / 1000000;
        if (abs < min) {
            sign = -sign;
            abs = min;
        }
    } else {
        gdouble v = sqrt(2.0 * data->deceleration * dp);
        if (v > abs) {
            /* Accelerate */
            abs += dt * data->acceleration / 1000000;
            if (abs > v) {
                abs = v;
            }
        } else {
            /* Decelerate */
            abs -= dt * data->deceleration / 1000000;
            if (abs < v) {
                abs = v;
            }
        }
        abs = CLAMP(abs, min, max);
    }

    data->timestamp = timestamp;
    data->position = position;
    data->speed = sign * abs;
    return dp == 0;
}
