#ifndef MACHINERY_GTK_H_
#define MACHINERY_GTK_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

typedef gchar *(*PermissionCallback)(void);
typedef struct _Deferred Deferred;


gint            machinery_widget_get_index          (GtkWidget *        widget);
GtkRadioButton *machinery_radio_group_get_active    (GtkRadioButton *   radio_group);

Deferred *      machinery_deferred_connect          (gpointer           instance,
                                                     const gchar *      signal,
                                                     GCallback          callback,
                                                     gpointer           user_data);
gulong          machinery_deferred_get_handler_id   (Deferred *         deferred);
gulong          machinery_deferred_connect_again    (Deferred *         deferred,
                                                     gpointer           instance,
                                                     const gchar *      signal);
void            machinery_deferred_connect_signals  (Deferred *         deferred,
                                                     gpointer           instance,
                                                     ...);
void            machinery_deferred_call             (Deferred *         deferred);

void            machinery_permission                (GtkLockButton *    lock_button,
                                                     GtkWidget *        settings,
                                                     const gchar *      password,
                                                     PermissionCallback callback);

GtkDialog *     machinery_input_new                 (GtkWindow *        parent,
                                                     gint               width,
                                                     gint               height, 
                                                     gint64             keyboard_plug);
GtkDialog *     machinery_input_new_with_command    (GtkWindow *        parent,
                                                     gint               width,
                                                     gint               height,
                                                     const gchar *      cmd);
gchar *         machinery_input_run                 (GtkDialog *        input,
                                                     const gchar *      title,
                                                     const gchar *      message,
                                                     const gchar *      prefill);

void            machinery_bind_settings             (GtkWidget *        widget,
                                                     GSettings *        settings,
                                                     const gchar *      prefix,
                                                     GSettingsBindFlags flags);
void            machinery_bind_settings_file_chooser(GtkFileChooser *   file_chooser,
                                                     GSettings *        settings,
                                                     const gchar *      key);
void            machinery_bind_settings_radio_group (GtkRadioButton *   radio_group,
                                                     GSettings *        settings,
                                                     const gchar *      key);
void            machinery_bind_input_dialog         (GtkWidget *        widget,
                                                     GtkDialog *        input_dialog,
                                                     const gchar *      title);
void            machinery_bind_input_dialog_without_spin_hack
                                                    (GtkWidget *        widget,
                                                     GtkDialog *        input_dialog,
                                                     const gchar *      title);

G_END_DECLS

#endif /* MACHINERY_GTK_H_ */
